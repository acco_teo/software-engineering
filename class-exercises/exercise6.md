# Speed Monitoring System on Highways

Traditional speed monitoring systems measure the instantaneous speed of a car on a street, following the car for few meters with radar based appliances.

The idea of the Tutor system instead is to measure the average speed of a car on a long distance (in the order of 1-10 kilometers). 

This is made by measuring the time when a car passes at point A, the time when the same car passes at point B, and doing simple math to compute the average speed. In point A and B on the highway a gate is built with one or more cameras that take pictures of cars passing, triggered by a motion sensor. 

The pictures, along with the time when they were shot, are sent to a computer based system for processing. 

The output is the average speed of each car passed through the gates.

In the following you should model this system, including hardware (cameras, computers) and software (notably functions provided). 

+ Define the stakeholders analysis
+ Define the context diagram (including relevant interfaces) 
+ Define the glossary with a class diagram.
+ Define the functional requirements
+ Define the non functional requirements 
+ Define the use case diagram 
+ Define one scenario describing a successful measurement of the speed of a vehicle. 
+ Define one scenario describing an unsuccessful measure. 
+ Define the system design

##Stakeholers analysis
| Stakeholder name | Interest |
|:----------------:|:--------:|
| Police | Receive reports about cars speed, reduce average speed of drivers |
| Owner | Provide speed reports to the police |
| Driver | Do not receive speed tickets, so he/she will reduce speed |
| System admin | Manage the system/sensors |

##Contex Diagram
```plantuml
lefto to right direction
skinparam packageStyle rectangle
actor Car as c
actor Police as p

rectangle system {
	(Tutor System) as ts
	p -- ts : "system report"
	c -- ts 
}

note "The gate/sensors are part of the system" as n
```

##Interfaces
- Interface to car (indirectly via motion sensor and camera)
- APIs to send speed reports to the Police
- Speed Reports should be defined (XML, Json, etc.) and should include number plate of the car, timestamp, speed, link to the pictures

##Glossary and Class Diagram
```plantuml
class SpeedComputer {
	+ distanceAtoB
}

class CarSpeedReport {
	+ carNumberPlate
	+ timestamp
	+ time_A
	+ time_B
	+ pathToPictureA
	+ pathToPictureB
}

class Picture {
	+ ID
	+ fileName
	+ filePath
}

class Gate {
	+ ID
	+ location
}

SpeedComputer "1" -- "*" CarSpeedReport : produces
SpeedComputer "1" -- "*" Picture : receives
Gate "1" -- "*" Picture : takes
```

##Requirements
###Functional
| ID  | Description |
|:---:|:-----------:|
| FR1		| Compute speed |
| FR1.1	| Take picture |
| FR1.2	| Process picture |
| FR1.3	| Recognize car number plate |
| FR2		| Produce speed report |

###Non Functional
| ID  | Type | Description | Refers to |
|:---:|:----:|:-----------:|:---------:|
| NF1 | Domain | Speed shall be computed in km/h | FR1 |
| NF2 | Reliability | Unsuccesful measurements shall be less than 5% of the total measurement | FR1, FR2 |
| NF3 | Reliability | System downtime shall be les than 30 minutes per week | FR1 |
| NF4 | Efficiency | Speed reports shall be produced in less than 2 minutes | FR1 |
| NF5 | Legislation | Speed reports shall be sent to the police in less than 60 days | FR2 |
| NF6 | Privacy | Sensitive data shall be preserved | FR2 |
| NF7 | Reliability | Computed speed shall not be 5% different from real speed | FR1 |

##Use Case Diagram
```plantuml
left to right direction
skinparam packageStyle rectangle

actor Car as c
actor Police as p

p <-- (Produce Speed Report)

(Produce Speed Report) .> (Compute Speed) : include
(Compute Speed) .> (Sense Car) : include

c --> (Sense Car)

(Compute Speed) .> (Take Picture) : include
(Compute Speed) .> (Process Picture) : include
(Compute Speed) .> (Recognize Number Plate) : include
```

##Scenarios
###Successful measurement of the speed of a vehicle
- Precondition: car with n_plate AA000AA passes between the gates
- Post condition: average speed for car with n_plate AA0000A is computed and stored

| Scenario ID: SC1 | Corresponds to UC: Compute Speed |
|:----------------:|--------------------------------|
| Step # | Description |
| 1 | Motion sensor A detects a car and triggers the Camera A to take the picture |
| 2 | Camera A takes the picture |
| 3 | Camera A sends the picture to the SpeedComputer |
| 4 | SpeedComputer processes the picture, recognize the n_plate AA000AA and generates the first part of the Speed Report | 
| 5 | Motion sensor B detects a car and triggers the Camera B to take the picture |
| 6 | Camera B takes the picture |
| 7 | Camera B sends the picture to the SpeedComputer |
| 8 | SpeedComputer processes the picture, recognize the n_plate AA000AA and generates the second part of the Speed Report |
| 9 | SpeedComputer computes the speed=(distance B - distance A)/(time B - time A) and stores the Speed Report for the car with the n_plate AA000AA |

###Usuccessful measurement of the speed of a vehicle
- Precondition: N/A
- Post condition: average speed for car with n_plate AA0000A is computed and stored

| Scenario ID: SC1 | Corresponds to UC: Compute Speed |
|:----------------:|--------------------------------|
| Step # | Description |
| 1 | Motion sensor A detects a car and triggers the Camera A to take the picture |
| 2 | Camera A takes the picture |
| 3 | Camera A sends the picture to the SpeedComputer |
| 4 | SpeedComputer processes the picture, the n_plate is not recognized|

##System Design Diagram
```plantuml
class TutorSystem {
}

class Gate {
}

class MotionSensor {
	+ senseCar()
}

class Camerea {
}

class SpeedComputer {
	+ distanceAtoB()
	+ processPicture()
	+ recognizeNPlatePicture()
	+ computeSpeed()
	+ produceSpeedReport()
}

TutorSystem o-- "2" Gate
TutorSystem o-- "1" SpeedComputer

Gate -- MotionSensor
Gate -- Camera
Camera -- MotionSensor
Camera -- SpeedComputer

```
