# Exercise 1
Define a class diagram for a university. 
In a university there are different classrooms, offices and departments. 
A *department* has a name and it contains many offices. 
A *person* working at the university has a unique ID and can be a *professor* or an *employee*.

*	A professor can be a full, associate or assistant professor and he/she is enrolled in one department.
*	*Offices* and classrooms have a number ID, and a classroom has a number of seats.
*	Every Employee works in an office

```plantuml

class Classroom {
	ID
	nSeats
}

class Office {
	ID
}

class Department {
	name
}

class Person {
	ID
}

University o-- "1..*" Classroom
University o-- "1..*" Department
Univeristy o-- "1..*" Office
Depratment o-- "0..*" Office
University -- "1..*" Person : works in
Person "0..*" -- "1" Office : works in
Person <|-- Professor
Person <|-- Employee
Professor <|-- Full
Professor <|-- Associate
Professor <|-- Assistant   

```