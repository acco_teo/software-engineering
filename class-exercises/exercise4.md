# Multplex Cinema Management

------------------------------------------------------
A multiplex cinema is composed of several rooms, each room may show a different movie, with a different schedule. Each room has a specific seat capacity, to be strictly enforced for safety regulations. 

Each seat in a room has an id. A management system for a multiplex cinema has to be built because there are several multiplex cinemas across Italy.

The management system will be used by clerks to sell tickets at a counter in the cinema. The sale of a ticket can also be done via Internet, the customers buys it connecting to a web site. A ticket is valid for a specific seat, in a specific room, for a specific show (movie and start time). 

A ticket is unique and has a bar code. The ticket must be printed to guarantee admission. A ticket is sold only in the same day of the show, and only if there is a seat available. The user can choose the seat. A ticket is refundable, if not used.

Another function of the system is admitting (or rejecting) people to the rooms. Admission is regulated by a gate, which opens after showing a valid ticket to a bar code reader. There is a first row of gates controlling access to all rooms, then a gate controlling access to each room.

Other functions supported by the system are refund of a ticket; various monitor functions for the clerks (how many persons are in a room, how many seats available for a room/movie session, average occupation of a room per day/ per session); definition of the schedule for the day (what movie for what room at what time); printout of the schedule of the day. 

- Define the context diagram (including relevant interfaces)
- Define the class diagram
- Define the use case diagram
- Define one scenario describing a successful sale for a ticket for a movie. 
- Define one scenario describing a successful admission to a room. 

------------------------------------------------------

## Context Diagram
```plantuml
left to right direction
skinparam packageStyle rectangle

actor Internt as i 
actor User as u
actor Credit Card System as ccs

rectangle system {
	(Multiplex Cinema) as mc
	u -- mc
	i -- mc
	ccs -- mc	
}
```

## Interfaces
- To User: GUI - web page accessible through internet
- To Credit Card System: web services, APIs

## Class Diagram
```plantuml
Class Multiplex {
	name
	location
}

Class Room {
	ID
	seatCapacity
}

Class Ticket {
	barCode
	used
	refundable
}

Class Show {
	startTime
	endTime
}

Class Movie {
	ID
	title
}

Class Seat {
	ID
	type
	occupied
}

Multiplex "1" -- "1..*" Room : has
Room "1" -- "1..*" Show : takes place
Show "1" -- "0..*" Ticket : requires
Show "1..*" -- "1" Movie : plays
Room "1" -- "1..*" Seat : contains
Ticket "1" -- "1" Seat : books
```
## Use Case Diagram
```plantuml
left to right direction
skinparam packageStyle rectangle

actor Customer as c 
actor Clerk as cl
actor Credit Card System as ccs

(Sell Ticket) as s
(Admit to room) as a
(Choose seat) as cs
(Handle payment) as h
(Print ticket) as t
(Refund a ticket) as r
(Monitor) as m
(Define schedule) as d
(Print schedule) as p

c -- a
c -- s

s -- ccs

s .> cs : include
s .> p  : include
s .> h  : include

cl -- m
cl -- s
cl -- r
cl -- d
cl -- p

}
```

##Scenarios
### Successful sale for a ticket for a movie
- Precondition: at least one seat is available for the selected show
- Post condition: selected seat is now occupied

| Scenario ID: SC1 | Corresponds to UC: Admit to Room |
|:----------------:|:--------------------------------:|
| Step # | Description |
| 1 | Customer selects show |
| 2 | Customer selects seats |
| 3 | Customer pays |
| 4 | System sets the seat as occupied, decreases the total # of seats available |

### Successful admission to a room
- Precondition: customer has a valid ticket for show and room
- Post condition: selected seat is now occupied

| Scenario ID: SC2 | Corresponds to UC: Admit to Room |
|:----------------:|:--------------------------------:|
| Step # | Description |
| 1 | Customer shows ticket to the barcode reader |
| 2 | System checks if the ticket is valid for show and room |
| 3 | Room gate opens, customer enters |
| 4 | System sets ticket as used and the seat as occupied |

##System Design Diagram
```plantuml
```