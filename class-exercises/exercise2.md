# Exercise 2
Draw a class diagram to model a system for management of flights and pilots.
An *airline* operates flights. Each airline has an ID.
Each *flight* has an ID a departure airport and an arrival *airport*: an airport has a unique identifier.
Each flight has a *pilot* and a co-pilot, and it uses an aircraft of a certain type; a flight has also a departure time and an arrival time.
An airline owns a set of *aircrafts* of different *types*.
An aircraft can be in a working state or it can be under repair, and in a particular moment an aircraft can be landed or airborne.
A company has a set of *pilots*: each pilot has an experience level: 1 is minimum, 3 is maximum.
A type of aircraft may need a particular number of pilots, with a different role (eg. captain, co-pilot, navigator): there must be at least one captain and one co-pilot, and a captain must have a level 3.
 

```plantuml
left to right direction

class Airline {
	ID
	name 	
}
	
class Flight {
	ID
	departureTime
	arrivalTime
}
	
class Airport {
	ID
	name
}
	
class Aircraft {
	name
	underRepair
	landed
}
	
class "Aircraft Type" {
	name
	nEngines
	maxPassengers
}

class Pilot {
	ID
	name
	levelExperience
}

Pilot <|-- Pilot1
Pilot <|-- Pilot2
Pilot <|-- Pilot3

Airline "1" -- "*" Flight : offers
Airline "1" -- "*" Aircraft : owns
Aircraft "*" -- "1" "Aircraft Type" : is of
Flight "*" -- "1" Airport : departs from
Flight "*" -- "1" Airport : arrives from
Flight "*" -- "1" Aircraft : uses
"Aircraft Type" "*" -- "1" Pilot3 : captain of
"Aircraft Type" "*" -- "1" Pilot : co-pilot of
"Aircraft Type" "*" -- "0.1" Pilot : naivgator of


note top of Aircraft : if the Aircraft is under repair it cannot fly!
note top of Pilot : level must be 1, 2 or 3
note top of Pilot3 : pilot with level 3 of expertise

```