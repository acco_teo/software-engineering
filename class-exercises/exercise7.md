#TESTING

- *Step 1*: define criteria
- *Step 2*: define predicates for each criterion
- *Step 3*: run the test cases (1,2 per subset)


#EXERCISE
A queue of events in a simulation system receives events. Each event has a time tag.
It's possible to extract events from the queue, the extraction must return the event with lower time tag.
The queue must accept at most 100'000 events.
Events with the same time tag must be merged (i.e. the second received is discarded)

public class EventsQueue {
	public void reset();	//cancels all events 
	public vod push(int timeTag) throws InvalidTag, QueueOverflow
	public int pop() throws EmptyQueue
}

Define test cases
- define equivalence classes, and related boundary condition
- for each class define at least one test case

##PUSH
###Step 1 (criteria)
- Sign of timeTag
- There are equal timeTags
- Number of timeTags
- Type on timeTag

###Step 2 (predicates)
| Criterion						|			|
| --------------------------- | ------- |
| **Sign of timeTag**			| >0 		|
|									| ≤0 		|
| **Type of timeTag**			| Integer |
|									| Char		|
| 									| Float	|
|									| String	|
| **Equal timeTags**			| Yes		|
|									| No		|
| **timeTags number**			| ≤100'000 |
|									| >100'000 |

###Step 3 (combine predicates)
| Type    | Sign     | Equal | Number   | Test case |
| ------- | -------- | ----- | -------- | --------- |
| Integer | Positive | No    | ≤100'000 | Push(10);<br>Push(11);<br>Push(100);<br>Pop->10;<br>Pop->11; |
| Integer | Positive | No    | >100'000 | for(100'000 times) {<br>&nbsp;&nbsp;&nbsp;&nbsp;Push(i);<br>&nbsp;&nbsp;&nbsp;&nbsp;i++<br>}<br>Push(900'000)->QueueOverflow |
| Integer | Positive | Yes   | ≤100'000 | Push(10);<br>Push(10);<br>Push(1000);<br>Push(1000);<br>Pop->10;<br>Pop->1000; |
| Integer | Positive | Yes   | >100'000 |      |
| Integer | Negative | No    | ≤100'000 |      |
| Integer | Negative | No    | >100'000 |      |
| Integer | Negative | Yes   | ≤100'000 |      |
| Integer | Negative | Yes   | >100'000 |      |
| Char		| Positive | No    | ≤100'000 |      |
| Char		| Positive | No    | >100'000 |      |
| Char		| Positive | Yes   | ≤100'000 |      |
| Char		| Positive | Yes   | >100'000 |      |
| Char		| Negative | No    | ≤100'000 |      |
| Char		| Negative | No    | >100'000 |      |
| Char		| Negative | Yes   | ≤100'000 |      |
| Char		| Negative | Yes   | >100'000 |      |
| Float	| Positive | No    | ≤100'000 |      |
| Float	| Positive | No    | >100'000 |      |
| Float	| Positive | Yes   | ≤100'000 |      |
| Float	| Positive | Yes   | >100'000 |      |
| Float	| Negative | No    | ≤100'000 |      |
| Float	| Negative | No    | >100'000 |      |
| Float	| Negative | Yes   | ≤100'000 |      |
| Float	| Negative | Yes   | >100'000 |      |
| String	| Positive | No    | ≤100'000 |      |
| String	| Positive | No    | >100'000 |      |
| String	| Positive | Yes   | ≤100'000 |      |
| String	| Positive | Yes   | >100'000 |      |
| String	| Negative | No    | ≤100'000 |      |
| String	| Negative | No    | >100'000 |      |
| String	| Negative | Yes   | ≤100'000 |      |
| String	| Negative | Yes   | >100'000 |      |

##POP
###Step 1 (criteria)
- Number of timeTags
- Equal timeTags

###Step 2 (predicates)
| Criterion						|			|
| --------------------------- | ------- |
| **Sign of timeTag**			| >0 		|
|									| ≤0 		|
| **Type of timeTag**			| Integer |
|									| Char		|
| 									| Float	|
|									| String	|
| **Equal timeTags**			| Yes		|
|									| No		|
| **timeTags number**			| ≤100'000 |
|									| >100'000 |

###Step 3 (combine predicates)
The combination of predicates is a subset of table for PUSH.  


