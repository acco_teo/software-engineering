# Exercise 3

Each house connected to the electric network has a *meter* . The meter computes and stores how much electrical energy (kW hour) is used by the house. 
The meter sends regularly (say every 24 hours) the consumption over the electric wires (the electric wires of the power network can in fact be used as data transmission lines). 
A remote central server receives the data, stores it and computes a bill (say every two months), sends the bill to the *customer*. 

The bill can be sent in two different ways:
 
* electronically (*email* or web site accessible to the customer via username and password),
* on paper. (using *postal service*)

In the following analyze and model the application that collects power usage from each meter, computes the bill and sends it to the customer. *bank*

1. Define the context diagram (including relevant interfaces)
2. List the requirements in tabular form
3. Write a glossary by defining the key concepts and entities and their relationships, with UML class diagram, for the application.

## CONTEXT DIAGRAM
```plantuml
left to right direction
skinparam packageStyle rectangle

actor Meter as m
actor Customer as c
actor "Email System" as es
actor "Postal Service" as ps
actor "Banking System" as bs

rectangle system {
	(Electric Power Biliing System) as epbs
	epbs -- m
	epbs -- c
	bs -- epbs
	ps -- epbs
	epbs -- es
	
}

```

### Interfaces:

* to coustomer: GUI - web page;
* to postal service: web service offered by postal company;
* to emay system: web service, POP, IMAP, SMTP;
* meter: electrical wire;
* banking system: web services, APIs.

## REQUIREMENTS

| ID  | Type (F NF)| Description  |
| --- |:----------:|:------------:|
| 1   | F          | Read consumption from meter |
| 2   | F          | Store meter reading with timestamp |
| 3   | F          | Send meter reading to server |
| 4   | F          | Compute the bill |
| 5   | F          | Send the bill via postal system |
| 6   | F          | Send the bill via mail |
| 7   | NF         | Privacy: data of each customer should not be visible from others |
| 8   | NF         | Domain: currency is € |
| 9   | NF         | Domain: Electric consumption is in KWh | 

##GLOSSARY
```plantuml
Class Customer {
	ID
	name
	surname
	address
}

class Meter {
	ID
	model
}

class Contract {
	ID
}

class Bill {
	ID
	date
	consumption
	amount
	period
}

class MeterReading {
	date
	kwh
}

Customer "1" -- "1..*" Contract : signs
Contracr "1" -- "1" Meter : assigns
Contracr "1" -- "1..*" Bill : generates
Contract "1" -- "1..*" MeterReading : lists

```

##SCENARIO
|	Step	|	Description	|
|:-------:|:------------:	|
|	1		|
