# Internship

------------------------------------------------------
In order to perform the internship, each student has to select a proposal from a list. Each proposal is acknowledged by a company agreement with the university.

The Stage & Job office receives the request, verifies the formal correctness (e.g. by checking if the student has enough credits for applying the internship) and then it activates the internship.
At this point the student must find an academic tutor (a professor) who wants to follow the training program, and then the student enter his/her name in the request. The academic tutor must countersign the request as well as the company tutor (already defined by the company in the proposal).

Once the Stage & Job office accepts the proposal, the company (in accordance with the student)
defines for the internship a start date, and an end date.

The student begins the internship at the specified date.
At the end of the internship, the student must deliver to the office Stage & Job the attendance sheet. In addition, both the company tutor and the student must complete a specific evaluation questionnaire.

The professor must then assign a mark for the internship.

At this point the internship is recorded in the student's academic career

- Provide the UML Use Case Diagram for the case study described above
- Identify the three main use cases, and for each use case describe the related scenarios.

------------------------------------------------------
## Use Case Diagram
```plantuml
left to right direction

actor Student as s
actor Academic Tutor as at
actor Stage&Job as sj
actor Professor as p
actor Company as c
actor Company Tutor as ct

(Load proposal) as lp
(Apply for internship) as afi
(Rate internship) as ri
(Put mark) as pm
(Formal check) as fc
(Credid check) as cc
(Accept proposal) as ap

c -- lp
s -- afi
s -- ri
ri -- ct
p -- pm
sj -- fc
sj -- ap
fc .> cc : include

```
## Scenarios
### Internship proposal

- Precondition: company registered in the university informetion system
- Post condition: the proposal is in the list
 
| Scenario ID: SC1 | Corresponds to UC: Load proposal |
|:----------------:|:--------------------------------:|
| Step # | Description |
| 1 | Company selects 'add new proposal' |
| 2 | System shows forms for describing the proposal |
| 3 | Company adds proposal details in the form and confirms |
| 4 | System shows the list of the company proposals |

### Internship selection

- Precondition: student enrolled in the current university 
- Post condition: the request is sent to the S&J office
 
| Scenario ID: SC2 | Corresponds to UC: Apply for internship |
|:----------------:|:---------------------------------------:|
| Step # | Description |
| 1 | Student opens the internships list |
| 2 | System shows the list of proposals |
| 3 | Student selects one of them |
| 4 | System shows the details for the selected internship |
| 5 | Student adds the professor's name and clicks on apply button |
| 6 | System stores the request and sends it to the S&J office for verification |

### Evaluation and Mark

- Precondition: professor is a tutor for the given internship
- Post condition: the grade is registered into the student's career
 
| Scenario ID: SC3 | Corresponds to UC: Put mark |
|:----------------:|:---------------------------:|
| Step # | Description |
| 1 | Professor opens the list of internships he/she is managing |
| 2 | System shows tthe list of internships and students  |
| 3 | Professor selects the current instrnship and puts the grade |
| 4 | System stores the grade into the student's career |