# Requirements Document Template

Authors: MMMA group

Date: 09/04/2019

Version: 2.1.1

# Contents

- [Stakeholders](#stakeholders)
- [Context Diagram and interfaces](#context-diagram-and-interfaces)
	+ [Context Diagram](#context-diagram)
	+ [Interfaces](#interfaces) 
- [Stories and personas](#stories-and-personas)
- [Functional and non functional requirements](#functional-and-non-functional-requirements)
	+ [Functional Requirements](#functional-requirements)
	+ [Non functional requirements](#non-functional-requirements)
- [Use case diagram and use cases](#use-case-diagram-and-use-cases)
	+ [Use case diagram](#use-case-diagram)
	+ [Use cases](#use-cases)
	+ [Relevant scenarios](#relevant-scenarios)
- [Glossary](#glossary)
- [System design](#system-design)


# Stakeholders

| Stakeholder name  | Description | 
| ----------------- | ----------- |
| Manager | Handle the purchase and payment of capsules |
| Employee | Buy capsules with cash or local account (indirectly through Manager)| 


# Context Diagram and interfaces

## Context Diagram
```plantuml
left to right direction
skinparam packageStyle rectangle

actor Manager as m
actor "Email System" as es

rectangle system {
	(LaTazza) as lt
	m -- lt : manages
	lt -- es : supports
}
note right of es: The Supplier and Employees\nreceive emails from LaTazza\nthrough Email System 
```
## Interfaces
| Actor | Logical Interface | Physical Interface |
|:-----:|:-----------------:|:------------------:|
| Manager | GUI | Screen, keyboard, mouse, printer |
| Email System | SMTP/IMAP/POP3 | Internet connection |

# Stories and personas

## Persona # 1:

* **Name**: Walter Smith
* **Sex**: Male
* **Age**: Senior
* **Relation**: Married with children
* **Income**: High
* **Role**: Senior Manager
* **Work Schedule**: 8:30 am - 4:30 pm

## Persona # 2:

* **Name**: Jennifer Brown
* **Sex**: Female
* **Age**: Young
* **Relation**: Not married
* **Income**: Low
* **Role**: Intern
* **Work Schedule**: 8:30 am - 2:30 pm


## Persona # 3:

* **Name**: Steven Johnson
* **Sex**: Male
* **Age**: Adult
* **Relation**: Married
* **Income**: Middle-High
* **Role**: Employee
* **Work Schedule**: 8:30 am - 5:30 pm

## Story # 1

It's 3 P.M. and Walter Smith is reading an other email of complaint from Steven Johnson, an Employee that would
like to drink his favourite beverage during his break, after a whole morning of hard work.
Unfortunately, capsules for the coffee maker run out quickly and Walter can not waste time to go to
the coffee machine and check availability. Moreover, the Supplier never answers to the phone immediately, thus
wasting even more time trying to contact him.

## Story # 2

Jennifer Brown is a young intern that loves camomile tea. But she always forgets to bring capsules from home.
At the moment the only ways for Jennifer to have a cup of camomile are two: buying them in a shop - which
is quite far from the office - or buying them from Jacob, an Employee who fraudently controls the internal
trade of capsules among offices, charging a fee for himself. Being an intern, Jennifer is poorly paid and
would like to avoid spending too much money.


## Story # 3

Steven Johnson is a simple Employee. He likes to lead a quiet life, working not too much and having many breaks
along the work day. Unfortunately, his breaks are often ruined, because coffee capsules are not always available
in the machine. He hates to do so, but he has to complain with the Manager - who is in charge of checking the
capsules availability - sending an email. And this takes precious time away from his next break.

# Functional and non functional requirements

## Functional Requirements

| ID        | Description  |
|:---------:| ------------ | 
|  FR1      | Manage credit of Employees |
|  FR2      | Order boxes of capsules from Supplier through Email System |
|  FR3.1    | Sell capsules to Employees |
|  FR3.2    | Sell capsules to Visitors |
|  FR4      | Check the current status of the inventory |
|  FR5      | Update the inventory |
|  FR6      | Create a new Employee account |
|  FR7      | Delete an existing Employee account |
|  FR8      | Create a receipt for a transaction |
|  FR9.1    | Send a receipt through Email System to Employee |
|  FR9.2    | Send a set of transactions made by Employee through Email System to Employee |
|  FR10     | Print a receipt for a transaction |
|  FR11     | Save all the transactions, orders, inventory and user data |
|  FR12     | Create transactions history report according to some filters (Employee ID, time period) |
|  FR13     | Initialize System password, Supplier email, System email, inventory capacity, credit and debit thresholds |
|  FR14     | Change System configuration (see FR13) |
|  FR15     | Log into the system |
|  FR16     | Log-out from the system |


## Non Functional Requirements

| ID | Type | Description | Refers to |
|:---------:|:-------------|:-----|:-----:|
|  NFR1     | Domain | The Clients shall pay in Euros | FR3 |
|  NFR2     | Domain | The Visitors shall not have debt | FR3.2 |
|  NFR3     | Constraint | The Employee's debt shall not exceed the threshold | FR1 FR3.1 FR13 FR14 |
|  NFR4     | Constraint | The Employee's credit shall not exceed the threshold | FR1 FR3.1 FR13 FR14 |
|  NFR5     | Domain, Constraint | An Employee can not have more than one account | FR6 |
|  NFR6     | Efficiency, Safety, Constraint | The inventory shall not overflow due to huge amount of boxes of capsules | FR2 |
|  NFR7     | Domain | The payment of the order is made by hand to delivery boy | FR2 |
|  NFR8     | Domain | The System shall have an Email @ Email System to send emails | FR2 FR9 |
|  NFR9     | Privacy | List of all Employee's transactions can be sent only to the Employee they refer to | FR9.2 FR12 |
|  NFR10    | Reliability | Stored data shall not be lost | FR11 |
|  NFR11    | Privacy | Sensible data shall be protected according to privacy laws | FR11 |
|  NFR12    | Security | The access to the System requires the use of a password | FR13, FR15, FR16 |
|  NFR13    | Constraint, Security | Password shall contain at least 10 characters including a number and a capital letter | FR13 FR14 |
|  NFR14    | Portability | The System shall be installable and runnable on Linux, MacOS and Windows machines | FR14 |


# Use case diagram and use cases

## Use case diagram
```plantuml

skinparam LegendBackgroundColor yellow
skinparam LegendBorderColor red

left to right direction
actor Visitor as v
actor Employee as e
actor Manager as m
actor Supplier as s

(Charge account) as cc
(Sell capsules) as sc
(Manage credit) as mc
(Check inventory) as ci
(Update inventory) as ui
(Check cash account) as cca
(Manage inventory) as mi
(Order boxes of capsules) as ob
(Manage account) as ma
(Create account) as ca
(Delete account) as da
(Initialize the System) as isy
(Log-in) as lgi
(Log-out) as lgo

(Generate transaction history) as gth
v <-- sc
e <-- sc
e <-- cc
e <-- cca
s <-- ob
sc -- m
ma -- m
mi -- m
e <-- ca
e <-- da
ci <.. mi : include
ui <.. mi : include
ob <.. mi : include
mc <.  ma : include
cca <.. mc : include
cc <.. mc : include
ca <.. ma : include
da <.. ma : include
gth -- m
e <-- gth
m --> isy
m --> lgi
m --> lgo

legend
Charge credit and Sell capsules automatically generate a receipt
endlegend
```

## Use Cases

### UC1 (create account)
| Actors Involved	| Manager |
| ----------------- | ------- |
| Precondition 		| The Employee has already received the company badge. |
| Post condition	| The Employee account is created. |
| Nominal Scenario	| The Employee asks the Manager to create his/her account. The Manager checks if his/her badge is valid and then begins the creation procedure. He/she inserts ID, name, surname and email through the interface, confirms and waits for the message of success. |
| Variants			| The Employee wants to create a new account and to charge immediatly credits to his/her balance. The Manager procedes first with UC1 and right after with UC2. |

### UC2 (manage credit)
| Actors Involved	| Manager, Email System |
| ----------------- | --------------------- | 
| Precondition		| Employee account is valid. |  
| Post condition	| Employee account and the total cash amount are up to date. The receipt is produced. |
| Nominal Scenario	| The Employee has a valid account and wants to check his/her credit or to charge his/her balance. The Manager checks the badge of the Employee, selects the his/her ID through the system and updates the Employee credit if required. The summary is updated, the receipt is sent through Email System and the Manager handles the printed receipt to the Employee if he/she requests it. |
| Variants			| • The Employee has forgotten his/her badge at home. The Manager shouldn't perform any action (either to charge or to communicate balance).<br><br>• The Employee doesn't have a valid account because e.g. he/she has been hired recently. The Manager should create a new account and then proceeds. |

### UC3 (sell capsules)
| Actors Involved	| Manager, Email System |
| ----------------- | --------------------- | 
| Precondition		| The amount of money (credits or Euros) is sufficient to buy the capsule/s and the requested ones are available. |  
| Post condition	| The Manager gives the bought capsule/s to the Client and the receipt is produced. |
| Nominal Scenario (credits payment)	| The Employee requests to the Manager capsule/s of interest. The Manager starts the transaction. After the operation succeeds, he gets the capsules from the inventory and provides them to the Employee. Furthermore, the receipt is sent through Email System to the Employee's email and the Manager handles the printed receipt to the Employee if he/she requests it. |
| Nominal Scenario (cash payment)		| The Visitor requests to the Manager capsule/s of interest. The Manager gets the money and starts the transaction. After the operation succeeds, he gets the capsules from the inventory and provides them to the Visitor. Moreover, the Manager handles the printed receipt to the Visitor. |
| Variants			| The Manager has not enough coins to give the correct change to the Client in cash transactions. The Manager must propose a solution to the Client. |

### UC4 (manage inventory and order boxes of capsules)
| Actors Involved	| Manager, Email System |
| ----------------- | --------------------- | 
| Precondition		| Total cash amount covers the boxes order payment and the inventory capacity is not exceeded. |  
| Post condition	| The Email System sends the order email to the Supplier. |
| Nominal Scenario	| The Manager checks the inventory in order to decide if new boxes are needed. If a certain type of capsules runs out, he places a new order. |
| Variants			| The Manager orders more capsules even if the inventory has not run out. |

### UC5 (transaction history request)
| Actors Involved	| Manager, Email System |
| ----------------- | --------------------- | 
| Precondition		| The Manager or an Employee is interested in a particular set of transactions. |  
| Post condition	| The transaction history is computed and sent through the Email System to a specific email |
| Nominal Scenario (requested by Employee)	| The Manager sets the Employee ID and optionally the time period. The results are automatically sent to the Employee that made the request. |
| Nominal Scenario (requested by Manager)	| The Manager sets the time period filter. The results are automatically sent to the configured email (see UC6). |
| Variants			| The result set is empty. No Email is sent and a message is displayed on the screen. |

### UC6 (initialize system)
| Actors Involved	| Manager |
| ----------------- | ------- | 
| Precondition | System is correctly installed on the computer. |
| Post condition | System is successfully configured. |
| Nominal Scenario | The Manager runs the System for the first time. He should insert in order to complete the configuration: a proper password (in accord to NFR13), an email @ Email System, the email of the Supplier, the inventory capacity and finally the credit and debit thresholds. Then he waits for the completion message. |
| Variants | The Manager inserts a password that doesn’t meet the requirements. The System displays an error message until a correct one is inserted. |

# Relevant scenarios

## Scenario 1
### Successful sale for capsules to a Visitor

- *Preconditions*: Visitor has cash in Euro to pay for the goods and the requested capsules are available.

- *Post conditions*: Visitor has his capsules and the inventory is updated.

| Scenario ID: SC1 | Corresponds to UC2: Sell capsules to a Visitor |
|:-------------:|-------------| 
| Step#  | Description |
|  1     | The Visitor chooses the desired capsules and their quantity  |  
|  2     | The Manager checks for the availability of the goods |
|  3     | The Manager retrieves the money from the Visitor |  
|  4     | The System updates the inventory |
|  5     | The Manager gives the requested items to the Visitor |  


## Scenario 2
### Successful charge of money into the balance of an Employee

- *Preconditions*: Employee account is valid.

- *Post conditions*: Employee account and the total cash amount are up to date.

| Scenario ID: SC2 | Corresponds to UC1: manage credit |
|:-------------:|-------------| 
| Step#  | Description |
|  1     | The Employee asks the Manager to charge his account |  
|  2     | The Manager gets the money from the Employee |
|  3     | The Manager selects the Employee in the list showed by the System |  
|  4     | The Manager inserts the amount received in the corresponding form |
|  5     | The Manager confirms the operation |
|  6     | The Manager tells the outcome of the transaction to the Employee |

## Scenario 3
### Successful generation of transaction history

- *Preconditions*: An Employee is interested in a particular set of transactions

- *Post conditions*: The transaction history is computed and sent through the Email System to the Employee email

| Scenario ID: SC3 | Corresponds to UC5: transaction history requested by an Employee |
|:-------------:|-------------| 
| Step#  | Description |
|  1     | The Manager receives the request from the interested Employee, by voice |  
|  2     | The Manager checks the Employee identity, using the personal badge |
|  3     | The Manager selects the Employee ID in the list proposed by the System |  
|  4     | The Manager fills other possible filters (e.g. time period) according to the Employee request |
|  5     | The Manager confirms the research request on the System |
|  6     | The System shows a message to inform about the positive outcome of the operation |
|  7     | The email containing the results is sent to the Employee through the Email System |

# Glossary

```plantuml
 
left to right direction

class EmployeeAccount {
    ID
    Name
    Surname
    Balance
    Email
}

class Visitor {
}

Class Client{
}

class Transaction{
    ID
    dateTime
    totalAmount
    paymentType
}

class Box {
    ID
    name
    price
}

class CapsuleType{
    ID
    type
}

Class Capsule{
    ID
    price
    expirationDate
}

Class Manager{
    ID
    Name
    Surname
}

Class Supplier{
    ID
    Name
    Email
}

Class Order{
    ID
    dateTime
    totAmount
    paymentType
    status
}

Class Inventory{
    cashAccount
    size
}



Inventory "1" -- "0..*" Capsule: contains
Manager "1" -- "1" Inventory : manages

Capsule "50" -- "1" Box : contains
Capsule "0..*" -- "1" CapsuleType: of
Transaction "0..1" - "0..*" Capsule: refers to

Client  <|.. Visitor  
EmployeeAccount "0..*" --- "1"  Manager: manages
Manager "1"  -- "0..*" Order : makes

Supplier "1" -- "0..*" Order: receives 
Supplier "1" --- "0..*" Order : delivers

Order "1" -- "1..*" Box: refers to 
Transaction "0..*" -- "1" Manager : manages
Box "1" -- "1" CapsuleType: contains

Client  <|..  EmployeeAccount
Client "1" -- "0..*" Transaction: refers to
```


# System Design
```plantuml
class LaTazza {
	+createEmployeeAccount()
	+deleteEmployeeAccount()
	+chargeEmployeeCredit()
	+checkEmployeeCredit()
	+sellCapsulesWithCredits()
	+sellCapsulesWithCash()
	+orderBoxes()
    +checkInventory()
    +updateInventory()
    +createReceipt()
    +printReceipt()
    +sendReceipt()
    +initSystem()
    +editSettings()
    +login()
    +logout()
}

class Printer{
    +print()
}


Computer -- LaTazza
Computer --- Printer
```