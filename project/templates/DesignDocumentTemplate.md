# Design Document Template

Authors: MMMA group

Date: 04/06/2019

Version: 1.2.0

# Contents

- [Package diagram](#package-diagram)
- [Class diagram](#class-diagram)
- [Verification traceability matrix](#verification-traceability-matrix)
- [Verification sequence diagrams](#verification-sequence-diagrams)

# Instructions

The design document has to comply with:
1. [Official Requirement Document](../Official\ Requirements\ Document.md)
2. [DataInterface.java](../src/main/java/it/polito/latazza/data/DataInterface.java)

UML diagrams **MUST** be written using plantuml notation.

# Package diagram

```plantuml

left to right direction

package data <<Frame>>  #ffffb3 {
}

package storage <<Frame>>  #ffffb3 {
}

package gui <<Frame>> #ffffb3 {
}

package data <<Frame>>  #ffffb3 {
}

package exceptions <<Frame>>  #ffffb3 {
}

data <.. gui
storage <.. data 
exceptions <.. data

```


# Class diagram

```plantuml

package data <<Frame>>  #ffffb3 {

    class Transaction {
        -String date
        -String operation
        -String subject
        -String beverageName
        -String amount
        
        +setDate()
        +setOperation()
        +setSubject()
        +setBeverageName()
        +setAmount()
        +getDate()
        +getOperation()
        +getSubject()
        +getBeverageName()
        +getAmount()
        +toString()
        +hashCode()
        +equals()
    }

    class Employee {
        -Integer employeeId
        -String name
        -String surname
        -Integer balance
        
        +getEmployeeId()
        +getName()
        +getSurname()
        +getBalance()
        +setEmployeeId()
        +setName()
        +setSurname()
        +setBalance()
        +hashCode()
        +equals()
    }
    
    class Beverage {
        -Integer beverageId
        -String name
        -List<Integer> available
        -Integer boxPrice
        -Integer capsulesPerBox
        -List<Integer> price

        +getBeverageId()
        +getName()
        +getAvailable()
        +getBoxPrice()
        +getCapsulesPerBox()
        +getPrice()
        +setBeverageId()
        +setName()
        +setAvailableAndPrice()
        +aggregateBeverages()
        +getBeverages()
        +setBoxPrice()
        +setCapsulesPerBox()
        +equals()
    }
    
   interface Data {
        +sellCapsules()
        +sellCapsulesToVisitor()
        +rechargeAccount()
        +buyBoxes()
        +getEmployeeReport()
        +getReport()
        +createBeverage()
        +updateBeverage()
        +getBeverageName()
        +getBeverageCapsulesPerBox()
        +getBeverageBoxPrice()
        +getBeveragesId()
        +getBeverages()
        +getBeverageCapsules()
        +createEmployee()
        +updateEmployee()
        +getEmployeeName()
        +getEmployeeSurname()
        +getEmployeeBalance()
        +getEmployeesId()
        +getEmployees()
        +getBalance()
        +reset()
   } 
   
   class DataImpl {
        -Map<Integer, Employee> employees
        -Map<Integer, Beverage>  beverages
        -StorageImpl storage
        -Integer totalBalance
        
        +getTimestamp()
        +formatAmount()
   }
   
   Data <|-- DataImpl: implements
   DataImpl o-- Beverage 
   DataImpl o-- Employee 
   DataImpl o-- Transaction 
}
   
package storage <<Frame>>  #ffffb3 {

    class StorageImpl {
        -String pathTransactions
        -String pathBeverages
        -String pathEmployees
    }
    
    interface Storage {
        +readEmployees()
        +readTransactions()
        +readBeverages()
        +readBalance()
        +writeEmployees()
        +writeTransactions()
        +writeTransaction()
        +writeBeverages()
        +writeBalance()
    }
    
    Storage <|-- StorageImpl: implements
    Storage <.. DataImpl  
}
```

```plantuml
package gui <<Frame>>  #ffffb3 {

    class Refill {
    }
    
    class Buy {
    }
    
    class PrintLogsFrame {
    }
    
    class ReportFrame {
    }
    
    class BeveragesFrame {
    }
    
    class MainSwing {
    }
    
    class Payment {
    }
    
    class MappedArray {
    }
    
    class EmployeesFrame {
    }
    
    class Euro {
    }
}

package exception <<Frame>>  #ffffb3 {

    class NotEnoughCapsules  {
    }
    
    class NotEnoughBalance {
    }
    
    class EmployeeException {
    }
    
    class DateException {
    }
    
    class BeverageException {
    }
    
    class BadDataFormatException{
        
    }
}

```

# Verification traceability matrix

|  | DataImpl | Employee  | Beverage | StorageImpl | Transaction |
| ------------- |:-------------:| :-----: | :-----:| :-----: | :-----: | 
| FR1 | X | X | X | X | X |
| FR2 | X |  | X | X | X |
| FR3 | X | X |  | X | X |
| FR4 | X |  | X | X | X |
| FR5 | X |  |  | X | X |
| FR6 | X |  |  | X | X |
| FR7 | X |  | X | X |  |
| FR8 | X | X |  | X |  |

# Verification sequence diagrams 

### Sequence Diagram for Scenario 1
```plantuml
skinparam LegendBackgroundColor yellow
skinparam LegendBorderColor red

"GUI" -> "DataImpl": 1: sellCapsules()
"DataImpl" -> "Beverage": 2: getAvailable()
"DataImpl" <- "Beverage": 
"DataImpl" -> "Beverage": 3: getPrice()
"DataImpl" <- "Beverage": 
"DataImpl" -> "Employee": 4: getBalance()
"DataImpl" <- "Employee": 
"DataImpl" -> "Beverage": 5: setAvailable()
"DataImpl" -> "Employee": 6: setBalance()
"DataImpl" -> "StorageImpl": 7: writeEmployees()
"DataImpl" -> "StorageImpl": 8: writeTransactions() 
"DataImpl" -> "StorageImpl": 9: writeBeverages() 
```
### Sequence Diagram for Scenario 2
```plantuml
skinparam LegendBackgroundColor yellow
skinparam LegendBorderColor red

"GUI" -> "DataImpl": 1: sellCapsules()
"DataImpl" -> "Beverage": 2: getAvailable()
"DataImpl" <- "Beverage": 
"DataImpl" -> "Beverage": 3: getPrice()
"DataImpl" <- "Beverage": 
"DataImpl" -> "Employee": 4: getBalance()
"DataImpl" <- "Employee": 
"DataImpl" -> "Beverage": 5: setAvailable()
"DataImpl" -> "Employee": 6: setBalance()
"Employee" -> "Exception": 7: NotEnoughBalance()
"Exception" -> "DataImpl":
"DataImpl" -> GUI
"DataImpl" -> "StorageImpl": 8: writeEmployees()
"DataImpl" -> "StorageImpl": 9: writeTransactions() 
"DataImpl" -> "StorageImpl": 10: writeBeverages() 

```