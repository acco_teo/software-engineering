# Acceptance Testing Documentation template

Authors: MMMA group

Date: 21/05/2019

Version: 1.0.0

# Contents

- [Scenarios](#scenarios)

- [Coverage of scenarios](#scenario-coverage)
- [Coverage of non-functional requirements](#nfr-coverage)


# Scenarios

| Scenario ID: SC1 | Corresponds to UC1                             |
| ---------------- | ---------------------------------------------- |
| Description      | Colleague uses one capsule of type T           |
| Precondition     | Account of C has enough money to buy capsule T |
| Postcondition    | Account of C updated, count of T updated       |
| Step#            | Step description                               |
| 1                | Administrator selects "buy credits" = yes      |
| 2                | Administrator selects colleague C              |
| 3                | Administrator selects capsule type T           |
| 4                | Deduce one for quantity of capsule T           |
| 5                | Deduce price of T from account of C            |

| Scenario ID: SC1_bis | Corresponds to UC1                                 |
| ---------------- | ------------------------------------------------------ |
| Description      | Colleague uses one capsule of type T, account negative |
| Precondition     | Account of C has not enough money to buy capsule T     |
| Postcondition    | Account of C updated, count of T updated               |
| Step#            | Step description                                       |
| 1                | Administrator selects "buy credits" = yes              |
| 2                | Administrator selects colleague C                      |
| 3                | Administrator selects capsule type T                   |
| 4                | Deduce one for quantity of capsule T                   |
| 5                | Deduce price of T from account of C                    |
| 6                | Account of C is negative, issue warning                |

| Scenario ID: SC2 |               Corresponds to UC2            |
| ---------------- | ------------------------------------------- |
| Description      | Visitor uses one capsule of type T          |
| Precondition     | Capsule T is available                      |
| Postcondition    | System balance updated, count of T updated  |
| Step#            | Step description                            |
| 1                | Administrator selects capsule type T        |
| 2                | Administrator selects Visitor               |
| 3                | Deduce one for quantity of capsule T        |
| 4                | Add price of T to System balance            |

| Scenario ID: SC3 |        Corresponds to UC3                    |
| ---------------- | ---------------------------------------------|
| Description      | Colleague C recharges his own account        |
| Precondition     | Account of C exists                          |
| Postcondition    | Account of C updated                         |
| Step#            | Step description                             |
| 1                | Administrator selects colleague C            |
| 2                | Administrator inserts amount to be racharged |
| 3                | Add amount to C balance                      |

| Scenario ID: SC4 |                Corresponds to UC4                       |
| ---------------- | ------------------------------------------------------- |
| Description      | Administrator records purchase of one capsules box      |
| Precondition     | Capsule of type T exists                                |
| Postcondition    | System balance updated, count of T updated              |
| Step#            | Step description                                        |
| 1                | Administrator selects one as number of boxes            |
| 2                | Administrator selects Beverage type                     |
| 3                | Deduce box price from System balance, update count of T |

| Scenario ID: SC5 |                Corresponds to UC5                          |
| ---------------- | ---------------------------------------------------------- |
| Description      | Administrator creates a report of a colleague consumptions |
| Precondition     | Colleague C exists                                         |
| Postcondition    | Report is visualized                                       |
| Step#            | Step description                                           |
| 1                | Administrator selects colleague C                          |
| 2                | Administrator selects start and end date                   |

| Scenario ID: SC6 |                    Corresponds to UC6                      |
| ---------------- | ---------------------------------------------------------- |
| Description      | Administrator creates a report of all consumptions         |
| Precondition     | none                                                       |
| Postcondition    | Report is visualized                                       |
| Step#            | Step description                                           |
| 1                | Administrator selects start and end date                   |



# Coverage of Scenarios

### 

| Scenario ID | Functional Requirements covered |            API Test(s)         |    GUI Test(s)  |
| ----------- | ------------------------------- | ------------------------------ | --------------- |
| 1           | FR1                             | it.polito.latazza.data.testUC1 | SC1.txt         |
| 1_bis       | FR1                             | it.polito.latazza.data.testUC1 | SC1_bis.txt     |
| 2           | FR2                             | it.polito.latazza.data.testUC2 | SC2.txt         |
| 3           | FR3                             | it.polito.latazza.data.testUC3 | SC3.txt         |
| 4           | FR4                             | it.polito.latazza.data.testUC4 | SC4.txt         |
| 5           | FR5                             | it.polito.latazza.data.testUC5 | SC5.txt         |
| 6           | FR6                             | it.polito.latazza.data.testUC6 | SC6.txt         |



# Coverage of Non Functional Requirements

### 

| Non Functional Requirement |                      Test name                         |
| -------------------------- | ------------------------------------------------------ |
| NFR2                       | all methods in TestNFR class at it.polito.latazza.data |

