# Post Mortem Report

Authors: MMMA group

Date: 15/06/2019

Version: 0.0.1


# Requirements

We would have done more simple. It was not very clear, at the beginning, that we should have coded it later.

# Design


In this case, we would have done the same.


# Code and test cases

We tested some extreme border cases (i.e. overflow) that were not addressed. Having more strict requirements could have led to targeted tests.

# Changes

<Report here your observations, if any, about the three change cycles corresponding to deadlines 5,6,7>


# Build, tools and environment

<Report here your observations, if any, about the tools and environment used>

# Other observations

<Report here any other comment or suggestion >