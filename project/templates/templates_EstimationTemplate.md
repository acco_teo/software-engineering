# Project Estimation  template

Authors: MMMA group

Date: 28/05/2019

Version: 1.0.0

# Contents

- [Data from your LaTazza project](#data-from-your-latazza-project)
- [Estimate by product decomposition](#estimate-by-product-decomposition)
- [Estimate by activity decomposition](#estimate-by-activity-decomposition)


# Data from your LaTazza project

| Description | Value |   
| ----------- | ------------------------------- | 
| Total person hours  worked by your  team, considering period March 5 to May 26, considering ALL activities (req, des, code, test,..) | 18.25 |  
| Total Java LoC delivered on May 26 (only code, without Exceptions, no Junit code) | 856 |
| Total number of Java classes delivered on May 26 (only code, no Junit code, no Exception classes) | 8 |
| Productivity P = (856/146)| 5.86 |
| Average size of Java class A = | 107 |


# Estimate by product decomposition

|             | Estimate                        |             
| ----------- | ------------------------------- |  
| Estimated n classes NC (no Exception classes) | 11 |
| Estimated LOC per class  (Here use Average A computed above )      | 107 |
| Estimated LOC (= NC * A) | 1177 |
| Estimated effort  (person days) (Here use productivity P) = (1177 / 5.86 / 8)  | 25.1 |
| Estimated calendar time (calendar weeks) (Assume team of 4 people, 8 hours per day, 5 days per week ) | 1.25 weeks |


# Estimate by activity decomposition

|         Activity name    | Estimated effort  (person days)  |             
|:------------:|:---------------------------:|
| Requirements | 34% * 25.1 =  8.53 |
| Design       | 12% * 25.1 =  3.01 |
| Coding       | 23% * 25.1 =  5.77 |
| Testing      | 31% * 25.1 =  7.78 |


### Gantt Chart
```plantuml
project starts the 2018/03/13
-- Phase 1 --
[Requirements] lasts 29 days
-- Phase 2 --
[Design] lasts 13 days
-- Phase 3 --
[Coding] lasts 34 days
[Testing] lasts 27 days
[Design] starts at [Requirements]'s end
[Coding] starts at [Design]'s end
[Testing] starts 7 days after [Coding]'s start
[Requirements] is colored in Orange/FireBrick 
[Design] is colored in GreenYellow/SeaGreen
[Coding] is colored in SkyBlue/Navy
[Testing] is colored in Orchid/Indigo
```
