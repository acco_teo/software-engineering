# Inspection Document

Authors: MMMA group

Date: 08/04/2019

Version: 1.0.1

# Summary
| | __Description__ |
|-|-|
| __Date__ | 01/04/2019 |
| __Place__| Politecnico di Torino |
| __Object of Inspection__| [Requirements document](https://git-softeng.polito.it/se-2019/group-03/LaTazza/blob/branch-requirements/templates/RequirementsTemplate.md) |
| __Participants and roles:__| <br><ul><li>Moderator: "Matteo Costa"</li><li>Author: "Alessio Ciarcià"</li><li>Reader: "Marco Rossi"</li><li>Reader: "Paolo Accornero"</li></ul> |
| __Start Time__ | 8:00 AM |
| __End Time__ |  12:00 PM |

<br>

# Problems encountered 

| Problem id | Location (where in the document) | Problem description | Status (open, assigned, closed) | Type (see note) | Gravity (minor, normal, major)| 
| :--------: | -------------------------------- | ------------------- | :-----------------------------: | --------------- | :---------------------------: |
| __1__ | NFR          | [Missing reference of FR in NFRs](#4)              | open | Incompleteness | normal |
| __2__ | UCD          | [Missing create an account for an Employee](#5)    | open | Omission       | major |
| __3__ | Glossary     | [Missing expiration date of capsule](#6)           | open | Incompleteness | normal |
| __4__ | Glossary     | [Missing Supplier](#7)                             | open | Omission       | major |
| __5__ | UC           | [Missing create an account for an Employee](#8)    | open | Ambiguity      | major |
| __6__ | All          | [Missing transaction receipt](#9)                  | open | Omission       | major |
| __7__ | All          | [Missing definition of APIs with Supplier](#10)    | open | Omission       | major |
| __8__ | UC, Scenario | [Employee must be recognizable by manager](#11)    | open | Incompleteness | minor |
| __9__ | All          | [Missing Storage & Recovery System](#12)           | open | Omission       | major |
