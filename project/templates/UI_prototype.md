Authors: MMMA group

Date: 08/04/2019

Version: 1.0.0

- [Login](#login)
- [Home](#home)
- [Sell capsules](#sell-capsules)
- [Manage employee account](#manage-employee-account)
- [Manage inventory](#manage-inventory)
- [Transaction history](#transaction-history)
- [Manage inventory](#manage-inventory)
- [System settings](#system-settings)

# Login

![Login](./balsamiq/00_Login.png)

# Home

![Home](./balsamiq/01_Home.png)

# Sell capsules

![Sell Capsules](./balsamiq/02_Sell Capsules 01.png)
<img src="./balsamiq/02_Sell Capsules 02_a.png" alt="Sell Capsules" width="500" height="375"/>
<img src="./balsamiq/02_Sell Capsules 02_b.png" alt="Sell Capsules" width="500" height="375"/>
![Sell Capsules](./balsamiq/02_Sell Capsules 03.png)

# Manage employee-account

![Manage Account](./balsamiq/03_Manage Employee Account 01.png)
<img src="./balsamiq/03_Manage Employee Account 02_a.png" alt="Manage Account" width="500" height="375"/>
<img src="./balsamiq/03_Manage Employee Account 02_b.png" alt="Manage Account" width="500" height="375"/>
![Manage Account](./balsamiq/03_Manage Employee Account 03.png)

# Manage inventory

![Manage Inventory](./balsamiq/04_Manage Inventory 01.png)
<img src="./balsamiq/04_Manage Inventory 02_a.png" alt="Manage Inventory" width="500" height="375"/>
<img src="./balsamiq/04_Manage Inventory 02_b.png" alt="Manage Inventory" width="500" height="375"/>
![Manage Inventory](./balsamiq/04_Manage Inventory 03.png)

# Transaction history

![Transaction History](./balsamiq/05_Transaction History 01.png)
<img src="./balsamiq/05_Transaction History 02_a.png" alt="Transaction History" width="500" height="375"/>
<img src="./balsamiq/05_Transaction History 02_b.png" alt="Transaction History" width="500" height="375"/>
![Transaction History](./balsamiq/05_Transaction History 03.png)

# System settings

![System Settings](./balsamiq/06_System Settings 01.png)
![System Settings](./balsamiq/06_System Settings 02.png)