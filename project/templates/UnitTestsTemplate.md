# Unit Testing Documentation template

Authors: MMMA Group

Date: 18/05/2019

Version: 1.0.0

# Contents

- [Black Box Unit Tests](#black-box-unit-tests)
    * [sellCapsules](#class-dataimpl-method-sellcapsules)
    * [sellCapsulesToVisitor](#class-dataimpl-method-sellcapsulestovisitor)
    * [rechargeAccount](#class-dataimpl-method-rechargeaccount)
    * [buyBoxes](#class-dataimpl-method-buyboxes)
    * [getEmployeeReport](#class-dataimpl-method-getemployeereport)
    * [getReport](#class-dataimpl-method-getreport)
    * [createBeverage](#class-dataimpl-method-createbeverage)
    * [updateBeverage](#class-dataimpl-method-updatebeverage)
    * [getBeverageName](#class-dataimpl-method-getbeveragename)
    * [getBeverageCapsulesPerBox](#class-dataimpl-method-getbeveragecapsulesperbox)
    * [getBeverageBoxPrice](#class-dataimpl-method-getbeverageboxprice)
    * [getBeveragesId](#class-dataimpl-method-getbeveragesid)
    * [getBeverages](#class-dataimpl-method-getbeverages)
    * [getBeverageCapsules](#class-dataimpl-method-getbeveragecapsules)
    * [createEmployee](#class-dataimpl-method-createemployee)
    * [updateEmployee](#class-dataimpl-method-updateemployee)
    * [getEmployeeName](#class-dataimpl-method-getemployeename)
    * [getEmployeeSurname](#class-dataimpl-method-getemployeesurname)
    * [getEmployeeBalance](#class-dataimpl-method-getemployeebalance)
    * [getEmployeesId](#class-dataimpl-method-getemployeesid)
    * [getEmployees](#class-dataimpl-method-getemployees)
    * [getBalance](#class-dataimpl-method-getbalance)
    * [reset](#black-box-unit-tests)
    * [complexTests](#class-dataimpl-method-complextests)
    * [readEmployees](#class-storageimpl-method-reademployees)
    * [readTransactions](#class-storageimpl-method-readtransactions)
    * [readBeverages](#class-storageimpl-method-readbeverages)
    * [readBalance](#class-storageimpl-method-readbalance)
    * [writeEmployees](#class-storageimpl-method-writeemployees)
    * [writeTransactions](#class-storageimpl-method-writetransactions)
    * [writeBeverages](#class-storageimpl-method-writebeverages)
- [White Box Unit Tests](#white-box-unit-tests)
- [Integration Tests](#integration-tests)


# Black Box Unit Tests


## Class *DataImpl*
### **Class *DataImpl* - method *getBeverageCapsules***

**Criteria for method *getBeverageCapsules*:**

 - Beverage id

**Predicates for method *getBeverageCapsules*:**

|     Criteria			|       Predicate       |
| --------------------	| --------------------- |
| Beverage id			| 	in set 		        |
|						| 	not in set	        |
|						| 	null 			    |

**Combination of predicates**:

|    Beveraged Id      | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
| id is in the set     |        V        | buyBoxes(b_id1, 20); <br> assert(20 * getBeverageCapsulesPerBox(b_id1) == getBeverageCapsules(b_id1));  <br> sellCapsules(b_id1, 10);  <br> assert(20 * getBeverageCapsulesPerBox(b_id1) - 10 == getBeverageCapsules(b_id1));   | it.polito.latazza.data.testGetBeverageCapsules |
| id in not in the set or null|        I        | getBeverageCapsulesPerBox(wrongId) -> BeverageException <br> getBeverageCapsulesPerBox(null) -> BeverageException  |it.polito.latazza.data.testGetBeverageCapsules |


### **Class *DataImpl* - method *GetBeveragesId***

**Criteria for method *GetBeveragesId*:**

 - List Beverages

**Predicates for method *GetBeveragesId*:**

|     Criteria           |       Predicate        |
| ---------------------- | ---------------------- |
| List Beverages         |  is empty              |
|                        |  is not empty          |

**Combination of predicates**:

|    List Beverages    | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
|      is not empty    |        V        | b1 = createBeverage("name1", 2, 10);<br> b2 = createBeverage("name2", 5, 15);<br> list = getBeveragesId();<br> assert(2 == list.size());<br> assert(list.contains(b1))<br> assert(list.contains(b2))  | it.polito.latazza.data.testGetBeveragesId |
|  	   is empty        |        V        | assert(0 == list.size()) | it.polito.latazza.data.testGetBeveragesId |


### **Class *DataImpl* - method *getBeverageBoxPrice***

**Criteria for method *getBeverageBoxPrice*:**

 - Beverage id

**Predicates for method *getBeverageBoxPrice*:**

|     Criteria			|       Predicate       |
| --------------------	| --------------------- |
| Beverage id			| 	in set 		        |
|						| 	not in set	        |
|						| 	null 			    |

**Combination of predicates**:

|      Beverage id     | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
|      is in the set   |        V        | b1 = createBeverage("name1", 2, 10); b2 = createBeverage("name2", 5, 15);<br> assert(getBeverageBoxPrice(b1) == 10); <br> assert(getBeverageBoxPrice(b2) == 15);   | it.polito.latazza.data.testGetBeverageBoxPrice |
|  is not in the set   |        I        | getBeverageBoxPrice(null) -> BeverageException;<br> getBeverageBoxPrice(wrongId) -> BeverageException  | it.polito.latazza.data.testGetBeverageBoxPrice |


### **Class *DataImpl* - method *getBeverageCapsulesPerBox***

**Criteria for method *getBeverageCapsulesPerBox*:**

 - Beverage id

**Predicates for method *getBeverageCapsulesPerBox*:**

|     Criteria			|       Predicate       |
| --------------------	| --------------------- |
| Beverage id			| 	in set 		        |
|						| 	not in set	        |
|						| 	null     			|

**Combination of predicates**:

|      Beverage id     | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
|      is in the set   |        V        | b1 = createBeverage("name1", 2, 10); b2 = createBeverage("name2", 5, 15);<br> assert(getBeverageCapsulesPerBox(b1) == 2); <br> assert(getBeverageCapsulesPerBox(b2) == 5);   | it.polito.latazza.data.testGetBeverageName |
|  is not in the set   |        I        | getBeverageCapsulesPerBox(null) -> BeverageException;<br> getBeverageCapsulesPerBox(wrongId) -> BeverageException  | it.polito.latazza.data.testGetBeverageCapsulesPerBox |


### **Class *DataImpl* - method *getBeverageName***

**Criteria for method *getBeverageName*:**

 - Beverage id

**Predicates for method *getBeverageName*:**

|     Criteria			|       Predicate       |
| --------------------	| --------------------- |
| Beverage id			| 	in set 	        	|
|						| 	not in set      	|
|						| 	null    			|

**Combination of predicates**:

|      Beverage id     | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
|      is in the set   |        V        | b1 = createBeverage("name1", 2, 10); b2 = createBeverage("name2", 5, 15);<br> assert(getBeverageName(b1) == "name1");assert(getBeverageName(b2) == "name2");   | it.polito.latazza.data.testGetBeverageName |
|  is not in the set   |        I        | getBeverageName(null) -> BeverageException;<br> getBeverageName(wrongId) -> BeverageException  | it.polito.latazza.data.testGetBeverageName |


### **Class *DataImpl* - method *getEmployeeSurname***

**Criteria for method *getEmployeeSurname*:**

 - Employee id

**Predicates for method *getEmployeeSurname*:**

|     Criteria           |       Predicate       |
| ---------------------- | --------------------- |
| Employee id            |  id is in set         |
|                        |  id is not in set     |

**Combination of predicates**:

|      Employee id     | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
|      is in the set   |        V        | e1 = createEmployee("name1 ", "surname1"); e2 = createEmployee("name2", "surname2");<br> assert(getEmployeeSurname(e1) == "surname1");assert(getEmployeeSurname(e2) == "surname2");   | it.polito.latazza.data.testGetEmployeeSurname |
|  is not in the set   |        I        | getEmployeeSurname(null) -> EmployeeException;<br> getEmployeeSurname(wrongId) -> EmployeeException  | it.polito.latazza.data.testGetEmployeeSurname |


### **Class *DataImpl* - method *getEmployeeName***

**Criteria for method *getEmployeeName*:**

 - Employee id

**Predicates for method *getEmployeeName*:**

|     Criteria           |       Predicate       |
| ---------------------- | --------------------- |
| Employee id            |  id is in set         |
|                        |  id is not in set     |

**Combination of predicates**:

|      Employee id     | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
|      is in the set   |        V        | e1 = createEmployee("name1 ", "surname1"); e2 = createEmployee("name2", "surname2");<br> assert(getEmployeeName(e1) == "name1");assert(getEmployeeName(e2) == "name2");   | it.polito.latazza.data.testGetEmployeeName |
|  is not in the set   |        I        | getEmployeeName(null) -> EmployeeException;<br> getEmployeeName(wrongId) -> EmployeeException  | it.polito.latazza.data.testGetEmployeeName |


### **Class *DataImpl* - method *getEmployeesId***

**Criteria for method *getEmployeesId*:**

 - Employees

**Predicates for method *getEmployeesId*:**

|     Criteria           |       Predicate       |
| ---------------------- | --------------------- |
| Employees              |  is empty             |
|                        |  is not empty         |

**Combination of predicates**:

|      Employees       | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
|      is not empty    |        V        | e1 = createEmployee("e1", "aa"); e2 = createEmployee("e2", "bb"); -> list = getEmployeesId(); <br> assertEquals(2, list.size()); assert(list.contain(e1)); assert(list.contain(e2));   | it.polito.latazza.data.testGetEmployeesId |
|        is empty      |        V        | assertEquals(0, list.size());                                                              | it.polito.latazza.data.testGetEmployeesId |


### **Class *DataImpl* - method *getEmployees***

**Criteria for method *getEmployees*:**

 - Employees

**Predicates for method *getEmployees*:**

|     Criteria           |       Predicate       |
| ---------------------- | --------------------- |
| Employees              |  is empty             |
|                        |  is not empty         |

**Combination of predicates**:

|      Employees       | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
|      is not empty    |        V        | e1 = createEmployee("e1", "aa"); e2 = createEmployee("e2", "bb"); -> map = data.getEmployees(); <br> assertEquals(2, map.size()); assertEquals("e2 aa", map.get(e1)); assertEquals("e1 bb", map.get(e2));   | it.polito.latazza.data.testGetEmployees |
|        is empty      |        V        | assertEquals(0, map.size());                                                               | it.polito.latazza.data.testGetEmployees |


### **Class *DataImpl* - method *getBalance***

**Criteria for method *getBalance*:**

 - Latazza balance
 - Recharge account
 - buy boxes

**Predicates for method *getBalance*:**

|     Criteria          |       Predicate       |
| --------------------- | --------------------- |
| Latazza balance       |         0             |
|                       |         > 0           |
| Recharge account x    |         yes           |
|                       |         no            |
| buy boxes x           |         yes           |
|                       |         no            |

**Combination of predicates**:

| Latazza balance | Recharge account |   Buy Boxes   |Valid / Invalid|                  Description of the test case                                                                |         JUnit test case              |
|:---------------:|:----------------:|:-------------:|:-------------:|:------------------------------------------------------------------------------------------------------------:|:------------------------------------:|
|     x = any     |    yes           |      no       |       V       | rechargeAccount(id, 10) assert(getBalance() = x + 10);                                                       | it.polito.latazza.data.testGetBalance|
|     x = any     |    no            |      no       |       V       | assert(getBalance() = x );                                                                                   | it.polito.latazza.data.testGetBalance|
|x = any > price-10|    yes          |      yes      |       V       | rechargeAccount(id, 10); assert(getBalance() = x + 10); buyBoxes(price)-> assert(getBalance = x + 10 - price) | it.polito.latazza.data.testGetBalance|
| x = any > price |    no            |      yes      |       V       | buyBoxes(price)-> assert(getBalance = x + - price)                                                        | it.polito.latazza.data.testGetBalance|


### **Class *DataImpl* - method *reset***

**Criteria for method *reset*:**

- Latazza balance
- Employees
- Beverages
- Reports

**Predicates for method *reset*:**

|     Criteria    | Predicate |
| ----------------| --------- |
| Latazza balance | empty     |
|                 | not empty |
| Employees       | empty     |
|                 | not empty |
| Beverages       | empty     |
|                 | not empty |
| Reports         | empty     |
|                 | not empty |

**Combination of predicates**:

| Latazza balance | Employees | Beverages | Reports |Valid / Invalid| Description of the test case | JUnit test case |
|:---------------:|:---------:|:---------:|:-------:|:-------------:|:------------------------------------------------------------------------------------------------------------:|:------------------------------------:|
| any | any | any | any | V | reset() -> assert(beverage.isEmpty); assert(reports.isEmpty); assert(employee.isEmpty); assert(balance == 0) | it.polito.latazza.data.testReset |


### **Class *DataImpl* - method *sellCapsules***

**Criteria for method *sellCapsules*:**

 - Capsules amount
 - Employee Id
 - Beverage Id
 - FromAccount flag TODO
 - Employee Balance After

**Predicates for method *sellCapsules*:**

|     Criteria           |       Predicate       |
| ---------------------- | --------------------- |
| Capsules amount        | >  available          |
|                        | <= available && >= 0  |
|                        | < 0                   |
| Employee Id            | Id not in IDs set     |
|                        | Id in IDs set         |
|                        | null                  |
| Beverage Id            | Id not in IDs set     |
|                        | Id in IDs set         |
|                        | null                  |
| FromAccount Flag       | true                  |
|                        | false                 |
|                        | null                  | TODO
| Employee Balance After | < Integer.MIN_VALUE   |
|                        | >= Integer.MIN_VALUE  |

**Boundaries**:

| Criteria | Boundary values |
| ---------------- | --------------- |
| Capsules amount  | 0               |

**Combination of predicates**:

|    Capsules amount   |        Employee Id        |        Beverage Id        | FromAccount Flag | Employee Balance After | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------------:|:-------------------------:|:-------------------------:|:----------------:|:----------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
|      >  available    |             any           |            any            |        any       |  >= Integer.MIN_VALUE  |        I        | sellCapsules(1, 1, 1, true) -> NotEnoughCapsules                                           | it.polito.latazza.data.testSellCapsules |
|          < 0         |             any           |            any            |        any       |  >= Integer.MIN_VALUE  |        I        | sellCapsules(1, 1, -1, true) -> BeverageException                                          | it.polito.latazza.data.testSellCapsules |
|          any         |     Id not in IDs set     |            any            |        any       |  >= Integer.MIN_VALUE  |        I        | createEmployee(Name1, Surname1);<br />sellCapsules(100, 1, 1, true) -> EmployeeException   | it.polito.latazza.data.testSellCapsules |
|          any         |            null           |            any            |        any       |  >= Integer.MIN_VALUE  |        I        | sellCapsules(null, 1, 1, true) -> EmployeeException                                        | it.polito.latazza.data.testSellCapsules |
|          any         |             any           |     Id not in IDs set     |        any       |  >= Integer.MIN_VALUE  |        I        | createBeverage(Name1, 2, 10);<br />sellCapsules(1, 100, 1, true) -> BeverageException      | it.polito.latazza.data.testSellCapsules |
|          any         |             any           |           null            |        any       |  >= Integer.MIN_VALUE  |        I        | sellCapsules(1, null, 1, true) -> BeverageException                                        | it.polito.latazza.data.testSellCapsules |
|          any         |             any           |            any            |       null       |  >= Integer.MIN_VALUE  |        I        | sellCapsules(1, 1, 1, null) -> EmployeeException                                           | it.polito.latazza.data.testSellCapsules |
|          any         |             any           |            any            |        any       |  < Integer.MIN_VALUE   |        I        | try to push Employee balance below Integer.MINVALUE                                        | it.polito.latazza.data.testSellCapsules |
| <= available && >= 0 |       Id in IDs set       |       Id in IDs set       |       true       |  >= Integer.MIN_VALUE  |        V        | sellCapsules(1, 1, 1, true)                                                                | it.polito.latazza.data.testSellCapsules |
| <= available && >= 0 |       Id in IDs set       |       Id in IDs set       |      false       |  >= Integer.MIN_VALUE  |        V        | sellCapsules(1, 1, 1, false)                                                               | it.polito.latazza.data.testSellCapsules |


### **Class *DataImpl* - method *sellCapsulesToVisitor***

**Criteria for method *sellCapsulesToVisitor*:**

 - Capsules amount
 - Beverage Id

**Predicates for method *sellCapsulesToVisitor*:**

|     Criteria           |       Predicate       |
| ---------------------- | --------------------- |
| Capsules amount        | >  available          |
|                        | <= available && >= 0  |
|                        | < 0                   |
| Beverage Id            | Id not in IDs set     |
|                        | Id in IDs set         |
|                        | null                  |

**Boundaries**:

| Criteria | Boundary values |
| ---------------- | --------------- |
| Capsules amount  | 0               |

**Combination of predicates**:

|    Capsules amount   |        Beverage Id        | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------------:|:-------------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
|      >  available    |            any            |        I        | sellCapsulesToVisitor(1, 1) -> NotEnoughCapsules                                           | it.polito.latazza.data.testSellCapsules |
|          < 0         |            any            |        I        | sellCapsulesToVisitor(1, -1) -> BeverageException                                          | it.polito.latazza.data.testSellCapsules |
|          any         |     Id not in IDs set     |        I        | createBeverage(Name1, 2, 10);<br />sellCapsulesToVisitor(100, 1) -> BeverageException      | it.polito.latazza.data.testSellCapsules |
|          any         |           null            |        I        | sellCapsulesToVisitor(null, 1) -> BeverageException                                        | it.polito.latazza.data.testSellCapsules |
| <= available && >= 0 |       Id in IDs set       |        V        | createBeverage(Name1, 2, 10);<br />sellCapsulesToVisitor(1, 1)                             | it.polito.latazza.data.testSellCapsules |


### **Class *DataImpl* - method *rechargeAccount***

**Criteria for method *rechargeAccount*:**

 - Employee  id
 - Cash amountInCents
 - Employee Balance After

**Predicates for method *rechargeAccount*:**

|        Criteria        |       Predicate      |
| ---------------------- | -------------------- |
| Cash amountInCents     | > 0        	|
|                        | = 0			|
|                        | < 0                  |
| Employee Id            | Id not in IDs set    |
|                        | Id in IDs set        |
|                        | null                 |
| Employee Balance After | <= Integer.MAX_VALUE |
|                        | > Integer.MAX_VALUE  |

**Boundaries**:

|      Criteria      | Boundary values |
| ------------------ | --------------- |
| Cash amountInCents | 0               |

**Combination of predicates**:

|  Cash amountInCents  |        Employee Id        | Employee Balance After | Valid / Invalid |                                Description of the test case                                |                JUnit test case             |
|:--------------------:|:-------------------------:|:----------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:------------------------------------------:|
|         x >= 0       |       Id in IDs set       |  <= Integer.MAX_VALUE  |        V        | rechargeAccount(id,x) -> assert(x==getBalance()==getEmplBalance(id))                       | it.polito.latazza.data.testRechargeAccount |
|          any         |     Id not in IDs set     |  <= Integer.MAX_VALUE  |        I        | rechargeAccount(wrongId,0) -> EmployeeException                                            | it.polito.latazza.data.testRechargeAccount |
|          any         |           null            |  <= Integer.MAX_VALUE  |        I        | rechargeAccount(null,0) -> EmployeeException                                               | it.polito.latazza.data.testRechargeAccount |
|           -1         |            any            |  <= Integer.MAX_VALUE  |        I        | rechargeAccount(id,-1) -> EmployeeException                                                | it.polito.latazza.data.testRechargeAccount |
|          any         |       Id in IDs set       |   > Integer.MAX_VALUE  |        I        | rechargeAccount(id,1) -> rechargeAccount(id,Integer.MAXVALUE) -> EmployeeExceptionxception | it.polito.latazza.data.testRechargeAccount |


### **Class *DataImpl* - method *buyBoxes***

**Criteria for method *buyBoxes*:**

 - BoxId
 - NumOfBoxes
 - LaTazza's Balance

**Predicates for method *buyBoxes*:**

|     Criteria     |       Predicate       |
| ---------------- | --------------------- |
|    NumOfBoxes    | > 0        		   |
|                  | = 0  				   |
|                  | < 0                   |
|     Balance      | enough                |
|                  | not enough            |
|      BoxId       | Id not in IDs set     |
|                  | Id in IDs set         |
|                  | null                  |

**Boundaries**:

| Criteria | Boundary values |
| ---------------- | --------------- |
|    NumOfBoxes    | 0               |

**Combination of predicates**:

|      NumOfBoxes      |            Balance        |        BoxId      | Valid / Invalid |                                Description of the test case                                |                JUnit test case             |
|:--------------------:|:-------------------------:|:-----------------:|:---------------:|:------------------------------------------------------------------------------------------:|:------------------------------------------:|
|           > 0        |          not enough       |   Id in IDs set   |        I        | buyBoxes(boxId, 10) -> NotEnoughBalance                                                    | it.polito.latazza.data.testBuyBoxes        |
|           > 0        |           enough          |   Id in IDs set   |        V        | buyBoxes(boxId, 10) -> assert(numBox == 10); buyBoxes(boxId, 15) -> assert(numBox == 25)   | it.polito.latazza.data.testBuyBoxes        |
|            0         |             any           |   Id in IDs set   |        V        | buyBoxes(boxId, 0) -> assert(numBox == 0)                                                  | it.polito.latazza.data.testBuyBoxes        |
|           any        |             any           | Id not in IDs set |        I        | buyBoxes(wrongId, 15) -> BeverageException                                                 | it.polito.latazza.data.testBuyBoxes        |
|           < 1        |             any           |         any       |        I        | buyBoxes(id, -1) -> BeverageException                                                      | it.polito.latazza.data.testBuyBoxes        |
|           any        |             any           |        null       |        I        | buyBoxes(null, 1) -> BeverageException                                                     | it.polito.latazza.data.testBuyBoxes        |


### **Class *DataImpl* - method *createBeverage***

**Criteria for method *createBeverage*:**

 - Beverage Name
 - CapsulesPerBox
 - BoxPrice

**Predicates for method *createBeverage*:**

|    Criteria    |             Predicate               |
| -------------- | ----------------------------------- |
|  Beverage Name | Not-empty string, without semicolon |
|                | Duplicate name  		               |
|                | Empty string			|
|                | null                                |
|                | String with semicolon               |
| CapsulesPerBox | > 0                                 |
|                | <= 0                                |
|                | null                                |
|    BoxPrice    | > 0                                 |
|                | <= 0                                |
|                | null                                |

**Boundaries**:

|    Criteria    | Boundary values |
| -------------- | --------------- |
| CapsulesPerBox | 0               |
|    BoxPrice    | 0               |

**Combination of predicates**:

| Beverage Name |    CapsulesPerBox   |  BoxPrice  | Valid / Invalid |                                Description of the test case                                |                JUnit test case             |
|:-------------:|:-------------------:|:----------:|:---------------:|:------------------------------------------------------------------------------------------:|:------------------------------------------:|
|       ""      |         any         |    any     |        I        |  createBeverage("", 2, 10) -> BeverageException                                          | it.polito.latazza.data.testCreateBeverage |
|     null      |         any         |    any     |        I        |  createBeverage(null, 2, 10) -> BeverageException                                        | it.polito.latazza.data.testCreateBeverage |
|  "Name;Test"  |         any         |    any     |        I        |  createBeverage("Semicolon;Name", 2, 10) -> BeverageExcpetion                            | it.polito.latazza.data.testCreateBeverage |
|      any      |        <= 0         |    any     |        I        |  createBeverage("ZeroCapsulesPerBox", 0, 10) -> BeverageExcpetion                        | it.polito.latazza.data.testCreateBeverage |
|      any      |        null         |    any     |        I        |  createBeverage("NullCapsulesPerBox", null, 10) -> BeverageException                     | it.polito.latazza.data.testCreateBeverage |
|      any      |         any         |    <= 0    |        I        |  createBeverage("ZeroBoxPrice", 2, 0) -> BeverageException                               | it.polito.latazza.data.testCreateBeverage |
|      any      |         any         |   null     |        I        |  createBeverage("NullBoxPrice", 2, null) -> BeverageException                            | it.polito.latazza.data.testCreateBeverage |
|    "Test"     |         > 0         |    > 0     |        V        |  createBeverage("Name_1", 2, 10);<br>createBeverage("Name_2", 5, 15);<br>createBeverage("Name_1", 10, 10);<br>assertNotEquals(id1, id2);<br>assertNotEquals(id1, id3);<br>assertNotEquals(id2, id3);<br>| it.polito.latazza.data.testCreateBeverage |


### **Class *DataImpl* - method *createEmployee***

**Criteria for method *CreateEmployee*:**

 - Employee Name
 - Employee Surname

**Predicates for method *createEmployee*:**

|    Criteria      |             Predicate                  |
| ---------------- | -------------------------------------- |
| Employee Name    | Not-empty string, without semicolon    |
|                  | Duplicate name  		                |
|                  | Empty string  		                    |
|                  | null                                   |
|                  | String with semicolon                  |
| Employee Surname | Not-empty string, without semicolon    |
|                  | Duplicate surname  		            |
|                  | Empty string  		                    |
|                  | null                                   |
|                  | String with semicolon                  |

**Combination of predicates**:

|  Employee Name   |   Employee Surname  | Valid / Invalid |                                Description of the test case                                |                JUnit test case             |
|:----------------:|:-------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:------------------------------------------:|
|     null         |         any         |        I        | createEmployee(null, "NullName") -> EmployeeException                                      | it.polito.latazza.data.testCreateEmployee  |
|    empty         |         any         |        I        | createEmployee("", "EmptyName") -> EmployeeException                                       | it.polito.latazza.data.testCreateEmployee  |
| "Semicolon;Name" |         any         |        I        | createEmployee("Semicolon;Name", "Surname_1") -> EmployeeException                         | it.polito.latazza.data.testCreateEmployee  |
|      any         |        null         |        I        | createEmployee("NullSurname", null) -> EmployeeException                                   | it.polito.latazza.data.testCreateEmployee  |
|      any         |        empty        |        I        | createEmployee("EmptySurname", "") -> EmployeeException                                    | it.polito.latazza.data.testCreateEmployee  |
|      any         | "Semicolon;Surname" |        I        | createEmployee("Name_1", "Semicolon;Surname") -> EmployeeException                         | it.polito.latazza.data.testCreateEmployee  |
|    "Name1"       |     "Surname1"      |        V        | id1 = createEmployee("Name_1", "Surname_1") -> assertEquals(0, getEmployeeBalance(id1))    | it.polito.latazza.data.testCreateEmployee  |
|    "Name1"       |     "Surname1"      |        V        | id2 = createEmployee("Name_1", "Surname_1") -> assertEquals(0, getEmployeeBalance(id2))    | it.polito.latazza.data.testCreateEmployee  |


### **Class *DataImpl* - method *GetEmployeeReport***

**Criteria for method *GetEmployeeReport*:**

 - Employee Id
 - Report startDate
 - Report endDate

**Predicates for method *GetEmployeeReport*:**

|    Criteria      |             Predicate                  |
| ---------------- | -------------------------------------- |
| Employee Id      | Id not in IDs set	|
|	| Id in IDs set |
|	| null |
| Report startDate | startDate < endDate |
|	| startDate > endDate |
|	| startDate = endDate |
|	| null |
| Report endDate   | endDate > startDate |
|	| endDate < startDate |
|	| startDate = endDate |  
|	| null |

**Combination of predicates**:

|  EmployeeId  |   startDate  |   endDate   | Valid / Invalid |                             Description of the test case                            |                JUnit test case            |
|:------------:|:------------:|:-----------:|:---------------:|:-----------------------------------------------------------------------------------:|:-----------------------------------------:|
|  not in set  |      any     |    any      |        I        |  EmployeeException | it.polito.latazza.data.testGetEmployeeReport |
|     null     |      any     |    any      |        I        |  EmployeeException | it.polito.latazza.data.testGetEmployeeReport |
|     any      |      null    |    any      |        I        |  getEmployeeReport(newId1, null, new Date(1)) -> DateException | it.polito.latazza.data.testGetEmployeeReport |
|     any      |      any     |   null      |        I        |  getEmployeeReport(newId1, new Date(1), null) -> DateException | it.polito.latazza.data.testGetEmployeeReport |
|    in set    |   > endDate  |    any      |        I        |  getEmployeeReport(newId1, tomorrow, new Date(1)) -> DateException | it.polito.latazza.data.testGetEmployeeReport |
|    in set    |   = endDate  | = startDate |        I        | assertEquals(0, report.size()) | it.polito.latazza.data.testGetEmployeeReport |
|    in set    |      any     | > startDate |        V        | assertTrue(report.get(0).contains("RECHARGE " + getEmployeeName(e_id1) + " " + getEmployeeSurname(e_id1) + " 1.30€")); <br> assertTrue(report.get(1).contains("BALANCE " + getEmployeeName(e_id1) + " " + getEmployeeSurname(e_id1) + " " + getBeverageName(b_id1) + " 2")); <br> assertTrue(report.get(3).contains("CASH " + getEmployeeName(e_id1) + " " + getEmployeeSurname(e_id1) + " " + getBeverageName(b_id1) + " 2")); | it.polito.latazza.data.testGetEmployeeReport |                                                           


### **Class *DataImpl* - method *GetReport***

**Criteria for method *GetReport*:**

 - Report startDate
 - Report endDate

**Predicates for method *GetReport*:**

|    Criteria       |       Predicate           |
| ----------------- | ------------------------- |
| Report startDate  | startDate < endDate       |
| 	                | startDate > endDate       |
|	                | startDate = endDate       |
|	                | null                      |
| Report endDate    | endDate > startDate       |
|               	| endDate < startDate       |
|	                | startDate = endDate       |  
|	                | null                      |

**Combination of predicates**:

|   startDate  |   endDate   | Valid / Invalid |                             Description of the test case                           |                JUnit test case            |
|:------------:|:-----------:|:---------------:|:----------------------------------------------------------------------------------:|:-----------------------------------------:|
|      null    |    any      |        I        | getReport(null, new Date(1)) -> DateException 										| it.polito.latazza.data.testGetReport |
|      any     |   null      |        I        | getReport(new Date(1), null) -> DateException 										| it.polito.latazza.data.testGetReport |
|   > endDate  |    any      |        I        | getReport(tomorrow, new Date(1)) -> DateException 									| it.polito.latazza.data.testGetReport |
|   = endDate  | = startDate |        I        | assertEquals(0, report.size()) 													| it.polito.latazza.data.testGetReport |
|      any     | > startDate |        V        | assertTrue(report.get(0).contains("RECHARGE " + getEmployeeName(e_id1) + " " + getEmployeeSurname(e_id1) + " 1.30€"));<br />>assertTrue(report.get(2).contains("BUY " + getBeverageName(b_id1) + " 2"));<br />>assertTrue(report.get(4).contains("BALANCE " + getEmployeeName(e_id1) + " " + getEmployeeSurname(e_id1) + " " + getBeverageName(b_id1) + " 2"));<br />>assertTrue(report.get(8).contains("CASH " + getEmployeeName(e_id1) + " " + getEmployeeSurname(e_id1) + " " + getBeverageName(b_id1) + " 2"));<br />>assertTrue(report.get(9).contains("VISITOR " + getBeverageName(b_id2) + " 1")); | it.polito.latazza.data.testGetReport |


### **Class *DataImpl* - method *updateEmployee***

**Criteria for method *updateEmployee*:**
 - Employee Id
 - Employee Name
 - Employee Surname

**Predicates for method *updateEmployee*:**

|    Criteria      |             Predicate                  |
| ---------------- | -------------------------------------- |
| Employee Id	   | null						            |
|				   | not in set				                |
|				   | in set					                |
| Employee Name    | Not-empty string, without semicolon    |
|                  | Duplicate name                         |
|                  | Empty string  		                    |
|                  | null                                   |
|                  | String with semicolon                  |
| Employee Surname | Not-empty string, without semicolon    |
|                  | Duplicate surname                      |
|                  | Empty string  		                    |
|                  | null                                   |
|                  | String with semicolon                  |

**Combination of predicates**:

| Employee Id |   Employee Name  |  Employee Surname | Valid / Invalid |                         Description of the test case                           |                JUnit test case             |
|:-----------:|:----------------:|:-----------------:|:---------------:|:------------------------------------------------------------------------------:|:------------------------------------------:|
|   null      |     any          |         any       |        I        | updateEmployee(null, "NullId", "NullId") -> EmployeeException                  | it.polito.latazza.data.testUpdateEmployee  |
| not in set  |     null         |         any       |        I        | updateEmployee(newId, "NonExistingId", "NonExistingId") -> EmployeeException   | it.polito.latazza.data.testUpdateEmployee  |
|   in set    |     null         |         any       |        I        | updateEmployee(newId, "NonExistingId", "NonExistingId") -> EmployeeException   | it.polito.latazza.data.testUpdateEmployee  |
|   in set    |    empty         |         any       |        I        | updateEmployee(newId2, "", "EmptyName") -> EmployeeException                   | it.polito.latazza.data.testUpdateEmployee  |
|   in set    |  "Semicln;Name"  |         any       |        I        | updateEmployee(newId2, "Semicolon;Name", "Surname_1") -> EmployeeException     | it.polito.latazza.data.testUpdateEmployee  |
|   in set    |      any         |        null       |        I        | data.updateEmployee(newId2, "NullSurname", null) -> EmployeeExcpetion          | it.polito.latazza.data.testUpdateEmployee  |
|   in set    |      any         |        empty      |        I        | updateEmployee(newId2, "EmptySurname", "") -> EmployeeException | it.polito.latazza.data.testUpdateEmployee  |
|   in set    |      any         | "Semicln;Surname" |        I        | updateEmployee(newId2, "Name_1", "Semicolon;Surname") -> EmployeeException | it.polito.latazza.data.testUpdateEmployee  |
|   in set    |   "Name_1_2"     |   "Surname_1_2"   |        V        | updateEmployee(id1, "Name_1_2", "Surname_1_2");<br />assertEquals("Name_1_2", getEmployeeName(id1));<br />assertEquals("Surname_1_2", getEmployeeSurname(id1));| it.polito.latazza.data.testUpdateEmployee  |


### **Class *DataImpl* - method *getEmployeeBalance***

**Criteria for method *getEmployeeBalance*:**

 - Employee id

**Predicates for method *getEmployeeBalance*:**

|     Criteria      |  Predicate       |
| ----------------- | ---------------- |
| Employee id       |  null            |
| 			        |  in set          |
|                   |  not in set      |

**Combination of predicates**:

|      Employee id     | Valid / Invalid |                                Description of the test case                                  |             JUnit test case             |
|:--------------------:|:---------------:|:--------------------------------------------------------------------------------------------:|:---------------------------------------:|
|  null				   |        I        | getEmployeeBalance(null) -> EmployeeException 			                                    | it.polito.latazza.data.testGetEmployeeBalance |
|  not in set 		   |        I        | getEmployeeBalance(wrongId) -> EmployeeException			                                    | it.polito.latazza.data.testGetEmployeeBalance |
|  in the set 		   |        V        | assertEquals(Integer.valueOf(90), getEmployeeBalance(e_id1)) <br> assertEquals(Integer.valueOf(990), getEmployeeBalance(e_id2)) | it.polito.latazza.data.testGetEmployeeBalance |


### **Class *DataImpl* - method *updateBeverage***

**Criteria for method *updateBeverage*:**

 - Beverage Id
 - Beverage Name
 - CapsulesPerBox
 - BoxPrice

**Predicates for method *updateBeverage*:**

|    Criteria    |             Predicate                |
| -------------- | ------------------------------------ |
|  Beverage Id   | null						            |
| 			     | not in set				            |
|  				 | in set						        |
|  Beverage Name | Not-empty string, without semicolon  |
|                | Duplicate name  		                |
|                | Empty string  		                |
|                | null                                 |
|                | String with semicolon                |
| CapsulesPerBox | > 0                                  |
|                | <= 0                                 |
|                | null                                 |
|    BoxPrice    | > 0                                  |
|                | <= 0                                 |
|                | null                                 |

**Boundaries**:

|    Criteria    | Boundary values |
| -------------- | --------------- |
| CapsulesPerBox | 0               |
|    BoxPrice    | 0               |

**Combination of predicates**:

|  Beverage Id  | Beverage Name |    CapsulesPerBox   |  BoxPrice  | Valid / Invalid |               Description of the test case                 |                JUnit test case             |
|:-------------:|:-------------:|:-------------------:|:----------:|:---------------:|:----------------------------------------------------------:|:------------------------------------------:|
|    null       |      any      |         any         |    any     |        I        | -> BeverageException                                                     | it.polito.latazza.data.testUpdateBeverage |
|  not in set   |      any      |         any         |    any     |        I        | -> BeverageException                                                     | it.polito.latazza.data.testUpdateBeverage |
|   in set      |       ""      |         any         |    any     |        I        | updateBeverage(newId2, "", 2, 10) -> BeverageException                   | it.polito.latazza.data.testUpdateBeverage |
|   in set      |     null      |         any         |    any     |        I        | updateBeverage(newId2, null, 2, 10) -> BeverageException                 | it.polito.latazza.data.testUpdateBeverage |
|   in set      |  "Name;Test"  |         any         |    any     |        I        | updateBeverage(newId2, "Semicolon;Name", 2, 10) -> BeverageException     | it.polito.latazza.data.testUpdateBeverage |
|   in set      |      any      |        <= 0         |    any     |        I        | updateBeverage(newId2, "ZeroCapsulesPerBox", 0, 10) -> BeverageException | it.polito.latazza.data.testUpdateBeverage |
|   in set      |      any      |        null         |    any     |        I        | updateBeverage(newId2, "NullCapsulesPerBox", null, 10) -> BeverageException  | it.polito.latazza.data.testUpdateBeverage |
|   in set      |      any      |         any         |    <= 0    |        I        | updateBeverage(newId2, "ZeroBoxPrice", 2, 0) -> BeverageException            | it.polito.latazza.data.testUpdateBeverage |
|   in set      |      any      |         any         |   null     |        I        | updateBeverage(newId2, "NullBoxPrice", 2, null) -> BeverageException         | it.polito.latazza.data.testUpdateBeverage |
|   in set      |  "Name_1_2"   |         > 0         |    > 0     |        V        | updateBeverage(id1, "Name_1_2", 4, 20) <br />assertEquals("Name_1_2", name1);<br />assertEquals(Integer.valueOf(4), capsPerBox1);<br />assertEquals(Integer.valueOf(20), boxPrice1); | it.polito.latazza.data.testCreateBeverage |


### **Class *DataImpl* - method *getBeverages***

**Criteria for method *getBeverages*:**

 - Beverages

**Predicates for method *getBeverages*:**

|     Criteria           |       Predicate      |
| ---------------------- | -------------------- |
| Beverages Collection   |  empty               |
|                        |  not empty           |

**Combination of predicates**:

| Beverages Collection | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
|        empty         |        V        | rechargeAccount(e_id1, Integer.MAX_VALUE)   | it.polito.latazza.data.complexTests() |
|       not empty      |        V        | assertEquals(0, map.size());                                                               | it.polito.latazza.data.complexTests() |


### **Class *DataImpl* - method *complexTests***

**Criteria for method *rechargeAccount*:**

 - Employee's Balance

**Predicates for method *rechargeAccount*:**

|     Criteria           |       Predicate       |
| ---------------------- | --------------------- |
| Employee's Balance     | <= Integer.MAX_VALUE  |
|                        |  > Integer.MAX_VALUE  |

**Boundaries**:

|    Criteria    |  Boundary values  |
| -------------- | ----------------- |
|     Balance    | Integer.MAX_VALUE |

**Combination of predicates**:

|  Employee's Balance  | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
| <= Integer.MAX_VALUE |        V        | rechargeAccount(e_id1, Integer.MAX_VALUE);<br> assertEquals(Integer.MAX_VALUE, getEmployeeBalance(newEId2)); | it.polito.latazza.data.testGetBeverages |
|  > Integer.MAX_VALUE |        I        | rechargeAccount(newEId2, 1) -> EmployeeException                                                               | it.polito.latazza.data.testGetBeverages |

## Class *StorageImpl*
### **Class *StorageImpl* - method *readEmployees***

**Criteria for method *readEmployees*:**

 - Employee Storage file

**Predicates for method *readEmployees*:**

|        Criteria         |       Predicate       |
| ----------------------  | --------------------- |
|  Employee storage file  |        correct        |
|                         |      not correct      |
|                         |         empty         |

**Combination of predicates**:

| Employee storage file | Valid / Invalid |           Description of the test case                |             JUnit test case             |
|:---------------------:|:---------------:|:-----------------------------------------------------:|:---------------------------------------:|
|        correct        |        V        |  attempt to read a storage file with correct format   | it.polito.latazza.data.testReadEmployees |
|         empty         |        V        |         attempt to read an empty storage file         | it.polito.latazza.data.testReadEmployees |
|      not correct      |        I        |   attempt to read a storage file with wrong format    | it.polito.latazza.data.testReadEmployees |

### **Class *StorageImpl* - method *readBeverages***

**Criteria for method *readBeverages*:**

 - Beverage Storage file

**Predicates for method *readBeverages*:**

|        Criteria         |       Predicate       |
| ----------------------  | --------------------- |
|  Beverage storage file  |        correct        |
|                         |      not correct      |
|                         |         empty         |

**Combination of predicates**:

| Beverage storage file | Valid / Invalid |           Description of the test case                |             JUnit test case             |
|:---------------------:|:---------------:|:-----------------------------------------------------:|:---------------------------------------:|
|        correct        |        V        |  attempt to read a storage file with correct format   | it.polito.latazza.data.testReadBeverages |
|         empty         |        V        |         attempt to read an empty storage file         | it.polito.latazza.data.testReadBeverages |
|      not correct      |        I        |   attempt to read a storage file with wrong format    | it.polito.latazza.data.testReadBeverages |

### **Class *StorageImpl* - method *readTransactions***

**Criteria for method *readTransactions*:**

 - Transaction Storage file

**Predicates for method *readTransactions*:**

|          Criteria          |       Predicate       |
| -------------------------- | --------------------- |
|  Transaction storage file  |        correct        |
|                            |      not correct      |
|                            |         empty         |

**Combination of predicates**:

| Transaction storage file | Valid / Invalid |           Description of the test case                |               JUnit test case              |
|:------------------------:|:---------------:|:-----------------------------------------------------:|:------------------------------------------:|
|          correct         |        V        |  attempt to read a storage file with correct format   | it.polito.latazza.data.testReadTransactions |
|           empty          |        V        |       attempt to read an empty storage file           | it.polito.latazza.data.testReadTransactions |
|        not correct       |        I        |   attempt to read a storage file with wrong format    | it.polito.latazza.data.testReadTransactions |

### **Class *StorageImpl* - method *readBalance***

**Criteria for method *readBalance*:**

 - Balance Storage file

**Predicates for method *readBalance*:**

|          Criteria          |       Predicate       |
| -------------------------- | --------------------- |
|    Balance storage file    |        correct        |
|                            |         empty         |
|                            |      not correct      |

**Combination of predicates**:

| Balance storage file | Valid / Invalid |           Description of the test case                |             JUnit test case             |
|:------------------------:|:---------------:|:-----------------------------------------------------:|:---------------------------------------:|
|          correct         |        V        |  attempt to read a storage file with correct format   | it.polito.latazza.data.testReadBalance  |
|           empty          |        V        |        attempt to read an empty storage file          | it.polito.latazza.data.testReadBalance  |
|          correct         |        I        |   attempt to read a storage file with wrong format    | it.polito.latazza.data.testReadBalance  |


### **Class *StorageImpl* - method *writeEmployees***

**Criteria for method *writeEmployees*:**

 - Employee Set

**Predicates for method *writeEmployees*:**

|        Criteria         |       Predicate       |
| ----------------------  | --------------------- |
|      Employee set       |        correct        |

**Combination of predicates**:

| Employee storage file | Valid / Invalid |           Description of the test case                                    |             JUnit test case               |
|:---------------------:|:---------------:|:-------------------------------------------------------------------------:|:-----------------------------------------:|
|        correct        |        V        |  attempt to write a storage file with a set of Employee and read it back  | it.polito.latazza.data.testWriteEmployees |

### **Class *StorageImpl* - method *writeBeverages***

**Criteria for method *writeBeverages*:**

 - Beverage Set

**Predicates for method *writeBeverages*:**

|        Criteria         |       Predicate       |
| ----------------------  | --------------------- |
|      Beverage set       |        correct        |

**Combination of predicates**:

| Beverage storage file | Valid / Invalid |           Description of the test case                                     |             JUnit test case               |
|:---------------------:|:---------------:|:--------------------------------------------------------------------------:|:-----------------------------------------:|
|        correct        |        V        |  attempt to write a storage file with a set of Beverages and read it back  | it.polito.latazza.data.testWriteBeverages |

### **Class *StorageImpl* - method *writeTransactions***

**Criteria for method *writeTransactions*:**

 - Transaction Set

**Predicates for method *writeTransactions*:**

|        Criteria            |       Predicate       |
| -------------------------  | --------------------- |
|      Transaction set       |        correct        |

**Combination of predicates**:

| Transaction storage file | Valid / Invalid |           Description of the test case                                        |             JUnit test case                  |
|:---------------------:|:---------------:|:-----------------------------------------------------------------------------:|:--------------------------------------------:|
|        correct        |        V        |  attempt to write a storage file with a set of Transactions and read it back  | it.polito.latazza.data.testWriteTransactions |


# White Box Unit Tests

### Test cases definition

| Unit name | JUnit test case |
|-----------|-----------------|
| DataImpl  | it.polito.latazza.data.whiteBoxTests |

### Code coverage report

![CodeCoverage](./code_coverage.png)

### Loop coverage analysis

|Unit name | Loop rows | Number of iterations | JUnit test case |
|---|---|---|---|
| DataImpl | 290 | 0 | it.polito.latazza.data.testGetEmployeeReport |
| DataImpl | 290 | 2+ | it.polito.latazza.data.testGetEmployeeReport |
| DataImpl | 315 | 0 | it.polito.latazza.data.testGetReport |
| DataImpl | 315 | 2+ | it.polito.latazza.data.testGetReport |


# Integration Tests

## **Combination of Class DataImpl and Class StorageImpl**

**Criteria for combination of *Class DataImpl and Class StorageImpl*:**

 - Balance amount
 - Beverage Box Price
 - Beverage Capsules Per Box
 - Employee Name + Surname
 - Employee Balance

**Predicates for method *getBeverageCapsules*:**

|       Criteria            |       Predicate       |
| ------------------------- | --------------------- |
| Balance amount		    | 	correct		        |
| Beverage Box Price        | 	correct		        |
| Beverage Capsules Per Box | 	correct		        |
| Employee Name + Surname	| 	correct		        |
| Employee Balance		    | 	correct		        |

**Combination of predicates**:

| Balance amount | Beverage Box Price | Beverage Capsules Per Box | Employee Name + Surname | Employee Balance | Valid / Invalid |                                Description of the test case                                |             JUnit test case             |
|:--------------:|:------------------:|:-------------------------:|:-----------------------:|:----------------:|:---------------:|:------------------------------------------------------------------------------------------:|:---------------------------------------:|
| correct        | correct            | correct                   | correct                 | correct          |        V        | CreateEmployee("Name", "Surname"); RechargeAccount(1, 1000); CreateBeverage("Name", 1, 100); BuyBoxes(1, 1); SellCapsules(1, 1, 1, true);<br>Then create a new DataImpl Object and compare all data structures with expected ones | it.polito.latazza.data.IntegrationTest |
