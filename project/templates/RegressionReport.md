# GUI Test cases documentation template

Authors: MMMA

Date:   14/06/2019

Version: 1.0.0

# Contents

- [Regression Report for Visual Test Cases](#fragility)

# Regression Report for Visual Test Cases

| Scenario | Fragile / Non-Fragile | Number of changed screen captures | Percentage of changed screen captures |
| -------- | --------------------- | --------------------------------- | ------------------------------------- |
|  setup   |        Fragile        |                 5                 |                 33 %                  |
|    1     |        Fragile        |                10                 |                 91 %                  |
|  1_bis   |        Fragile        |                 8                 |                 80 %                  |
|    2     |        Fragile        |                 6                 |                 67 %                  |
|    3     |        Fragile        |                 2                 |                 50 %                  |
|    4     |        Fragile        |                 3                 |                 75 %                  |
|    5     |        Fragile        |                 4                 |                 50 %                  |
|    6     |        Fragile        |                 2                 |                 29 %                  |
