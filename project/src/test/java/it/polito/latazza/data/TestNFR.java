package it.polito.latazza.data;

import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import it.polito.latazza.exceptions.BeverageException;
import it.polito.latazza.exceptions.EmployeeException;
import it.polito.latazza.exceptions.NotEnoughBalance;

public class TestNFR {

	private static final Integer k = 100;
	private static final Integer max_time = 500;
	private Integer e_id1 = -1;
	private Integer b_id1 = -1;

	public TestNFR() {
	}

	private DataImpl setupFun() throws EmployeeException, BeverageException, NotEnoughBalance {
		DataImpl d = new DataImpl();
		d.reset();

		e_id1 = d.createEmployee("E Name_1", "E Surname_1");
		b_id1 = d.createBeverage("B Name_1", 20, 20);

		d.rechargeAccount(e_id1, 1000000000);
		d.buyBoxes(b_id1, 1000000000 / 200);

		return d;
	}

	@Test
	void testSellCapsules() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.sellCapsules(e_id1, b_id1, 1, false);
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testSellCapsulesToVisitor() {
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				d.sellCapsulesToVisitor(b_id1, 1);
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testRechargeAccount() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.rechargeAccount(e_id1, 1);
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testBuyBoxes() {
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				d.buyBoxes(b_id1, 1);
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testGetEmployeeReport() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.getEmployeeReport(e_id1, new Date(1), new Date());
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testGetReport() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.getReport(new Date(1), new Date());
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testCreateBeverage() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.createBeverage("beverage" + i.toString(), 10, 1);
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testUpdateBeverage() {
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				d.updateBeverage(b_id1, "bev" + i.toString(), 10, 100);
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testGetBeverageName() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.getBeverageName(b_id1);
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testGetBeverageCapsulesPerBox() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.getBeverageCapsulesPerBox(b_id1);
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testGetBeverageBoxPrice() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.getBeverageBoxPrice(b_id1);
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testGetBeveragesId() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.getBeveragesId();
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testGetBeverages() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.getBeverages();
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testGetBeverageCapsules() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.getBeverageCapsules(b_id1);
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testCreateEmployee() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.createEmployee("name" + i.toString(), "surname" + i.toString());
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testUpdateEmployee() {
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				d.updateEmployee(e_id1, "name" + i.toString(), "surname" + i.toString());
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testGetEmployeeName() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.getEmployeeName(e_id1);
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testGetEmployeeSurname() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.getEmployeeSurname(e_id1);
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testGetEmployeeBalance() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.getEmployeeBalance(e_id1);
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testGetEmployeesId() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.getEmployeesId();
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testGetBalance() {
		Object tmp = null;
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				tmp = d.getBalance();
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
			assert (tmp != null);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

	@Test
	void testReset() {
		try {
			DataImpl d = setupFun();
			Integer i;
			long init = System.currentTimeMillis();
			for (i = 0; i < k; i++) {
				d.reset();
			}
			long time = System.currentTimeMillis() - init;
			assert (time < max_time * k);
		} catch (Exception e) {
			e.printStackTrace();
			Assertions.fail();
		}
	}

}
