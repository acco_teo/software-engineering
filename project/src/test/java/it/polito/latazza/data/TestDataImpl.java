package it.polito.latazza.data;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import it.polito.latazza.exceptions.BeverageException;
import it.polito.latazza.exceptions.DateException;
import it.polito.latazza.exceptions.EmployeeException;
import it.polito.latazza.exceptions.NotEnoughBalance;
import it.polito.latazza.exceptions.NotEnoughCapsules;

// 23 methods to be tested
public class TestDataImpl {
	
	private DataInterface data = new DataImpl();

	@Test
	public void testSellCapsules() {
		
		Integer e_id1 = null, e_id2 = null, e_id3 = null, e_id4 = null, e_id5 = null, e_id6 = null;
		Integer b_id1 = null, b_id2 = null;
		Integer e1_balance = null, e2_balance = null;
		Integer b1_caps = null;
		Integer balance = null;

		this.data.reset();
		
		try {
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			e_id2 = data.createEmployee("E Name_2", "E Surname_2");
			e_id3 = data.createEmployee("E Name_3", "E Surname_3");
			e_id4 = data.createEmployee("E Name_4", "E Surname_4");
			e_id5 = data.createEmployee("E Name_5", "E Surname_5");
			e_id6 = data.createEmployee("E Name_6", "E Surname_6");
			
			b_id1 = data.createBeverage("B Name_1", 2, 10);	//5 each
			b_id2 = data.createBeverage("B Name_2", 1, Integer.MAX_VALUE);	//a lot each
		
			final Integer newEId1 = e_id1;
			final Integer newBId1 = b_id1;
			Assertions.assertThrows(NotEnoughCapsules.class, () -> {
				data.sellCapsules(newEId1, newBId1, 1, true);			//too many caps
			});
		
			data.rechargeAccount(e_id3, Integer.MAX_VALUE);
			data.buyBoxes(b_id2, 1);	//1 cap
			data.rechargeAccount(e_id4, Integer.MAX_VALUE);
			data.buyBoxes(b_id2, 1);	//2 caps
			data.rechargeAccount(e_id5, Integer.MAX_VALUE);
			data.buyBoxes(b_id2, 1);	//3 caps
			data.rechargeAccount(e_id6, Integer.MAX_VALUE);
			data.buyBoxes(b_id2, 1);	//4 caps
			
			e1_balance = data.rechargeAccount(e_id1, 5);
			e2_balance = data.rechargeAccount(e_id2, 100);
		
			assertEquals(e1_balance, data.getEmployeeBalance(e_id1));
			assertEquals(e2_balance, data.getEmployeeBalance(e_id2));
		
			data.buyBoxes(b_id1, 1);	//2 caps
			
			balance = data.getBalance();
			
			b1_caps = data.getBeverageCapsules(b_id1);
			
			final Integer newEId = wrongId(new Integer[] {e_id1, e_id2, e_id3, e_id4, e_id5, e_id6}, 6);
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.sellCapsules(newEId, newBId1, 1, null);			//null fromAccount
			});
			
			//ACCOUNT
		
			data.sellCapsules(e_id1, b_id1, 0, true);
		
			assertEquals(e1_balance, data.getEmployeeBalance(e_id1));
			
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.sellCapsules(newEId, newBId1, 1, true);			//wrong empl
			});
			
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.sellCapsules(null, newBId1, 1, true);				//null empl
			});
			
			final Integer newBId = wrongId(new Integer[] {b_id1, b_id2}, 2);
			Assertions.assertThrows(BeverageException.class, () -> {
				data.sellCapsules(newEId1, newBId, 1, true);			//wrong bev
			});
			
			Assertions.assertThrows(BeverageException.class, () -> {
				data.sellCapsules(newEId1, null, 1, true);				//null bev
			});
			
			Assertions.assertThrows(NotEnoughCapsules.class, () -> {
				data.sellCapsules(newEId1, newBId1, -1, true);			//negative caps
			});
			
			final Integer newEId2 = e_id2;
			final Integer newBId2 = b_id2;
			Assertions.assertThrows(BeverageException.class, () -> {
				data.sellCapsules(newEId2, newBId2, data.getBeverageCapsules(newBId2), true);
			});
			
			//CASH
			
			data.sellCapsules(e_id1, b_id1, 0, false);
			
			assertEquals(e1_balance, data.getEmployeeBalance(e_id1));
			
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.sellCapsules(newEId, newBId1, 1, false);
			});
			
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.sellCapsules(null, newBId1, 1, false);
			});
			
			Assertions.assertThrows(BeverageException.class, () -> {
				data.sellCapsules(newEId1, newBId, 1, false);
			});
			
			Assertions.assertThrows(BeverageException.class, () -> {
				data.sellCapsules(newEId1, null, 1, false);
			});
			
			Assertions.assertThrows(NotEnoughCapsules.class, () -> {
				data.sellCapsules(newEId1, newBId1, -1, false);
			});
			
			//CORRECT ONES
			
			data.sellCapsules(e_id1, b_id1, 1, true);
			
			assertEquals(Integer.valueOf(e1_balance - 5), data.getEmployeeBalance(e_id1));
			assertEquals(Integer.valueOf(b1_caps - 1), data.getBeverageCapsules(b_id1));
			
			data.sellCapsules(e_id1, b_id1, 1, false);
			assertEquals(Integer.valueOf(e1_balance - 5), data.getEmployeeBalance(e_id1));
			assertEquals(Integer.valueOf(b1_caps - 1 - 1), data.getBeverageCapsules(b_id1));
			assertEquals(Integer.valueOf(balance + 5), data.getBalance());
		} catch (BeverageException | EmployeeException | NotEnoughBalance | NotEnoughCapsules e) {
			e.printStackTrace();
			assertTrue(false);
		}

	}
	
	@Test
	public void testSellCapsulesToVisitor() {

		Integer e_id1 = null, e_id2 = null;
		Integer b_id1 = null, b_id2 = null;
		Integer e1_balance = null, e2_balance = null;
		Integer b1_caps = null;
		Integer balance = null;

		this.data.reset();
		
		try {
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			e_id2 = data.createEmployee("E Name_2", "E Surname_2");
		
			b_id1 = data.createBeverage("B Name_1", 2, 10);	//5 each
			b_id2 = data.createBeverage("B Name_2", 1, Integer.MAX_VALUE);	//a lot each
		
			final Integer newBId1 = b_id1;
			Assertions.assertThrows(NotEnoughCapsules.class, () -> {
				data.sellCapsulesToVisitor(newBId1, 1);					//too many caps
			});
			
			e1_balance = data.rechargeAccount(e_id1, 5);
			e2_balance = data.rechargeAccount(e_id2, 100);
		
			assertEquals(e1_balance, data.getEmployeeBalance(e_id1));
			assertEquals(e2_balance, data.getEmployeeBalance(e_id2));
		
			data.buyBoxes(b_id1, 1);	//2 caps
			
			balance = data.getBalance();
			
			b1_caps = data.getBeverageCapsules(b_id1);
		
			data.sellCapsulesToVisitor(b_id1, 0);
		
			assertEquals(e1_balance, data.getEmployeeBalance(e_id1));
			
			final Integer newBId = wrongId(new Integer[] {b_id1, b_id2}, 2);
			Assertions.assertThrows(BeverageException.class, () -> {
				data.sellCapsulesToVisitor(newBId, 1);					//wrong bev
			});
			
			Assertions.assertThrows(BeverageException.class, () -> {
				data.sellCapsulesToVisitor(null, 1);					//null bev
			});
			
			Assertions.assertThrows(NotEnoughCapsules.class, () -> {
				data.sellCapsulesToVisitor(newBId1, -1);				//negative caps
			});
			
			//CORRECT ONE
			
			data.sellCapsulesToVisitor(b_id1, 1);
			
			assertEquals(Integer.valueOf(balance + 5), data.getBalance());
			assertEquals(Integer.valueOf(b1_caps - 1), data.getBeverageCapsules(b_id1));
		} catch (BeverageException | EmployeeException | NotEnoughBalance | NotEnoughCapsules e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
	}
	
	@Test
	public void testRechargeAccount() {

		Integer id1 = null, id2 = null;
		Integer e1_balance = null, e2_balance = null;

		this.data.reset();
		
		try {
			id1 = data.createEmployee("E Name_1", "E Surname_1");
			id2 = data.createEmployee("E Name_2", "E Surname_2");
			
			assertEquals(Integer.valueOf(0), data.getBalance());
			
			e1_balance = data.rechargeAccount(id1, 0);
			
			assertEquals(Integer.valueOf(0), data.getEmployeeBalance(id1));
			assertEquals(Integer.valueOf(0), e1_balance);
			
			final Integer newId = wrongId(new Integer[] {id1, id2}, 2);
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.rechargeAccount(newId, 1);							//wrong id
			});
			
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.rechargeAccount(null, 1);							//null id
			});
			
			final Integer newId1 = id1;
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.rechargeAccount(newId1, -1);						//negative amount
			});
			
			assertEquals(Integer.valueOf(0), data.getEmployeeBalance(id1));
			
			e1_balance = data.rechargeAccount(id1, 10);
			
			assertEquals(Integer.valueOf(10), data.getEmployeeBalance(id1));
			assertEquals(Integer.valueOf(10), e1_balance);
			assertEquals(Integer.valueOf(10), data.getBalance());
			
			final Integer newId2 = id2;
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.rechargeAccount(newId2, -1);						//negative amount
			});
			
			e2_balance = data.rechargeAccount(id2, 15);
			
			assertEquals(Integer.valueOf(10), data.getEmployeeBalance(id1));
			assertEquals(Integer.valueOf(15), data.getEmployeeBalance(id2));
			assertEquals(Integer.valueOf(15), e2_balance);
			assertEquals(Integer.valueOf(25), data.getBalance());
			
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.rechargeAccount(newId2, Integer.MAX_VALUE);		//overflow
			});
		} catch (EmployeeException e) {
			e.printStackTrace();
			assertTrue(false);
		}

	}
	
	@Test
	public void testBuyBoxes() {

		Integer e_id1 = null;
		Integer b_id1 = null, b_id2 = null;

		this.data.reset();
		
		try {
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
		
			b_id1 = data.createBeverage("B Name_1", 2, 10);	//5 each
			b_id2 = data.createBeverage("B Name_2", 5, 20);	//4 each
			
			data.rechargeAccount(e_id1, 100);
			
			final Integer newBId1 = b_id1;
			Assertions.assertThrows(NotEnoughBalance.class, () -> {
				data.buyBoxes(newBId1, 11);								//too many
			});
			
			final Integer newBId = wrongId(new Integer[] {b_id1, b_id2}, 2);
			Assertions.assertThrows(BeverageException.class, () -> {
				data.buyBoxes(newBId, 1);								//wrong id
			});
			
			Assertions.assertThrows(BeverageException.class, () -> {
				data.buyBoxes(null, 1);									//null id
			});
			
			Assertions.assertThrows(BeverageException.class, () -> {
				data.buyBoxes(newBId1, -1);								//negative amount
			});
			
			assertEquals(Integer.valueOf(0), data.getBeverageCapsules(b_id1));
			
			data.buyBoxes(b_id2, 0);
			
			assertEquals(Integer.valueOf(0), data.getBeverageCapsules(b_id2));
			
			data.buyBoxes(b_id2, 5);
			
			assertEquals(Integer.valueOf(5 * data.getBeverageCapsulesPerBox(b_id2)), data.getBeverageCapsules(b_id2));
		} catch (EmployeeException | BeverageException | NotEnoughBalance e) {
			e.printStackTrace();
			assertTrue(false);
		}

	}
	
	@Test
	public void testGetEmployeeReport() {

		Integer e_id1 = null, e_id2 = null;
		Integer b_id1 = null, b_id2 = null;
		List<String> report = null;
		
		try {
			data.reset();
			
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			e_id2 = data.createEmployee("E Name_2", "E Surname_2");
			
			b_id1 = data.createBeverage("B Name_1", 4, 100);			//100 / 4 = 25 per cap
			b_id2 = data.createBeverage("B Name_2", 10, 150);			//150 / 10 = 15 per cap
			
			data.rechargeAccount(e_id1, 130);	//25*2 + 15*1 + 65
			data.rechargeAccount(e_id2, 220);	//25*4 + 15*8
			data.buyBoxes(b_id1, 2);
			data.buyBoxes(b_id2, 1);
			data.sellCapsules(e_id1, b_id1, 2, true);
			data.sellCapsules(e_id2, b_id1, 4, true);
			data.sellCapsules(e_id2, b_id2, 8, true);
			data.sellCapsules(e_id1, b_id2, 1, true);
			data.sellCapsules(e_id1, b_id1, 2, false);
			data.sellCapsulesToVisitor(b_id2, 1);
			
			LocalDateTime tomorrowLocal = LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()).plusDays(1);
			Date tomorrow = Date.from(tomorrowLocal.toInstant(ZoneId.systemDefault().getRules().getOffset(tomorrowLocal)));
			
			final Integer newId1 = e_id1;
			Assertions.assertThrows(DateException.class, () -> {
				data.getEmployeeReport(newId1, tomorrow, new Date(1));		//reverse date
			});
			
			Assertions.assertThrows(DateException.class, () -> {
				data.getEmployeeReport(newId1, null, new Date(1));		//null date 1
			});
			
			Assertions.assertThrows(DateException.class, () -> {
				data.getEmployeeReport(newId1, new Date(1), null);		//null date 2
			});
			
			report = data.getEmployeeReport(e_id1, new Date(1), tomorrow);
			
			assertEquals(4, report.size());
			
			//Manually check
			System.out.println("START -- EMPLOYEE REPORT");
			for(String line : report) {
				System.out.println(line);
			}
			System.out.println("END -- EMPLOYEE REPORT");
			
			assertTrue(report.get(0).contains("RECHARGE " + data.getEmployeeName(e_id1) + " " + data.getEmployeeSurname(e_id1) + " 1.30 €"));
			assertTrue(report.get(1).contains("BALANCE " + data.getEmployeeName(e_id1) + " " + data.getEmployeeSurname(e_id1) + " " + data.getBeverageName(b_id1) + " 2"));
			assertTrue(report.get(3).contains("CASH " + data.getEmployeeName(e_id1) + " " + data.getEmployeeSurname(e_id1) + " " + data.getBeverageName(b_id1) + " 2"));
			
			report = data.getEmployeeReport(e_id1, new Date(1), new Date(1));
			
			assertEquals(0, report.size());		//empty report
			
		} catch (EmployeeException | BeverageException | NotEnoughBalance | NotEnoughCapsules | DateException e) {
			e.printStackTrace();
			assertTrue(false);
		}

	}
	
	@Test
	public void testGetReport() {

		Integer e_id1 = null, e_id2 = null;
		Integer b_id1 = null, b_id2 = null;
		List<String> report = null;
		
		try {
			data.reset();
			
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			e_id2 = data.createEmployee("E Name_2", "E Surname_2");
			
			b_id1 = data.createBeverage("B Name_1", 4, 100);			//100 / 4 = 25 per cap
			b_id2 = data.createBeverage("B Name_2", 10, 150);			//150 / 10 = 15 per cap
			
			data.rechargeAccount(e_id1, 130);	//25*2 + 15*1 + 65
			data.rechargeAccount(e_id2, 220);	//25*4 + 15*8
			data.buyBoxes(b_id1, 2);
			data.buyBoxes(b_id2, 1);
			data.sellCapsules(e_id1, b_id1, 2, true);
			data.sellCapsules(e_id2, b_id1, 4, true);
			data.sellCapsules(e_id2, b_id2, 8, true);
			data.sellCapsules(e_id1, b_id2, 1, true);
			data.sellCapsules(e_id1, b_id1, 2, false);
			data.sellCapsulesToVisitor(b_id2, 1);
			
			LocalDateTime tomorrowLocal = LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()).plusDays(1);
			Date tomorrow = Date.from(tomorrowLocal.toInstant(ZoneId.systemDefault().getRules().getOffset(tomorrowLocal)));
			
			Assertions.assertThrows(DateException.class, () -> {
				data.getReport(tomorrow, new Date(1));		//reverse date
			});
			
			Assertions.assertThrows(DateException.class, () -> {
				data.getReport(null, new Date(1));		//null date 1
			});
			
			Assertions.assertThrows(DateException.class, () -> {
				data.getReport(new Date(1), null);		//null date 2
			});
			
			report = data.getReport(new Date(1), tomorrow);
			
			assertEquals(10, report.size());
			
			//Manually check
			System.out.println("START -- FULL REPORT");
			for(String line : report) {
				System.out.println(line);
			}
			System.out.println("END -- FULL REPORT");
			
			assertTrue(report.get(0).contains("RECHARGE " + data.getEmployeeName(e_id1) + " " + data.getEmployeeSurname(e_id1) + " 1.30 €"));
			assertTrue(report.get(2).contains("BUY " + data.getBeverageName(b_id1) + " 2"));
			assertTrue(report.get(4).contains("BALANCE " + data.getEmployeeName(e_id1) + " " + data.getEmployeeSurname(e_id1) + " " + data.getBeverageName(b_id1) + " 2"));
			assertTrue(report.get(8).contains("CASH " + data.getEmployeeName(e_id1) + " " + data.getEmployeeSurname(e_id1) + " " + data.getBeverageName(b_id1) + " 2"));
			assertTrue(report.get(9).contains("VISITOR " + data.getBeverageName(b_id2) + " 1"));
			
			report = data.getReport(new Date(1), new Date(1));
			
			assertEquals(0, report.size());		//empty report
			
		} catch (EmployeeException | BeverageException | NotEnoughBalance | NotEnoughCapsules | DateException e) {
			e.printStackTrace();
			assertTrue(false);
		}

	}
	
	@Test
	public void testCreateBeverage() {

		Integer id1 = null, id2 = null, id3 = null;
		
		this.data.reset();
		
		try {
			id1 = data.createBeverage("Name_1", 2, 10);
			id2 = data.createBeverage("Name_2", 5, 15);
			id3 = data.createBeverage("Name_1", 10, 10);	//duplicate, should not create any problem
		} catch (BeverageException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertNotEquals(id1, id2);
		assertNotEquals(id1, id3);
		assertNotEquals(id2, id3);
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.createBeverage(null, 2, 10);
		});
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.createBeverage("", 2, 10);
		});
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.createBeverage("Semicolon;Name", 2, 10);
		});
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.createBeverage("NullCapsulesPerBox", null, 10);
		});
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.createBeverage("ZeroCapsulesPerBox", 0, 10);
		});
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.createBeverage("NullBoxPrice", 2, null);
		});
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.createBeverage("ZeroBoxPrice", 2, 0);
		});

	}
	
	@Test
	public void testUpdateBeverage() {
		
		Integer id1 = null, id2 = null;
		Integer capsPerBox1 = null, boxPrice1 = null;
		String name1 = null;
		
		this.data.reset();
		
		try {
			id1 = data.createBeverage("Name_1", 2, 10);
			id2 = data.createBeverage("Name_2", 5, 15);
			
			data.updateBeverage(id1, "Name_1_2", 4, 20);
			
			name1 = data.getBeverageName(id1);
			capsPerBox1 = data.getBeverageCapsulesPerBox(id1);
			boxPrice1 = data.getBeverageBoxPrice(id1);
		} catch (BeverageException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals("Name_1_2", name1);
		assertEquals(Integer.valueOf(4), capsPerBox1);
		assertEquals(Integer.valueOf(20), boxPrice1);
		
		final Integer newId2 = id2;
		Assertions.assertThrows(BeverageException.class, () -> {
			data.updateBeverage(newId2, null, 2, 10);
		});
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.updateBeverage(newId2, "", 2, 10);
		});
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.updateBeverage(newId2, "Semicolon;Name", 2, 10);
		});
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.updateBeverage(newId2, "NullCapsulesPerBox", null, 10);
		});
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.updateBeverage(newId2, "ZeroCapsulesPerBox", 0, 10);
		});
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.updateBeverage(newId2, "NullBoxPrice", 2, null);
		});
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.updateBeverage(newId2, "ZeroBoxPrice", 2, 0);
		});
		
		final Integer newId = wrongId(new Integer[] {id1, id2}, 2);
		Assertions.assertThrows(BeverageException.class, () -> {
			data.updateBeverage(newId, "NonExistingId", 2, 10);
		});

	}
	
	@Test
	public void testGetBeverageName() {
		
		Integer id1 = null, id2 = null;
		String name1 = null, name2 = null;
		
		this.data.reset();
		
		try {
			id1 = data.createBeverage("Name_1", 2, 10);
			id2 = data.createBeverage("Name_2", 5, 15);
			
			name1 = data.getBeverageName(id1);
			name2 = data.getBeverageName(id2);
		} catch (BeverageException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals("Name_1", name1);
		assertEquals("Name_2", name2);
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.getBeverageName(null);
		});
		
		final Integer newId = wrongId(new Integer[] {id1, id2}, 2);
		Assertions.assertThrows(BeverageException.class, () -> {
			data.getBeverageName(newId);
		});

	}
	
	@Test
	public void testGetBeverageCapsulesPerBox() {
		
		Integer id1 = null, id2 = null;
		Integer capsPerBox1 = null, capsPerBox2 = null;
		
		this.data.reset();
		
		try {
			id1 = data.createBeverage("Name_1", 2, 10);
			id2 = data.createBeverage("Name_2", 5, 15);
			
			capsPerBox1 = data.getBeverageCapsulesPerBox(id1);
			capsPerBox2 = data.getBeverageCapsulesPerBox(id2);
		} catch (BeverageException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals(Integer.valueOf(2), capsPerBox1);
		assertEquals(Integer.valueOf(5), capsPerBox2);
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.getBeverageCapsulesPerBox(null);
		});
		
		final Integer newId = wrongId(new Integer[] {id1, id2}, 2);
		Assertions.assertThrows(BeverageException.class, () -> {
			data.getBeverageCapsulesPerBox(newId);
		});

	}
	
	@Test
	public void testGetBeverageBoxPrice() {
		
		Integer id1 = null, id2 = null;
		Integer boxPrice1 = null, boxPrice2 = null;
		
		this.data.reset();
		
		try {
			id1 = data.createBeverage("Name_1", 2, 10);
			id2 = data.createBeverage("Name_2", 5, 15);
			
			boxPrice1 = data.getBeverageBoxPrice(id1);
			boxPrice2 = data.getBeverageBoxPrice(id2);
		} catch (BeverageException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals(Integer.valueOf(10), boxPrice1);
		assertEquals(Integer.valueOf(15), boxPrice2);
		
		Assertions.assertThrows(BeverageException.class, () -> {
			data.getBeverageBoxPrice(null);
		});
		
		final Integer newId = wrongId(new Integer[] {id1, id2}, 2);
		Assertions.assertThrows(BeverageException.class, () -> {
			data.getBeverageBoxPrice(newId);
		});

	}
	
	@Test
	public void testGetBeveragesId() {
		
		Integer id1 = null, id2 = null;
		
		this.data.reset();
		
		List<Integer> list = data.getBeveragesId();
		
		assertEquals(0, list.size());
		
		try {
			id1 = data.createBeverage("Name_1", 2, 10);
			id2 = data.createBeverage("Name_2", 5, 15);
		} catch (BeverageException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		list = data.getBeveragesId();
		
		assertEquals(2, list.size());
		
		boolean flag = false;
		
		if(list.get(0).equals(id1) && list.get(1).equals(id2)) flag = true;
		if(list.get(0).equals(id2) && list.get(1).equals(id1)) flag = true;
		
		assertTrue(flag);

	}
	
	@Test
	public void testGetBeverages() {
		
		Integer id1 = null, id2 = null;
		
		this.data.reset();
		
		Map<Integer, String> map = data.getBeverages();
		
		assertEquals(0, map.size());
		
		try {
			id1 = data.createBeverage("Name_1", 2, 10);
			id2 = data.createBeverage("Name_2", 5, 15);
		} catch (BeverageException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		map = data.getBeverages();
		
		assertEquals(2, map.size());
		
		assertEquals("Name_1", map.get(id1));
		assertEquals("Name_2", map.get(id2));

	}
	
	@Test
	public void testGetBeverageCapsules() {

		Integer e_id1 = null;
		Integer b_id1 = null, b_id2 = null;

		this.data.reset();
		
		try {
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
		
			b_id1 = data.createBeverage("B Name_1", 2, 10);	//5 each
			b_id2 = data.createBeverage("B Name_2", 5, 20);	//4 each
			
			data.rechargeAccount(e_id1, 200);
			
			data.buyBoxes(b_id1, 20);
			
			assertEquals(Integer.valueOf(20 * data.getBeverageCapsulesPerBox(b_id1)), data.getBeverageCapsules(b_id1));
			
			data.sellCapsules(e_id1, b_id1, data.getBeverageCapsules(b_id1) - 1, true);
			
			assertEquals(Integer.valueOf(1), data.getBeverageCapsules(b_id1));
			
			final Integer newBId = wrongId(new Integer[] {b_id1, b_id2}, 2);
			Assertions.assertThrows(BeverageException.class, () -> {
				data.getBeverageCapsules(newBId);
			});
			
			Assertions.assertThrows(BeverageException.class, () -> {
				data.getBeverageCapsules(null);
			});
		} catch (EmployeeException | BeverageException | NotEnoughBalance | NotEnoughCapsules e) {
			e.printStackTrace();
			assertTrue(false);
		}

	}
	
	@Test
	public void testCreateEmployee() {

		Integer id1 = null, id2 = null, id3 = null;
		
		this.data.reset();
		
		try {
			id1 = data.createEmployee("Name_1", "Surname_1");
			id2 = data.createEmployee("Name_2", "Surname_2");
			id3 = data.createEmployee("Name_1", "Surname_1"); //duplicate, should not create any problem
			
			assertEquals(Integer.valueOf(0), data.getEmployeeBalance(id1));
			assertEquals(Integer.valueOf(0), data.getEmployeeBalance(id2));
			assertEquals(Integer.valueOf(0), data.getEmployeeBalance(id3));
		} catch (EmployeeException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertNotEquals(id1, id2);
		assertNotEquals(id1, id3);
		assertNotEquals(id2, id3);
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.createEmployee(null, "NullName");
		});
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.createEmployee("NullSurname", null);
		});
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.createEmployee("", "EmptyName");
		});
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.createEmployee("EmptySurname", "");
		});
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.createEmployee("Semicolon;Name", "Surname_1");
		});
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.createEmployee("Name_1", "Semicolon;Surname");
		});

	}
	
	@Test
	public void testUpdateEmployee() {

		Integer id1 = null, id2 = null;
		String name1 = null, surname1 = null;
		
		this.data.reset();
		
		try {
			id1 = data.createEmployee("Name_1", "Surname_1");
			id2 = data.createEmployee("Name_2", "Surname_2");
			
			data.updateEmployee(id1, "Name_1_2", "Surname_1_2");
			
			name1 = data.getEmployeeName(id1);
			surname1 = data.getEmployeeSurname(id1);
		} catch (EmployeeException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals("Name_1_2", name1);
		assertEquals("Surname_1_2", surname1);
		
		final Integer newId2 = id2;
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.updateEmployee(newId2, "NullSurname", null);
		});
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.updateEmployee(newId2, null, "NullName");
		});
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.updateEmployee(newId2, "", "EmptyName");
		});
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.updateEmployee(newId2, "EmptySurname", "");
		});
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.updateEmployee(newId2, "Semicolon;Name", "Surname_1");
		});
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.updateEmployee(newId2, "Name_1", "Semicolon;Surname");
		});
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.updateEmployee(null, "NullId", "NullId");
		});
		
		final Integer newId = wrongId(new Integer[] {id1, id2}, 2);
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.updateEmployee(newId, "NonExistingId", "NonExistingId");
		});
		
	}
	
	@Test
	public void testGetEmployeeName() {

		Integer id1 = null, id2 = null;
		String name1 = null, name2 = null;
		
		this.data.reset();
		
		try {
			id1 = data.createEmployee("Name_1", "Surname_1");
			id2 = data.createEmployee("Name_2", "Surname_2");
			
			name1 = data.getEmployeeName(id1);
			name2 = data.getEmployeeName(id2);
		} catch (EmployeeException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals("Name_1", name1);
		assertEquals("Name_2", name2);
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.getEmployeeName(null);
		});
		
		final Integer newId = wrongId(new Integer[] {id1, id2}, 2);
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.getEmployeeName(newId);
		});
		
	}
	
	@Test
	public void testGetEmployeeSurname() {

		Integer id1 = null, id2 = null;
		String surname1 = null, surname2 = null;
		
		this.data.reset();
		
		try {
			id1 = data.createEmployee("Name_1", "Surname_1");
			id2 = data.createEmployee("Name_2", "Surname_2");
			
			surname1 = data.getEmployeeSurname(id1);
			surname2 = data.getEmployeeSurname(id2);
		} catch (EmployeeException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals("Surname_1", surname1);
		assertEquals("Surname_2", surname2);
		
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.getEmployeeSurname(null);
		});
		
		final Integer newId = wrongId(new Integer[] {id1, id2}, 2);
		Assertions.assertThrows(EmployeeException.class, () -> {
			data.getEmployeeSurname(newId);
		});

	}
	
	@Test
	public void testGetEmployeeBalance() {
		
		Integer e_id1 = null, e_id2 = null;
		Integer b_id1 = null;

		this.data.reset();
		
		try {
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			e_id2 = data.createEmployee("E Name_2", "E Surname_2");
		
			b_id1 = data.createBeverage("B Name_1", 2, 10);	//5 each
			
			assertEquals(Integer.valueOf(0), data.getEmployeeBalance(e_id1));
			
			data.rechargeAccount(e_id1, 100);
			data.rechargeAccount(e_id2, 1000);
			
			assertEquals(Integer.valueOf(100), data.getEmployeeBalance(e_id1));
			assertEquals(Integer.valueOf(1000), data.getEmployeeBalance(e_id2));
			
			data.buyBoxes(b_id1, 20);
			
			data.sellCapsules(e_id1, b_id1, 2, false);
			
			assertEquals(Integer.valueOf(100), data.getEmployeeBalance(e_id1));
			assertEquals(Integer.valueOf(1000), data.getEmployeeBalance(e_id2));
			
			data.sellCapsules(e_id1, b_id1, 2, true);
			
			assertEquals(Integer.valueOf(90), data.getEmployeeBalance(e_id1));
			assertEquals(Integer.valueOf(1000), data.getEmployeeBalance(e_id2));
			
			data.sellCapsules(e_id2, b_id1, 2, true);
			
			assertEquals(Integer.valueOf(90), data.getEmployeeBalance(e_id1));
			assertEquals(Integer.valueOf(990), data.getEmployeeBalance(e_id2));
			
			final Integer newId = wrongId(new Integer[] {e_id1, e_id2}, 2);
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.getEmployeeBalance(newId);
			});
			
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.getEmployeeBalance(null);
			});
		} catch (EmployeeException | BeverageException | NotEnoughCapsules | NotEnoughBalance e) {
			e.printStackTrace();
			assertTrue(false);
		}

	}
	
	@Test
	public void testGetEmployeesId() {
		
		Integer id1 = null, id2 = null;
		
		this.data.reset();
		
		List<Integer> list = data.getEmployeesId();
		
		assertEquals(0, list.size());
		
		try {
			id1 = data.createEmployee("Name_1", "Surname_1");
			id2 = data.createEmployee("Name_2", "Surname_2");
		} catch (EmployeeException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		list = data.getEmployeesId();
		
		assertEquals(2, list.size());
		
		boolean flag = false;
		
		if(list.get(0).equals(id1) && list.get(1).equals(id2)) flag = true;
		if(list.get(0).equals(id2) && list.get(1).equals(id1)) flag = true;
		
		assertTrue(flag);

	}
	
	@Test
	public void testGetEmployees() {

		Integer id1 = null, id2 = null;
		
		this.data.reset();
		
		Map<Integer, String> map = data.getEmployees();
		
		assertEquals(0, map.size());
		
		try {
			id1 = data.createEmployee("Name_1", "Surname_1");
			id2 = data.createEmployee("Name_2", "Surname_2");
		} catch (EmployeeException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		map = data.getEmployees();
		
		assertEquals(2, map.size());
		
		assertEquals("Name_1 Surname_1", map.get(id1));
		assertEquals("Name_2 Surname_2", map.get(id2));

	}
	
	@Test
	public void testGetBalance() {
		Integer e_id1 = null;
		Integer b_id1 = null;
		Integer balance = null;

		this.data.reset();
		
		try {
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
		
			b_id1 = data.createBeverage("B Name_1", 2, 10);	//5 each
			
			assertEquals(Integer.valueOf(0), data.getBalance());
			
			data.rechargeAccount(e_id1, 300);
			
			assertEquals(Integer.valueOf(300), data.getBalance());
			
			data.buyBoxes(b_id1, 15);
			
			assertEquals(Integer.valueOf(300 - (15 * data.getBeverageBoxPrice(b_id1))), data.getBalance());
			
			data.buyBoxes(b_id1, 0);
			
			assertEquals(Integer.valueOf(300 - (15 * data.getBeverageBoxPrice(b_id1))), data.getBalance());
			
			data.sellCapsules(e_id1, b_id1, 8, true);
			
			assertEquals(Integer.valueOf(300 - (15 * data.getBeverageBoxPrice(b_id1))), data.getBalance());
			
			data.sellCapsules(e_id1, b_id1, 8, false);
			
			assertEquals(Integer.valueOf(300 - (15 * data.getBeverageBoxPrice(b_id1)) + (8 * data.getBeverageBoxPrice(b_id1) / data.getBeverageCapsulesPerBox(b_id1))), data.getBalance());
			balance = data.getBalance();
			
			data.sellCapsulesToVisitor(b_id1, 8);
			
			assertEquals(Integer.valueOf(balance + (8 * data.getBeverageBoxPrice(b_id1) / data.getBeverageCapsulesPerBox(b_id1))), data.getBalance());
		}  catch (EmployeeException | BeverageException | NotEnoughCapsules | NotEnoughBalance e) {
			e.printStackTrace();
			assertTrue(false);
		}

	}
	
	@Test
	public void testReset() {
		Integer e_id1 = null;
		Integer b_id1 = null;

		this.data.reset();
		
		assertEquals(Integer.valueOf(0), data.getBalance());
		assertEquals(0, data.getEmployeesId().size());
		assertEquals(0, data.getBeveragesId().size());
		
		try {
			assertEquals(0, data.getReport(new Date(1), new Date()).size());
		
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			b_id1 = data.createBeverage("B Name_1", 2, 10);	//5 each
			data.rechargeAccount(e_id1, 1000);
			data.buyBoxes(b_id1, 2);
			data.sellCapsules(e_id1, b_id1, 2, false);
			
			this.data.reset();
			
			assertEquals(Integer.valueOf(0), data.getBalance());
			assertEquals(0, data.getEmployeesId().size());
			assertEquals(0, data.getBeveragesId().size());
			assertEquals(0, data.getReport(new Date(1), new Date()).size());
		} catch (EmployeeException | BeverageException | NotEnoughBalance | NotEnoughCapsules | DateException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
	}
	
	/**
	 * All basic operations are assumed correct. Here I try to test only corner cases and particular combinations of inputs
	 */
	@Test
	public void complexTests() {

		Integer e_id1 = null, e_id2 = null/*, e_id3 = null*/;
		Integer b_id1 = null/*, b_id2 = null, b_id3 = null*/;
		
		try {
			//Test 1
			data.reset();
			
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			e_id2 = data.createEmployee("E Name_2", "E Surname_2");
			
			b_id1 = data.createBeverage("B Name_1", 3, 100);			//100 / 3 = 33.333 per caps
			
			data.rechargeAccount(e_id1, 66);
			data.rechargeAccount(e_id2, 34);
			data.buyBoxes(b_id1, 1);
			data.sellCapsules(e_id1, b_id1, 2, true);
			
			assertEquals(Integer.valueOf(0), data.getBalance());
			assertEquals(Integer.valueOf(0), data.getEmployeeBalance(e_id1));
			
			//Test 2
			
			data.reset();
			
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			e_id2 = data.createEmployee("E Name_2", "E Surname_2");
			b_id1 = data.createBeverage("B Name_1", 4, 100);
			
			data.rechargeAccount(e_id1, Integer.MAX_VALUE);
			final Integer newEId2 = e_id2;
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.rechargeAccount(newEId2, 1);
			});
		} catch (EmployeeException | BeverageException | NotEnoughBalance | NotEnoughCapsules e) {
			e.printStackTrace();
			assertTrue(false);
		}

	}
	
	/**
	 * Tests created looking in detail at code, trying to arise failures
	 */
	@Test
	public void whiteBoxTests() {
		
		Integer e_id1 = null, e_id2 = null, e_id3 = null;
		Integer b_id1 = null, b_id2 = null, b_id3 = null;
		Integer e3_balance = null;
		
		try {
			//Test 1
			
			data.reset();
			
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			e_id2 = data.createEmployee("E Name_2", "E Surname_2");
			e_id3 = data.createEmployee("E Name_3", "E Surname_3");
			b_id1 = data.createBeverage("B Name_1", 4, 100);
			b_id2 = data.createBeverage("B Name_1", 1, Integer.MAX_VALUE);
			
			data.rechargeAccount(e_id1, 100);
			data.buyBoxes(b_id1, 1);
			data.rechargeAccount(e_id2, Integer.MAX_VALUE);
			data.buyBoxes(b_id2, 1);
			
			e3_balance = data.sellCapsules(e_id3, b_id1, 4, true);			//employee balance goes negative, to trigger OF
			
			assertEquals(Integer.valueOf(-100), e3_balance);
			assertEquals(Integer.valueOf(1), data.getBeverageCapsules(b_id2));
			
			final Integer newEId3 = e_id3;
			final Integer newBId2 = b_id2;	
			Assertions.assertThrows(EmployeeException.class, () -> {
				data.sellCapsules(newEId3, newBId2, 1, true);				//OF triggered
			});
			
			assertEquals(Integer.valueOf(1), data.getBeverageCapsules(b_id2));	//number of capsules should still be 1
			
			//Test 2
			
			data.reset();
			
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			b_id1 = data.createBeverage("B Name_1", 4, 100);
			
			data.rechargeAccount(e_id1, 100);
			
			final Integer newBId1 = b_id1;
			Assertions.assertThrows(BeverageException.class, () -> {
				data.buyBoxes(newBId1, null);				//null boxQuantity
			});
			
			//Test 3
			
			data.reset();
			
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			b_id3 = data.createBeverage("B Name_3", Integer.MAX_VALUE, 100);
			
			data.rechargeAccount(e_id1, 200);
			
			final Integer newBId3 = b_id3;
			Assertions.assertThrows(BeverageException.class, () -> {
				data.buyBoxes(newBId3, 2);				//capsules amount OF
			});
		} catch (EmployeeException | BeverageException | NotEnoughBalance | NotEnoughCapsules e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
	}
	
	private Integer wrongId(Integer a[], int n) {
		Integer res = 0;
		
		for(int i = 0; i < n; i++) {
			res += a[i];
			res += a[i];
		}
		
		return res;
	}

}
