package it.polito.latazza.data;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import it.polito.latazza.exceptions.BeverageException;
import it.polito.latazza.exceptions.DateException;
import it.polito.latazza.exceptions.EmployeeException;
import it.polito.latazza.exceptions.NotEnoughBalance;
import it.polito.latazza.exceptions.NotEnoughCapsules;

public class TestAPI {

	private DataInterface data = new DataImpl();

	@Test
	public void testUC1() {
		Integer e_id1 = null, e_id2 = null, b_id1 = null, b_id2 = null, e1_balance = null, b1_caps = null,
				b2_caps = null;
		this.data.reset();

		try {
			/* preconditions */
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			b_id1 = data.createBeverage("B Name_1", 2, 10); // 5 each
			e1_balance = data.rechargeAccount(e_id1, 10);
			data.buyBoxes(b_id1, 1);

			/* UC1 */
			b1_caps = data.getBeverageCapsules(b_id1);
			data.sellCapsules(e_id1, b_id1, 1, true);

			/* asstertions on post conditions */
			assertEquals(Integer.valueOf(e1_balance - 5), data.getEmployeeBalance(e_id1));
			assertEquals(Integer.valueOf(b1_caps - 1), data.getBeverageCapsules(b_id1));

			/* testing UC1b */
			e_id2 = data.createEmployee("E Name_2", "E Surname_2");
			b_id2 = data.createBeverage("B Name_2", 1, Integer.MAX_VALUE); // a lot each
			data.rechargeAccount(e_id2, Integer.MAX_VALUE);
			data.buyBoxes(b_id2, 1);

			e1_balance = data.getEmployeeBalance(e_id1);
			b2_caps = data.getBeverageCapsules(b_id2);

			data.sellCapsules(e_id1, b_id2, 1, true);

			/* asstertions on post conditions */
			assertEquals(Integer.valueOf(e1_balance - Integer.MAX_VALUE), data.getEmployeeBalance(e_id1));
			assertEquals(Integer.valueOf(b2_caps - 1), data.getBeverageCapsules(b_id2));

		} catch (EmployeeException | BeverageException | NotEnoughCapsules | NotEnoughBalance e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	public void testUC2() {
		Integer e_id1 = null, b_id1 = null, b1_caps = null, balance = null;
		this.data.reset();

		try {
			/* preconditions */
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			b_id1 = data.createBeverage("B Name_1", 2, 10); // 5 each
			data.rechargeAccount(e_id1, 100);
			data.buyBoxes(b_id1, 1); // 2 caps

			/* UC2 */
			balance = data.getBalance();
			b1_caps = data.getBeverageCapsules(b_id1);
			data.sellCapsulesToVisitor(b_id1, 1);

			/* post conditions */
			assertEquals(Integer.valueOf(balance + 5), data.getBalance());
			assertEquals(Integer.valueOf(b1_caps - 1), data.getBeverageCapsules(b_id1));
		} catch (BeverageException | EmployeeException | NotEnoughBalance | NotEnoughCapsules e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	public void testUC3() {
		Integer id1 = null, id2 = null;
		Integer e1_balance = null, e2_balance = null;
		this.data.reset();

		try {
			/* preconditions */
			id1 = data.createEmployee("E Name_1", "E Surname_1");
			id2 = data.createEmployee("E Name_2", "E Surname_2");

			/* UC3 */
			e1_balance = data.rechargeAccount(id1, 10);
			e2_balance = data.rechargeAccount(id2, 15);

			/* post conditions */
			assertEquals(Integer.valueOf(10), data.getEmployeeBalance(id1));
			assertEquals(Integer.valueOf(15), data.getEmployeeBalance(id2));
			assertEquals(Integer.valueOf(10), e1_balance);
			assertEquals(Integer.valueOf(15), e2_balance);
			assertEquals(Integer.valueOf(25), data.getBalance());

		} catch (EmployeeException e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	public void testUC4() {
		Integer e_id1 = null, b_id1 = null, balance = null;
		this.data.reset();
		
		try {
			/* preconditions */
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			b_id1 = data.createBeverage("B Name_2", 5, 20); // 4 each
			data.rechargeAccount(e_id1, 100);
			balance = data.getBalance();

			/* UC4 */
			data.buyBoxes(b_id1, 5);

			/* post conditions */
			assertEquals(Integer.valueOf(5 * data.getBeverageCapsulesPerBox(b_id1)), data.getBeverageCapsules(b_id1));
			assertEquals(Integer.valueOf(balance - (5 * data.getBeverageBoxPrice(b_id1))), data.getBalance());
		} catch (EmployeeException | BeverageException | NotEnoughBalance e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	public void testUC5() {
		Integer e_id1 = null, e_id2 = null;
		Integer b_id1 = null, b_id2 = null;
		List<String> report = null;
		this.data.reset();

		try {
			/* preconditions */
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			e_id2 = data.createEmployee("E Name_2", "E Surname_2");
			b_id1 = data.createBeverage("B Name_1", 4, 100); // 100 / 4 = 25 per cap
			b_id2 = data.createBeverage("B Name_2", 10, 150); // 150 / 10 = 15 per cap

			data.rechargeAccount(e_id1, 130); // 25*2 + 15*1 + 65
			data.rechargeAccount(e_id2, 220); // 25*4 + 15*8
			data.buyBoxes(b_id1, 2);
			data.buyBoxes(b_id2, 1);
			data.sellCapsules(e_id1, b_id1, 2, true);
			data.sellCapsules(e_id2, b_id1, 4, true);
			data.sellCapsules(e_id2, b_id2, 8, true);
			data.sellCapsules(e_id1, b_id2, 1, true);
			data.sellCapsules(e_id1, b_id1, 2, false);
			data.sellCapsulesToVisitor(b_id2, 1);

			LocalDateTime tomorrowLocal = LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault()).plusDays(1);
			Date tomorrow = Date.from(tomorrowLocal.toInstant(ZoneId.systemDefault().getRules().getOffset(tomorrowLocal)));

			/* UC5 */
			report = data.getEmployeeReport(e_id1, new Date(1), tomorrow);

			/* post conditions */
			assertEquals(4, report.size());

			assertTrue(report.get(0).contains("RECHARGE " + data.getEmployeeName(e_id1) + " " + data.getEmployeeSurname(e_id1) + " 1.30 €"));
			assertTrue(report.get(1).contains("BALANCE " + data.getEmployeeName(e_id1) + " " + data.getEmployeeSurname(e_id1) + " " + data.getBeverageName(b_id1) + " 2"));
			assertTrue(report.get(3).contains("CASH " + data.getEmployeeName(e_id1) + " " + data.getEmployeeSurname(e_id1) + " " + data.getBeverageName(b_id1) + " 2"));

		} catch (EmployeeException | BeverageException | NotEnoughBalance | NotEnoughCapsules | DateException e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}

	@Test
	public void testUC6() {
		Integer e_id1 = null, e_id2 = null;
		Integer b_id1 = null, b_id2 = null;
		List<String> report = null;

		this.data.reset();

		try {
			/* preconditions */
			e_id1 = data.createEmployee("E Name_1", "E Surname_1");
			e_id2 = data.createEmployee("E Name_2", "E Surname_2");

			b_id1 = data.createBeverage("B Name_1", 4, 100); // 100 / 4 = 25 per cap
			b_id2 = data.createBeverage("B Name_2", 10, 150); // 150 / 10 = 15 per cap

			data.rechargeAccount(e_id1, 130); // 25*2 + 15*1 + 65
			data.rechargeAccount(e_id2, 220); // 25*4 + 15*8
			data.buyBoxes(b_id1, 2);
			data.buyBoxes(b_id2, 1);
			data.sellCapsules(e_id1, b_id1, 2, true);
			data.sellCapsules(e_id2, b_id1, 4, true);
			data.sellCapsules(e_id2, b_id2, 8, true);
			data.sellCapsules(e_id1, b_id2, 1, true);
			data.sellCapsules(e_id1, b_id1, 2, false);
			data.sellCapsulesToVisitor(b_id2, 1);

			LocalDateTime tomorrowLocal = LocalDateTime.ofInstant((new Date()).toInstant(), ZoneId.systemDefault())
					.plusDays(1);
			Date tomorrow = Date
					.from(tomorrowLocal.toInstant(ZoneId.systemDefault().getRules().getOffset(tomorrowLocal)));

			/* UC6 */
			report = data.getReport(new Date(1), tomorrow);

			/* post conditions */
			assertEquals(10, report.size());

			assertTrue(report.get(0).contains(
					"RECHARGE " + data.getEmployeeName(e_id1) + " " + data.getEmployeeSurname(e_id1) + " 1.30 €"));
			assertTrue(report.get(2).contains("BUY " + data.getBeverageName(b_id1) + " 2"));
			assertTrue(report.get(4).contains("BALANCE " + data.getEmployeeName(e_id1) + " "
					+ data.getEmployeeSurname(e_id1) + " " + data.getBeverageName(b_id1) + " 2"));
			assertTrue(report.get(8).contains("CASH " + data.getEmployeeName(e_id1) + " "
					+ data.getEmployeeSurname(e_id1) + " " + data.getBeverageName(b_id1) + " 2"));
			assertTrue(report.get(9).contains("VISITOR " + data.getBeverageName(b_id2) + " 1"));

		} catch (EmployeeException | BeverageException | NotEnoughBalance | NotEnoughCapsules | DateException e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}
}
