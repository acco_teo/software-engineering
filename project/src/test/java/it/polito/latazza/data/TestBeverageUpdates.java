package it.polito.latazza.data;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestBeverageUpdates {
	

	@Test
	public void testAvailable() {
		
		// Beverage creation
		Beverage b = new Beverage(0,"BeverageTest",0,10,100,10);
		Assertions.assertEquals(Integer.valueOf(0),b.getAvailable());
		
		// Increase Availability by 0 capsules
		b.setAvailableAndPrice(0);
		Assertions.assertEquals(Integer.valueOf(0),b.getAvailable());
		
		// Update using a positive amount
		b.setAvailableAndPrice(10);
		Assertions.assertEquals(Integer.valueOf(10),b.getAvailable());
		
		// Update using a negative amount
		b.setAvailableAndPrice(-5);
		Assertions.assertEquals(Integer.valueOf(5),b.getAvailable());
		
		// TEST WITH 2 prices
		// Update beverage to price B
		b.setCapsulesPerBox(20);
		b.setBoxPrice(400);
		Assertions.assertEquals(Integer.valueOf(5), b.getAvailable());
		
		// Update using a positive amount
		b.setAvailableAndPrice(10);
		Assertions.assertEquals(Integer.valueOf(15), b.getAvailable());
		
		// Update using a negative amount
		b.setAvailableAndPrice(-12);
		Assertions.assertEquals(Integer.valueOf(3),b.getAvailable());
								
	}
	
	@Test
	public void testGetBeverages() {
		
		// Beverage creation with price A
		Beverage b = new Beverage(0,"BeverageTest",0,10,100,10);
		// Get List of Beverages (should contain only one element)
		List<Beverage> l = b.getBeverages();
		Assertions.assertEquals(Integer.valueOf(1),Integer.valueOf(l.size()));
		Assertions.assertTrue(l.contains(b));
		
		// Update beverage to price B
		b.setCapsulesPerBox(20);
		b.setBoxPrice(400);
		
		// List should contain again 1 elements 
		// ( since there are no capsules yet, last price only should be stored)
		l = b.getBeverages();
		Assertions.assertEquals(Integer.valueOf(1),Integer.valueOf(l.size()));
		
		// Increase Availability of capsules of price B
		b.setAvailableAndPrice(15);
		
		// Update beverage to price C
		b.setCapsulesPerBox(30);
		b.setBoxPrice(400);
		
		// List should now contain 2 elements
		l = b.getBeverages();
		Assertions.assertEquals(Integer.valueOf(1),Integer.valueOf(l.size()));
		Assertions.assertTrue(l.contains(b));
				
		// Increase Availability of capsules of price C
		b.setAvailableAndPrice(10);
				
		// List should contain again 2 elements
		l = b.getBeverages();
		Assertions.assertEquals(Integer.valueOf(2),Integer.valueOf(l.size()));
		//Assertions.assertTrue(l.contains(b));
		
		// Decrease Availability of capsules of price B
		b.setAvailableAndPrice(-10);
				
		// List should contain again 2 elements
		l = b.getBeverages();
		Assertions.assertEquals(Integer.valueOf(2),Integer.valueOf(l.size()));
		//Assertions.assertTrue(l.contains(b));
		
		// Decrease Availability of capsules of price B
		b.setAvailableAndPrice(-10);
		
		// List should contain 1 element since there are no more capsules of price B
		l = b.getBeverages();
		Assertions.assertEquals(Integer.valueOf(1), Integer.valueOf(l.size()));
		Assertions.assertTrue(l.contains(b));
	}
	
	@Test
	public void testPrice() {
		
		// Beverage creation
		Beverage b = new Beverage(0,"BeverageTest",0,100,10,10);
		Assertions.assertEquals(Integer.valueOf(10), b.getLastPrice());
		
		// Update beverage to price B
		b.setCapsulesPerBox(20);
		b.setBoxPrice(400);
		Assertions.assertEquals(Integer.valueOf(20), b.getLastPrice());
		
		// Increase Availability of capsules of price B
		b.setAvailableAndPrice(20);
		
		// Update beverage to price C
		b.setCapsulesPerBox(20);
		b.setBoxPrice(800);
		Assertions.assertEquals(Integer.valueOf(40), b.getLastPrice());
		
		// Increase Availability of capsules of price C
		b.setAvailableAndPrice(10);
		Assertions.assertEquals(Integer.valueOf(800), b.getPrice(b.getAvailable()));

	}
	
}
