package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import it.polito.latazza.exceptions.BeverageException;
import it.polito.latazza.exceptions.DateException;
import it.polito.latazza.exceptions.EmployeeException;
import it.polito.latazza.exceptions.NotEnoughBalance;
import it.polito.latazza.exceptions.NotEnoughCapsules;
import it.polito.latazza.exceptions.WrongFileFormatException;
import it.polito.latazza.storage.StorageImpl;

public class TestLaTazza {

	@Test
	public void startupIntegrationTest() {
		StorageImpl s = new StorageImpl();
		DataImpl di = new DataImpl();

		di.reset();

		/* empty collection tests */
		try {
			s.readBeverages();
		} catch (WrongFileFormatException | IOException e1) {
			e1.printStackTrace();
			Assertions.fail();
		}

		try {
			s.readEmployees();
		} catch (WrongFileFormatException | IOException e1) {
			e1.printStackTrace();
			Assertions.fail();
		}

		try {
			s.readTransactions();
		} catch (WrongFileFormatException | IOException e1) {
			e1.printStackTrace();
			Assertions.fail();
		}

		try {
			s.readBalance();
		} catch (IOException e1) {
			e1.printStackTrace();
			Assertions.fail();
		}

		Assertions.assertTrue(di.getEmployees().isEmpty());
		Assertions.assertTrue(di.getBeverages().isEmpty());
		try {
			Assertions.assertTrue(di.getReport(new Date(1), new Date()).isEmpty());
		} catch (DateException e) {
			e.printStackTrace();
			Assertions.fail();
		}
		assertEquals(di.getBalance(), Integer.valueOf(0));
	}

	@Test
	public void addBeverageIntegrationTest() {
		DataImpl di = new DataImpl();
		di.reset();

		try {
			di.createBeverage("caffe", 10, 100);
		} catch (BeverageException e) {
			e.printStackTrace();
			Assertions.fail();
		}

	}

	@Test
	public void addEmployeeIntegrationTest() {
		DataImpl di = new DataImpl();
		di.reset();
		Integer i = 0;
		String name = "Matteo";
		String surname = "Coscia";
		try {
			i = di.createEmployee(name, surname);
		} catch (EmployeeException e) {
			e.printStackTrace();
			Assertions.fail();
		}

		DataImpl d2 = new DataImpl();

		assertEquals(d2.getEmployees().get(i), name + " " + surname);
	}

	@Test
	public void IntegrationTest() {
		DataImpl di = new DataImpl();
		di.reset();

		String name = "Matteo";
		String surname = "Coscia";
		Integer empId = 0;
		Integer empBalance = 1000;

		try {
			empId = di.createEmployee(name, surname);
			di.rechargeAccount(empId, empBalance);
		} catch (EmployeeException e) {
			e.printStackTrace();
			Assertions.fail();
		}

		Integer bevId = 0;
		Integer boxPrice = 100;
		Integer numCapsules = 1;
		try {
			bevId = di.createBeverage("caffe", numCapsules, boxPrice);
		} catch (BeverageException e) {
			e.printStackTrace();
			Assertions.fail();
		}

		try {
			di.buyBoxes(bevId, 1);
		} catch (BeverageException | NotEnoughBalance e) {
			e.printStackTrace();
			Assertions.fail();
		}

		try {
			di.sellCapsules(empId, bevId, 1, true);
		} catch (EmployeeException | BeverageException | NotEnoughCapsules e) {
			e.printStackTrace();
			Assertions.fail();
		}

		DataImpl d2 = new DataImpl();
		assertEquals(d2.getBalance(), Integer.valueOf(empBalance - boxPrice));

		try {
			assertEquals(d2.getBeverageBoxPrice(bevId), boxPrice);
		} catch (BeverageException e) {
			e.printStackTrace();
			Assertions.fail();
		}

		try {
			assertEquals(d2.getBeverageCapsulesPerBox(bevId), numCapsules);
		} catch (BeverageException e) {
			e.printStackTrace();
			Assertions.fail();
		}

		assertEquals(d2.getEmployees().get(empId), name + " " + surname);

		try {
			assertEquals(d2.getEmployeeBalance(empId), Integer.valueOf(empBalance - boxPrice / numCapsules));
		} catch (EmployeeException e) {
			e.printStackTrace();
			Assertions.fail();
		}

	}
	
	@Test
	public void updateBeverageIntegrationTest() {
		DataImpl di = new DataImpl();
		
		di.reset();
		
		Integer bevId = 0;
		Integer boxPrice = 100;
		Integer newBoxPrice = 200;
		Integer numCapsules = 1;
		Integer newNumCapsules = 3;
		
		try {
			bevId = di.createBeverage("caffe", numCapsules, boxPrice);
		} catch (BeverageException e) {
			e.printStackTrace();
			Assertions.fail();
		}
		
		String name = "Matteo";
		String surname = "Coscia";
		Integer empId = 0;
		Integer empBalance = 1000;

		try {
			empId = di.createEmployee(name, surname);
			di.rechargeAccount(empId, empBalance);
		} catch (EmployeeException e) {
			e.printStackTrace();
			Assertions.fail();
		}
		
		try {
			di.buyBoxes(bevId, 1);
		} catch (BeverageException | NotEnoughBalance e) {
			e.printStackTrace();
			Assertions.fail();
		}
		
		try {
			di.updateBeverage(bevId, "caffe", newNumCapsules, newBoxPrice); 
		} catch (BeverageException e) {
			e.printStackTrace();
			Assertions.fail();
		}
		
		try {
			di.buyBoxes(bevId, 1);
		} catch (BeverageException | NotEnoughBalance e) {
			e.printStackTrace();
			Assertions.fail();
		}
		
		DataImpl di2 = new DataImpl();
		
		
		try {
			di2.sellCapsules(empId, bevId, 1, true);
			empBalance = empBalance - 1 * (boxPrice / numCapsules);
			assertEquals(empBalance, di2.getEmployeeBalance(empId));
			di2.sellCapsules(empId, bevId, 1, true);
			empBalance = empBalance - 1 * (newBoxPrice / newNumCapsules);
			assertEquals(empBalance, di2.getEmployeeBalance(empId));
		} catch (EmployeeException | BeverageException | NotEnoughCapsules e) {
			e.printStackTrace();
			Assertions.fail();
		}
		
	}

}
