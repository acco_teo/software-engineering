package it.polito.latazza.data;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import it.polito.latazza.exceptions.WrongFileFormatException;
import it.polito.latazza.storage.Storage;
import it.polito.latazza.storage.StorageImpl;



// 6 methods to be tested
public class TestStorageImpl {

	private static final String pathTransactions = "dataTransactions.csv";
	private static final String pathBeverages = "dataBeverages.csv";
	private static final String pathEmployees = "dataEmployees.csv";
	private static final String pathBalance = "dataBalance.csv";
	
	@Test
	public void testReadEmployees() {
		
		Storage storage = new StorageImpl();

		Employee e1 = new Employee(1, "Name_1", "Surname_1", 0);
		Employee e2 = new Employee(2, "Name_2", "Surname_2", 10);
		Employee e3 = new Employee(3, "Name_3", "Surname_3", 100);
		Employee e4 = new Employee(4, "Name_4", "Surname_4", 277);
		
		Map<Integer, Employee> mapIn = new HashMap<Integer, Employee>();
		Map<Integer, Employee> mapOut = new HashMap<Integer, Employee>();
		
		mapIn.put(e1.getEmployeeId(), e1);
		mapIn.put(e2.getEmployeeId(), e2);
		mapIn.put(e3.getEmployeeId(), e3);
		mapIn.put(e4.getEmployeeId(), e4);
		
		// Correct format
		try {	
		
			FileWriter fileWriter = new FileWriter(pathEmployees);
			PrintWriter printWriter = new PrintWriter(fileWriter);
		
			mapIn.entrySet().forEach(en -> {
				Employee e = en.getValue();
				printWriter.printf("%d;%s;%s;%d\n", e.getEmployeeId(), e.getName(), e.getSurname(), e.getBalance());
			});

			printWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		try {
			mapOut = storage.readEmployees();
		} catch (WrongFileFormatException | IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals(mapIn.size(), mapOut.size());
		
		boolean flag = true;

		if(!mapOut.containsValue(e1)) flag = false;
		if(!mapOut.containsValue(e2)) flag = false;
		if(!mapOut.containsValue(e3)) flag = false;
		if(!mapOut.containsValue(e4)) flag = false;
		
		assertTrue(flag);
		
		// reset 
		
		try {
			storage.resetStorage();
		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		
		// Wrong format 
		
		try {	
			
			FileWriter fileWriter = new FileWriter(pathEmployees);
			PrintWriter printWriter = new PrintWriter(fileWriter);
		
			mapIn.entrySet().forEach(en -> {
				printWriter.printf("%d;%s;%s;%s;%d\n",-1 , "WrongF","WrongF","WrongF", -1 );
			});

			printWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}

		// Attempt to read
		Assertions.assertThrows(WrongFileFormatException.class, () -> {
			storage.readEmployees();
		});
		
		// reset 
		
		try {
			storage.resetStorage();
		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		try {
			mapOut = storage.readEmployees();
		} catch (WrongFileFormatException | IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals(0, mapOut.size());

	}
	
	@Test
	public void testReadTransactions() {

		Storage storage = new StorageImpl();

		Transaction t1 = new Transaction("Date1", "-", "-", "Name_1", "0");
		Transaction t2 = new Transaction("Date2", "-", "-", "Name_1", "0");
		Transaction t3 = new Transaction("Date3", "-", "-", "Name_1", "0");
		Transaction t4 = new Transaction("Date4", "-", "-", "Name_1", "0");
		
		List<Transaction> listIn = new ArrayList<Transaction>();
		List<Transaction> listOut = new ArrayList<Transaction>();
		
		listIn.add(t1);
		listIn.add(t2);
		listIn.add(t3);
		listIn.add(t4);
		

		// Correct format
		
		try {	
		
			FileWriter fileWriter = new FileWriter(pathTransactions);
			PrintWriter printWriter = new PrintWriter(fileWriter);
		
			listIn.forEach(t -> {
				printWriter.printf("%s;%s;%s;%s;%s\n", t.getDate(), t.getOperation(), t.getSubject(), t.getBeverageName(),
						t.getAmount());
			});

			printWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		// attempt to read
		try {
			listOut = storage.readTransactions();
		} catch (WrongFileFormatException | IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals(listIn.size(), listOut.size());
		
		boolean flag = true;

		if(!listOut.contains(t1)) flag = false;
		if(!listOut.contains(t2)) flag = false;
		if(!listOut.contains(t3)) flag = false;
		if(!listOut.contains(t4)) flag = false;
		
		assertTrue(flag);
		

		// reset 
		
		try {
			storage.resetStorage();
		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		// Wrong format
		
		try {	
		
			FileWriter fileWriter = new FileWriter(pathTransactions);
			PrintWriter printWriter = new PrintWriter(fileWriter);
		
			listIn.forEach(t -> {
				printWriter.printf("%s;%s;%s\n","WrongF","WrongF","WrongF") ;
			});

			printWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
				
		// Attempt to read		
		Assertions.assertThrows(WrongFileFormatException.class, () -> {
					storage.readTransactions();
				});
				
		// reset 
		
		try {
			storage.resetStorage();
		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		try {
			listOut = storage.readTransactions();
		} catch (WrongFileFormatException | IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals(0, listOut.size());

	}
	
	@Test
	public void testReadBeverages() {

		Storage storage = new StorageImpl();

		Beverage b1 = new Beverage(1, "Name_1", 0, 10, 2, 5);
		Beverage b2 = new Beverage(2, "Name_2", 10, 15, 5, 3);
		Beverage b3 = new Beverage(3, "Name_3", 20, 6, 3, 2);
		Beverage b4 = new Beverage(4, "Name_4", 52, 8, 1, 8);
		
		Map<Integer, Beverage> mapIn = new HashMap<Integer, Beverage>();
		Map<Integer, Beverage> mapOut = new HashMap<Integer, Beverage>();
		
		mapIn.put(b1.getBeverageId(), b1);
		mapIn.put(b2.getBeverageId(), b2);
		mapIn.put(b3.getBeverageId(), b3);
		mapIn.put(b4.getBeverageId(), b4);
		

		// Correct format
		try {	
		
			FileWriter fileWriter = new FileWriter(pathBeverages);
			PrintWriter printWriter = new PrintWriter(fileWriter);
		
			mapIn.entrySet().forEach(be -> {
				Beverage b = be.getValue();
				
				List<Beverage> beveragesList = b.getBeverages();
				beveragesList.forEach(bev -> {						
					printWriter.printf("%d;%s;%d;%d;%d;%d\n", bev.getBeverageId(), bev.getName(), b.getAvailable(), b.getBoxPrice(),
							b.getCapsulesPerBox(), b.getPrice(1));				
				});
				
	
			});

			printWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		 
		// attempt to read
		try {
			mapOut = storage.readBeverages();
		} catch (WrongFileFormatException | IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals(mapIn.size(), mapOut.size());
		
		boolean flag = true;

		if(!mapOut.containsValue(b1)) flag = false;
		if(!mapOut.containsValue(b2)) flag = false;
		if(!mapOut.containsValue(b3)) flag = false;
		if(!mapOut.containsValue(b4)) flag = false;
		
		assertTrue(flag);
		

		// reset
		try {
			storage.resetStorage();
		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		// Wrong format 
		
		try {	
			
			FileWriter fileWriter = new FileWriter(pathBeverages);
			PrintWriter printWriter = new PrintWriter(fileWriter);
		
			mapIn.entrySet().forEach(be -> {
				printWriter.printf("%d;%s;%d\n", -1, "WrongF", -1);		
			});
			
			printWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}

		// Attempt to read
		Assertions.assertThrows(WrongFileFormatException.class, () -> {
			storage.readBeverages();
		});
				
		
		// reset
		try {
			storage.resetStorage();
		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		try {
			mapOut = storage.readBeverages();
		} catch (WrongFileFormatException | IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals(0, mapOut.size());

	}
	
	@Test
	public void testReadBalance() {
		Storage storage = new StorageImpl();
		int balance = -1;
		// Correct format
		try {	
		
			FileWriter fileWriter = new FileWriter(pathBalance);
			PrintWriter printWriter = new PrintWriter(fileWriter);
		
			printWriter.printf("%d\n", 25);

			printWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}

		// attempt to read
		try {
			balance = storage.readBalance();
		} catch (WrongFileFormatException | IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals(25, balance);
		
		// reset
		try {
			storage.resetStorage();
		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}

		
		// Wrong format
		try {	
		
			FileWriter fileWriter = new FileWriter(pathBalance);
			PrintWriter printWriter = new PrintWriter(fileWriter);
		
			printWriter.print("a");

			printWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}

		// Attempt to read
				Assertions.assertThrows(IOException.class, () -> {
					storage.readBalance();
				});
		
		
		// reset
		try {
			storage.resetStorage();
		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
	 
		// attempt to read
		try {
			balance = storage.readBalance();
		} catch (WrongFileFormatException | IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		assertEquals(0, balance);
	}
	
	
	@Test
	public void testWriteEmployees() {
		
		Storage storage = new StorageImpl();

		Employee e1 = new Employee(1, "Name_1", "Surname_1", 0);
		Employee e2 = new Employee(2, "Name_2", "Surname_2", 10);
		Employee e3 = new Employee(3, "Name_3", "Surname_3", 100);
		Employee e4 = new Employee(4, "Name_4", "Surname_4", 277);
		
		Map<Integer, Employee> mapIn = new HashMap<Integer, Employee>();
		Map<Integer, Employee> mapOut = new HashMap<Integer, Employee>();
		
		mapIn.put(e1.getEmployeeId(), e1);
		mapIn.put(e3.getEmployeeId(), e3);
		mapIn.put(e4.getEmployeeId(), e4);
		mapIn.put(e2.getEmployeeId(), e2);
		
		try {
			storage.writeEmployees(mapIn);
		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}

		try {
			mapOut = storage.readEmployees();
		} catch (WrongFileFormatException | IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals(mapIn.size(), mapOut.size());
		
		boolean flag = true;

		if(!mapOut.containsValue(e1)) flag = false;
		if(!mapOut.containsValue(e2)) flag = false;
		if(!mapOut.containsValue(e3)) flag = false;
		if(!mapOut.containsValue(e4)) flag = false;
		
		assertTrue(flag);
	}
	
	@Test
	public void testWriteTransactions() {

		Storage storage = new StorageImpl();

		Transaction t1 = new Transaction("-", "-", "-", "Name_1", "0");
		Transaction t2 = new Transaction("-", "-", "-", "Name_1", "0");
		Transaction t3 = new Transaction("-", "-", "-", "Name_1", "0");
		Transaction t4 = new Transaction("-", "-", "-", "Name_1", "0");
		
		List<Transaction> listIn = new ArrayList<Transaction>();
		List<Transaction> listOut = new ArrayList<Transaction>();
		
		listIn.add(t1);
		listIn.add(t2);
		listIn.add(t3);
		listIn.add(t4);
		
		try {
			storage.writeTransactions(listIn);
		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}

		
		// attempt to read
		try {
			listOut = storage.readTransactions();
		} catch (WrongFileFormatException | IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals(listIn.size(), listOut.size());
		
		boolean flag = true;

		if(!listOut.contains(t1)) flag = false;
		if(!listOut.contains(t2)) flag = false;
		if(!listOut.contains(t3)) flag = false;
		if(!listOut.contains(t4)) flag = false;
		
		assertTrue(flag);
		
	}
	
	@Test
	public void testWriteBeverages() {

		Storage storage = new StorageImpl();

		Beverage b1 = new Beverage(1, "Name_1", 0, 10, 2, 5);
		Beverage b2 = new Beverage(2, "Name_2", 10, 15, 5, 3);
		Beverage b3 = new Beverage(3, "Name_3", 20, 6, 3, 2);
		Beverage b4 = new Beverage(4, "Name_4", 52, 8, 1, 8);
		
		Map<Integer, Beverage> mapIn = new HashMap<Integer, Beverage>();
		Map<Integer, Beverage> mapOut = new HashMap<Integer, Beverage>();
		
		mapIn.put(b1.getBeverageId(), b1);
		mapIn.put(b2.getBeverageId(), b2);
		mapIn.put(b3.getBeverageId(), b3);
		mapIn.put(b4.getBeverageId(), b4);
		
		try {
			storage.writeBeverages(mapIn);
		} catch (IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		// attempt to read
		try {
			mapOut = storage.readBeverages();
		} catch (WrongFileFormatException | IOException e) {
			e.printStackTrace();
			assertTrue(false);
		}
		
		assertEquals(mapIn.size(), mapOut.size());
		
		boolean flag = true;

		if(!mapOut.containsValue(b1)) flag = false;
		if(!mapOut.containsValue(b2)) flag = false;
		if(!mapOut.containsValue(b3)) flag = false;
		if(!mapOut.containsValue(b4)) flag = false;
		
		assertTrue(flag);
	

	}

}
