package it.polito.latazza.storage;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import it.polito.latazza.data.Beverage;
import it.polito.latazza.data.Employee;
import it.polito.latazza.data.Transaction;
import it.polito.latazza.exceptions.WrongFileFormatException;

public class StorageImpl implements Storage {
	private static final String pathTransactions = "dataTransactions.csv";
	private static final String pathBeverages = "dataBeverages.csv";
	private static final String pathEmployees = "dataEmployees.csv";
	private static final String pathBalance = "dataBalance.csv";
	private static final String pathData = "";

	public StorageImpl() {
		
	}

	@Override
	public Map<Integer, Employee> readEmployees() throws IOException, WrongFileFormatException {
		Path path = FileSystems.getDefault().getPath(pathData, pathEmployees);
		Stream<String> strings = Files.lines(path, StandardCharsets.UTF_8);
		Map<Integer, Employee> employeesMap = new HashMap<Integer, Employee>();
		strings.forEach(l -> {
			String[] employeeAttr = l.split(";");
			if (employeeAttr.length != 4) {
				throw new WrongFileFormatException();
			} else {
				Employee e = new Employee(Integer.parseInt(employeeAttr[0]), employeeAttr[1], employeeAttr[2],
						Integer.parseInt(employeeAttr[3]));
				employeesMap.put(e.getEmployeeId(), e);
			}
		});
		strings.close();
		return employeesMap;

	}

	@Override
	public List<Transaction> readTransactions() throws IOException, WrongFileFormatException {
		Path path = FileSystems.getDefault().getPath(pathData, pathTransactions);
		Stream<String> strings = Files.lines(path, StandardCharsets.UTF_8);
		List<Transaction> transactionList = new ArrayList<Transaction>();
		strings.forEach(s -> {
			String[] transactionValues = s.split(";");
			if (transactionValues.length != 5) {
				throw new WrongFileFormatException();
			} else {
				Transaction t = new Transaction(transactionValues[0], transactionValues[1], transactionValues[2],
						transactionValues[3], transactionValues[4]);
				transactionList.add(t);
			}
		});

		strings.close();
		return transactionList;
	}

	@Override
	public Map<Integer, Beverage> readBeverages() throws IOException, WrongFileFormatException {
		Path path = FileSystems.getDefault().getPath(pathData, pathBeverages);
		Stream<String> strings = Files.lines(path, StandardCharsets.UTF_8);
		Map<Integer, Beverage> beverageMap = new HashMap<Integer, Beverage>();
		strings.forEach(l -> {
			String[] beverageAttr = l.split(";");
			if (beverageAttr.length != 6) {
				throw new WrongFileFormatException();
			} else {
				
				Integer beverageId = Integer.parseInt(beverageAttr[0]);
				String name = beverageAttr[1];
				Integer available = Integer.parseInt(beverageAttr[2]); 
				Integer boxPrice = Integer.parseInt(beverageAttr[3]);
				Integer capsulesPerBox = Integer.parseInt(beverageAttr[4]);
				Integer price = Integer.parseInt(beverageAttr[5]);
				
				if(beverageMap.containsKey(beverageId)) {  //already exists
					beverageMap.get(beverageId).aggregateBeverages(available, price);
				}else { //new one
					Beverage b = new Beverage(beverageId, name, available, boxPrice, capsulesPerBox, price);
					beverageMap.put(b.getBeverageId(), b);
				}
			}
		});
		strings.close();
		return beverageMap;
	}

	@Override
	public void writeEmployees(Map<Integer, Employee> employeesMap) throws IOException {
		FileWriter fileWriter = new FileWriter(pathEmployees);
		PrintWriter printWriter = new PrintWriter(fileWriter);

		employeesMap.entrySet().forEach(en -> {
			Employee e = en.getValue();
			
			if(!e.getName().contains(";") && !e.getSurname().contains(";")) {
				printWriter.printf("%d;%s;%s;%d\n", e.getEmployeeId(), e.getName(), e.getSurname(), e.getBalance());
			}
		});
		printWriter.close();
	}

	@Override
	public void writeTransactions(List<Transaction> transactions) throws IOException {
		FileWriter fileWriter = new FileWriter(pathTransactions);
		PrintWriter printWriter = new PrintWriter(fileWriter);

		transactions.forEach(t -> {
			if(!t.getAmount().contains(";") && 
					!t.getBeverageName().contains(";") && 
					!t.getDate().contains(";") && 
					!t.getOperation().contains(";") && 
					!t.getSubject().contains(";")) {
				printWriter.printf("%s;%s;%s;%s;%s\n", t.getDate(), t.getOperation(), t.getSubject(), t.getBeverageName(),
						t.getAmount());
			}
		});
		printWriter.close();
	}

	@Override
	public void writeBeverages(Map<Integer, Beverage> beveragesMap) throws IOException {
		FileWriter fileWriter = new FileWriter(pathBeverages);
		PrintWriter printWriter = new PrintWriter(fileWriter);

		beveragesMap.entrySet().forEach(en -> {
			Beverage b = en.getValue();
			if(!b.getName().contains(";")) {
				List<Beverage> beveragesList = b.getBeverages();
					beveragesList.forEach(bev -> {						
						printWriter.printf("%d;%s;%d;%d;%d;%d\n", bev.getBeverageId(), bev.getName(), bev.getAvailable(), bev.getBoxPrice(),
								bev.getCapsulesPerBox(), bev.getPrice(1));				
					});
			}
		});
	
		
		printWriter.close();
	}

	@Override
	public void resetStorage() throws IOException {
		  
		   FileWriter fileWriter = new FileWriter(pathEmployees);
		   PrintWriter printWriter = new PrintWriter(fileWriter);
		   printWriter.print("");
		   printWriter.close();
		
		   FileWriter fileWriter2 = new FileWriter(pathTransactions);
		   PrintWriter printWriter2 = new PrintWriter(fileWriter2);
		   printWriter2.printf("");
		   printWriter2.close();
		
		   FileWriter fileWriter3 = new FileWriter(pathBeverages);
		   PrintWriter printWriter3 = new PrintWriter(fileWriter3);	
		   printWriter3.printf(""); 
		   printWriter3.close();
		   
		   FileWriter fileWriter4 = new FileWriter(pathBalance);
		   PrintWriter printWriter4 = new PrintWriter(fileWriter4);	
		   printWriter4.printf("0"); 
		   printWriter4.close();
	}

	@Override
	public void writeTransaction(Transaction t) throws IOException {
		if(!t.getAmount().contains(";") && 
				!t.getBeverageName().contains(";") && 
				!t.getDate().contains(";") && 
				!t.getOperation().contains(";") && 
				!t.getSubject().contains(";")) {

			FileWriter fr = new FileWriter(pathTransactions, true);
			PrintWriter pw = new PrintWriter(fr);
			pw.printf("%s;%s;%s;%s;%s\n", t.getDate(), t.getOperation(), t.getSubject(), t.getBeverageName(),
					t.getAmount());
			fr.close();
				
		}
		
	}

	@Override
	public Integer readBalance() throws IOException {
		Path path = FileSystems.getDefault().getPath(pathData, pathBalance);
		Stream<String> stringStream = Files.lines(path, StandardCharsets.UTF_8);
		ArrayList<Integer> listBalance = new ArrayList<Integer>();
		stringStream.forEach(s->{
			try {				
				listBalance.add(Integer.parseInt(s));
			}catch(Exception e) {
				System.err.println("WrongFileFormat: dataBalance.csv file");
				listBalance.clear();
			}
		});
		
		stringStream.close();
		if(!listBalance.isEmpty())
			return listBalance.get(0);
		else
			throw new IOException();
	}

	@Override
	public void writeBalance(Integer balance) throws IOException {
		   FileWriter fileWriter = new FileWriter(pathBalance);
		   PrintWriter printWriter = new PrintWriter(fileWriter);
		   printWriter.print(balance);
		   printWriter.close();
	}
	
}
