package it.polito.latazza.storage;

import java.util.Map;
import java.io.IOException;
import java.util.List;

import it.polito.latazza.data.Beverage;
import it.polito.latazza.data.Employee;
import it.polito.latazza.data.Transaction;
import it.polito.latazza.exceptions.WrongFileFormatException;

public interface Storage {
	
	/**
	 * Read employees accounts from file.
	 * 
	 * @return a map with format <Key, Employee> which contains the employees accounts
	 * @throws IOException if the file can't be opened
	 * @throws WrongFileFormatException if format of the file is not as expected
	 */
	public Map<Integer,Employee> readEmployees() throws IOException, WrongFileFormatException;
	
	/**
	 * Read transactions from file.
	 * 
	 * @return a list of employees accounts in a map format <Key, Employee> 
	 * @throws IOException if the file can't be opened
	 * @throws WrongFileFormatException if format of the file is not as expected
	 */
	public List<Transaction> readTransactions() throws IOException, WrongFileFormatException;
	
	/**
	 * Read beverages from file.
	 * 
	 * @return a map with format <Key, Beverage> which contains the beverages 
	 * @throws IOException if the file can't be opened
	 * @throws WrongFileFormatException if format of the file is not as expected
	 */
	public Map<Integer,Beverage> readBeverages() throws IOException;
	
	/**
	 * Write employees accounts on file.
	 * 
	 * @throws IOException if the file can't be opened
	 * @throws BadDataFormatException 
	 */
	public void writeEmployees(Map<Integer,Employee> employeesMap) throws IOException;
	
	/**
	 * Write transactions on file.
	 * 
	 * @throws IOException if the file can't be opened
	 */
	public void writeTransactions(List<Transaction> transactions) throws IOException;
	
	/**
	 * Append transaction on file.
	 * 
	 * @throws IOException if the file can't be opened
	 */
	public void writeTransaction(Transaction t) throws IOException;
	
	/**
	 * Write beverages on file.
	 * 
	 * @throws IOException if the file can't be opened
	 */
	public void writeBeverages(Map<Integer,Beverage> beveragesMap) throws IOException;

	/**
	 * Reset all files.
	 * 
	 * @throws IOException if the file can't be opened
	 */
	public void resetStorage() throws IOException;

	/**
	 * Read Balance of LaTazza.
	 * 
	 * @throws IOException if the file can't be opened
	 */
	public Integer readBalance() throws IOException;

	/**
	 * Write Balance of LaTazza.
	 * 
	 * @throws IOException if the file can't be opened
	 */
	public void writeBalance(Integer balance) throws IOException;
	
}
