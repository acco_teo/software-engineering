package it.polito.latazza.data;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import it.polito.latazza.storage.*;
import it.polito.latazza.exceptions.BeverageException;
import it.polito.latazza.exceptions.DateException;
import it.polito.latazza.exceptions.EmployeeException;
import it.polito.latazza.exceptions.NotEnoughBalance;
import it.polito.latazza.exceptions.NotEnoughCapsules;
import it.polito.latazza.exceptions.WrongFileFormatException;

public class DataImpl implements DataInterface {

	private Map<Integer, Employee> e;
	private Map<Integer, Beverage> b;
	private List<Transaction> t;
	private Integer balance;
	private Storage s;
	
	public DataImpl() {
		
		s = new StorageImpl();

		/* Employee collection */
		try {
			e = s.readEmployees();
		} catch (WrongFileFormatException | IOException ex) {
			e = new HashMap<Integer, Employee>();
			try {
				s.writeEmployees(e);
			} catch (IOException ex1) {
				System.exit(-10);
			}
		} 	
		
		/* Beverage collection */
		try {
			b = s.readBeverages(); 	
		} catch (WrongFileFormatException | IOException ex) {
			b = new HashMap<Integer, Beverage>();
			try {
				s.writeBeverages(b);
			} catch (IOException ex1) {
				System.exit(-20);
			}
		}
		
	 	/* Transaction collection */
		try {
			t = s.readTransactions(); 	
		} catch (WrongFileFormatException | IOException ex) {
			t = new ArrayList<Transaction>(); 
			try {
				s.writeTransactions(t);
			} catch (IOException ex1) {
				System.exit(-30);
			}
		}
		
		/* Balance */
		try {
			balance = s.readBalance();
		} catch (IOException ex) {
			balance = Integer.valueOf(0);
			try {
				s.writeBalance(balance);
			} catch (IOException ex1) {
				System.exit(-40);
			}
		}	
	}

	@Override
	public Integer sellCapsules(Integer employeeId, Integer beverageId, Integer numberOfCapsules, Boolean fromAccount)
			throws EmployeeException, BeverageException, NotEnoughCapsules {
		
		/* check data existence */
		if (!e.containsKey(employeeId))
			throw new EmployeeException();
		if (!b.containsKey(beverageId))
			throw new BeverageException();
		Employee et = e.get(employeeId);
		Beverage bt = b.get(beverageId);
		
		/* check correctness */
		if (numberOfCapsules == null || numberOfCapsules < 0)
			throw new NotEnoughCapsules();

		Integer amount = (bt.getPrice(numberOfCapsules));
		if ( amount < 0)
			throw new BeverageException();
		
		if(fromAccount == null)
			throw new EmployeeException();

		/* check availability */
		if (bt.getAvailable() < numberOfCapsules)
			throw new NotEnoughCapsules();
		

		String operationType;
		
		/* update employee balance if needed */
		if (fromAccount) {
			Integer initialBalance = et.getBalance();
			
			/* check overflow on employee balance */
			if( initialBalance < 0 && initialBalance - amount > 0)
				throw new EmployeeException();
			
			et.setBalance( initialBalance - amount );
			operationType = "BALANCE";
			
			/* write employees on file */
			try {
				s.writeEmployees(e);
			} catch (IOException e1) {
				System.exit(-1);
			}
		} else {
			operationType = "CASH";
			
			/* update balance */
			balance += amount;
			
			/* wtite balance on file*/
			try {
				s.writeBalance(balance);
			} catch (IOException e1) {
				System.exit(-1);
			}
		}

		/* update number of available capsules */
		bt.setAvailableAndPrice(- numberOfCapsules);

		/* add new transaction */
		t.add(new Transaction(getTimestamp(), operationType, et.getName() + " " + et.getSurname(), bt.getName(), numberOfCapsules.toString()));
		
		/* write transaction on file */
		/* write beverages on file */
		try {
			s.writeTransaction(t.get(t.size()-1));
			s.writeBeverages(b);
		} catch (IOException e1) {
			System.exit(-1);
		}
		
		return et.getBalance();
	}

	@Override
	public void sellCapsulesToVisitor(Integer beverageId, Integer numberOfCapsules)
			throws BeverageException, NotEnoughCapsules {
		
		/* check data existence */
		if (!b.containsKey(beverageId))
			throw new BeverageException();
		Beverage bt = b.get(beverageId);
		
		/* check correctness */
		if (numberOfCapsules == null || numberOfCapsules < 0)
			throw new NotEnoughCapsules();

		Integer amount = ( bt.getPrice(numberOfCapsules));
		
		if ( amount < 0)
			throw new BeverageException();
		
		/* check availability */
		if (bt.getAvailable() < numberOfCapsules)
			throw new NotEnoughCapsules();
		
		
		/* update number of available capsules */
		bt.setAvailableAndPrice(-numberOfCapsules);
		
		/* add new transaction */
		t.add(new Transaction(getTimestamp(), "VISITOR", "", bt.getName(), numberOfCapsules.toString()));
		
		/* update balance */
		balance += amount;
		
		/* write transaction on file */
		/* wtite balance on file*/
		/* write beverages on file */
		try {
			s.writeTransaction(t.get(t.size()-1));
			s.writeBalance(balance);
			s.writeBeverages(b);
		} catch (IOException e1) {
			System.exit(-1);
		}
	}

	@Override
	public Integer rechargeAccount(Integer id, Integer amountInCents) throws EmployeeException {
		/* check data existence */
		if (!e.containsKey(id))
			throw new EmployeeException();
		Employee et = e.get(id);
		
		/* check correctness */
		Integer emNewBal = et.getBalance() + amountInCents;
		if(amountInCents < 0 || balance + amountInCents < 0 )
				throw new EmployeeException();
	
		et.setBalance(emNewBal);
		
		/* add new transaction */
		t.add(new Transaction(getTimestamp(), "RECHARGE", e.get(id).getName() + " " + e.get(id).getSurname(), "",
				formatAmount(amountInCents)));
		
		/* update balance */
		balance += amountInCents;
		
		/* write transaction on file */
		/* wtite balance on file*/
		/* wrtie employees on file */
		try {
			s.writeTransaction(t.get(t.size()-1));
			s.writeBalance(balance);
			s.writeEmployees(e);
		} catch (IOException e1) {
			System.exit(-1);
		}

		return et.getBalance();
	}

	@Override
	public void buyBoxes(Integer beverageId, Integer boxQuantity) throws BeverageException, NotEnoughBalance {
		/* check data existence */
		if (!b.containsKey(beverageId))
			throw new BeverageException();
		Beverage be = b.get(beverageId);
		
		/* check correctness */
		if(boxQuantity == null)
			throw new BeverageException();
		Integer amount = be.getBoxPrice() * boxQuantity;
		Integer newAvailable = be.getAvailable() + boxQuantity*be.getCapsulesPerBox();
		if (boxQuantity < 0 || amount < 0 || newAvailable < 0)
			throw new BeverageException();
		
		/* check availability */
		if (balance < amount)
			throw new NotEnoughBalance();
		
		/* update beverage availability */
		be.setAvailableAndPrice(+ (boxQuantity * be.getCapsulesPerBox()));

		/* add new transaction */
		t.add(new Transaction(getTimestamp(), "BUY", "", be.getName(), boxQuantity.toString()));

		/* update balance */
		balance -= amount;
		
		/* write transaction on file */
		/* wtite balance on file*/
		/* wrtie beverages on file */
		try {
			s.writeTransaction(t.get(t.size()-1));
			s.writeBalance(balance);
			s.writeBeverages(b);
		} catch (IOException e1) {
			System.exit(-1);
		}

	}

	@Override
	public List<String> getEmployeeReport(Integer employeeId, Date startDate, Date endDate)
			throws EmployeeException, DateException {

		/* check data existence */
		if (!e.containsKey(employeeId))
			throw new EmployeeException();
		
		/* check correctness */
		if( startDate == null || endDate == null)
			throw new DateException();
		if (startDate.after(endDate))
			throw new DateException();

		List<String> employeeRecord = new ArrayList<String>();
		Date d;
		for (Transaction tt : t) {
			try {
				d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(tt.getDate());
				if (tt.getSubject()
						.equals(this.getEmployeeName(employeeId) + " " + this.getEmployeeSurname(employeeId)))
					if (d.after(startDate) && d.before(endDate))
						employeeRecord.add(tt.toString());
			} catch (ParseException e1) {
				throw new DateException();
			}
		}

		return employeeRecord;
	}

	@Override
	public List<String> getReport(Date startDate, Date endDate) throws DateException {
		/* check correctness */
		if( startDate == null || endDate == null)
			throw new DateException();
		if (startDate.after(endDate))
			throw new DateException();

		List<String> generalReport = new ArrayList<String>();
		Date d;
		for (Transaction tt : t) {
			try {
				d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(tt.getDate());
				if (d.after(startDate) && d.before(endDate))
					generalReport.add(tt.toString());
			} catch (ParseException e1) {
				throw new DateException();
			}
		}

		return generalReport;
	}

	@Override
	public Integer createBeverage(String name, Integer capsulesPerBox, Integer boxPrice) throws BeverageException {
		Integer id = b.size() + 1;
		
		/* check correctness */
		if (name == null || capsulesPerBox == null || boxPrice == null)
			throw new BeverageException();
		if (name.equals("") || name.contains(";") || capsulesPerBox <= 0 || boxPrice <= 0)
			throw new BeverageException();

		b.put(id, new Beverage(id, name, 0, boxPrice, capsulesPerBox, boxPrice / capsulesPerBox));
	
		
		/* write beverage on file */
		try {
			s.writeBeverages(b);
		} catch (IOException e) {
			System.exit(-1);
		}

		return id;
	}

	@Override
	public void updateBeverage(Integer id, String name, Integer capsulesPerBox, Integer boxPrice)
			throws BeverageException {
		
		/* check correctness */
		if (id == null || capsulesPerBox == null || name == null || boxPrice == null)
			throw new BeverageException();
		if (name.equals("") || name.contains(";") || capsulesPerBox <= 0 || boxPrice <= 0)
			throw new BeverageException();
		
		/* check data existence */
		if(!b.containsKey(id))
			throw new BeverageException();
		Beverage be = b.get(id);

		be.setName(name);
		be.setCapsulesPerBox(capsulesPerBox);
		be.setBoxPrice(boxPrice);
		
		/* write beverage on file */
		try {
			s.writeBeverages(b);
		} catch (IOException e) {
			System.exit(-1);
		}
	}

	@Override
	public String getBeverageName(Integer id) throws BeverageException {
		if (b.containsKey(id))
			return b.get(id).getName();
		else
			throw new BeverageException();
	}

	@Override
	public Integer getBeverageCapsulesPerBox(Integer id) throws BeverageException {
		if (b.containsKey(id)) {
			return b.get(id).getCapsulesPerBox();
		} else
			throw new BeverageException();
	}

	@Override
	public Integer getBeverageBoxPrice(Integer id) throws BeverageException {
		if (b.containsKey(id)) {
			return b.get(id).getBoxPrice();
		} else
			throw new BeverageException();
	}

	@Override
	public List<Integer> getBeveragesId() {
		return new ArrayList<Integer>(b.keySet());
	}

	@Override
	public Map<Integer, String> getBeverages() {
		return b.entrySet()
				.stream()
				.collect(Collectors.toMap(
						Map.Entry::getKey, 
						entry -> (entry.getValue()).getName()));
	}

	@Override
	public Integer getBeverageCapsules(Integer id) throws BeverageException {
		if (id != null && b.containsKey(id)) {
			return b.get(id).getAvailable();
		} else
			throw new BeverageException();
	}

	@Override
	public Integer createEmployee(String name, String surname) throws EmployeeException {
		Integer id = e.size() + 1;

		/* check correctness */
		if (name == null || surname == null)
			throw new EmployeeException();
		if (name.equals("") || surname.equals("") || name.contains(";") || surname.contains(";"))
			throw new EmployeeException();

		e.put(id, new Employee(id, name, surname, 0));
		
		
		/* write employee on file */
		try {
			s.writeEmployees(e);
		} catch (IOException e1) {
			System.exit(-1);
		}

		return id;
	}

	@Override
	public void updateEmployee(Integer id, String name, String surname) throws EmployeeException {
		
		/* check correctness */
		if (id == null || name == null || surname == null)
			throw new EmployeeException();
		if (name.equals("") || surname.equals("") || name.contains(";") || surname.contains(";"))
			throw new EmployeeException();
		
		/* check data existence */
		if(!e.containsKey(id))
			throw new EmployeeException();
		Employee em = e.get(id);

		em.setSurname(surname);
		em.setName(name);
		
		/* write employee on file */
		try {
			s.writeEmployees(e);
		} catch (IOException e1) {
			System.exit(-1);
		}
	}

	@Override
	public String getEmployeeName(Integer id) throws EmployeeException {
		if (e.containsKey(id))
			return e.get(id).getName();
		else
			throw new EmployeeException();
	}

	@Override
	public String getEmployeeSurname(Integer id) throws EmployeeException {
		if (e.containsKey(id))
			return e.get(id).getSurname();
		else
			throw new EmployeeException();
	}

	@Override
	public Integer getEmployeeBalance(Integer id) throws EmployeeException {
		if (e.containsKey(id))
			return e.get(id).getBalance();
		else
			throw new EmployeeException();
	}

	@Override
	public List<Integer> getEmployeesId() {
		return new ArrayList<Integer>(e.keySet());
	}

	@Override
	public Map<Integer, String> getEmployees() {
		return e.entrySet()
				.stream()
				.collect(Collectors.toMap(
						Map.Entry::getKey,
						entry -> (entry.getValue()).getName() + " " + (entry.getValue()).getSurname()));
	}

	@Override
	public Integer getBalance() {
		return balance;
	}

	@Override
	public void reset() {
		e.clear();
		b.clear();
		t.clear();
		balance = 0;
		try {
			s.resetStorage();
		} catch (IOException ex) {
			System.exit(-1);
		}
	}

	private String getTimestamp() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}
	
	private String formatAmount(Integer amount) {
		return String.format("%.2f \u20ac", (float)amount/100).replace(',', '.');
	}

}
