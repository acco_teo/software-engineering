package it.polito.latazza.data;

import java.util.LinkedList;
import java.util.List;

public class Beverage {

	private Integer beverageId;
	private String name;
	private List<Integer> available;
	private Integer boxPrice;
	private Integer capsulesPerBox;
	private List<Integer> price;

	public Beverage(Integer beverageId, String name, Integer available, Integer boxPrice, Integer capsulesPerBox,
			Integer price) {
		this.beverageId = beverageId;
		this.name = name;
		this.available = new LinkedList<Integer>();
		this.available.add(available);
		this.boxPrice = boxPrice;
		this.capsulesPerBox = capsulesPerBox;
		this.price = new LinkedList<Integer>();
		this.price.add(price);
	}

	public Integer getBeverageId() {
		return this.beverageId;
	}

	public String getName() {
		return this.name;
	}

	public Integer getAvailable() {
		Integer res = 0;
		for (Integer num : this.available) {
			res += num;
		}

		return res;
	}

	public Integer getBoxPrice() {
		return this.boxPrice;
	}

	public Integer getCapsulesPerBox() {
		return this.capsulesPerBox;
	}

	public Integer getLastPrice() {
		return this.boxPrice / this.capsulesPerBox;
	}

	public Integer getPrice(Integer numberOfCapsules) {
		Integer totPrice = 0;

		if (numberOfCapsules < 0)
			return -1;

		if (this.available.size() != this.price.size()) {
			System.err.println("Error in getPrice: " + this.available.size() + " != " + this.price.size());
			return -1;
		}

		for (int i = 0; i < this.price.size(); i++) {
			if (numberOfCapsules <= 0)
				break;
			Integer usedCapsules = Math.min(numberOfCapsules, this.available.get(i));
			totPrice += this.price.get(i) * usedCapsules;
			numberOfCapsules -= usedCapsules;

		}

		return totPrice;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * if {@code amount} is > 0, adds a new set of available capsules or updates the
	 * last one (if the current price is the same).<br>
	 * if {@code amount} is < 0, removes from the head of the list as many sets of
	 * capsules as required.<br>
	 * if {@code amount} is == 0, returns.
	 * 
	 * @param amount
	 */
	public void setAvailableAndPrice(Integer amount) {
		if (amount == 0)
			return;

		if (this.available.size() != this.price.size()) {
			System.err.println(
					"Error in setAvailableAndPrice (before): " + this.available.size() + " != " + this.price.size());
			return;
		}

		Integer last = this.available.size() - 1;

		if (amount > 0) {
			Integer newPrice = this.boxPrice / this.capsulesPerBox;
			Integer lastPrice = this.price.get(last);

			if (newPrice == lastPrice)
				this.available.set(last, this.available.get(last) + amount);
			else {
				/*
				 * same kind of capsules but with a new price has been purchased removing
				 */
				if (this.available.size() == 1 && this.available.get(0) == 0 && this.price.size() == 1) {
					this.available.remove(0);
					this.price.remove(0);
				}
				this.available.add(amount);
				this.price.add(newPrice);
			}
		} else {
			amount = -amount;
			int i;
			for (i = 0; i <= last; i++) {
				if (amount <= 0)
					break;
				Integer toRemove = Math.min(amount, this.available.get(i));
				this.available.set(i, this.available.get(i) - toRemove);
				amount -= toRemove;
			}

			if (i > 0 && this.available.size() > 1 && this.price.size() > 1) {
				if (this.available.get(i - 1) != 0)
					i--;
				this.available = this.available.subList(i, last + 1);
				this.price = this.price.subList(i, last + 1);
			}
		}

		if (this.available.size() != this.price.size()) {
			System.err.println(
					"Error in setAvailableAndPrice (after): " + this.available.size() + " != " + this.price.size());
			return;
		}
	}

	public void setBoxPrice(Integer boxPrice) {
		this.boxPrice = boxPrice;
	}

	public void aggregateBeverages(Integer available, Integer price) {
		if (available < 0 || price < 0)
			return;

		if (this.available.size() != this.price.size()) {
			System.err.println("Error in aggregateBeverages: " + this.available.size() + " != " + this.price.size());
			return;
		}

		this.available.add(available);
		this.price.add(price);
	}

	public List<Beverage> getBeverages() {
		List<Beverage> list = new LinkedList<Beverage>();

		if (this.available.size() != this.price.size()) {
			System.err.println("Error in getBeverages: " + this.available.size() + " != " + this.price.size());
			return list;
		}

		for (int i = 0; i < this.available.size(); i++) {
			Beverage b = new Beverage(this.beverageId, this.name, this.available.get(i), this.boxPrice,
					this.capsulesPerBox, this.price.get(i));
			list.add(b);
		}

		return list;
	}

	public void setCapsulesPerBox(Integer capsulesPerBox) {
		this.capsulesPerBox = capsulesPerBox;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Beverage) {
			Beverage other = (Beverage) obj;
			if (this == obj || (other.getAvailable()).equals(this.getAvailable())
					&& other.getBeverageId().equals(beverageId) && other.getName().equals(name)
					&& other.getBoxPrice().equals(boxPrice) && other.getCapsulesPerBox().equals(capsulesPerBox))
				return true;
		}
		return false;
	}

}
