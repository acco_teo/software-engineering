package it.polito.latazza.data;

public class Transaction {

	private String date;
	private String operation;
	private String subject;
	private String beverageName;
	private String amount;

	public Transaction(String date, String operation, String subject, String beverageName, String amount) {
		this.date = date;
		this.operation = operation;
		this.subject = subject;
		this.beverageName = beverageName;
		this.amount = amount;
	}

	public String getDate() {
		return this.date;
	}

	public String getOperation() {
		return this.operation;
	}

	public String getSubject() {
		return this.subject;
	}

	public String getBeverageName() {
		return this.beverageName;
	}

	public String getAmount() {
		return this.amount;
	}

	@Override 
	public String toString() {
		String tmp = new String(date + " " + operation);
		if(!subject.equals(""))
			tmp += " " + subject;
		if(!beverageName.equals(""))
			tmp += " " + beverageName;
		return tmp + " " + amount;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Transaction) {
			Transaction other = (Transaction) obj;
			if(this == obj || this.toString().equals(other.toString()))
				return true;
		}
		return false;
	}
}
