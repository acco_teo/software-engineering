package it.polito.latazza.data;

public class Employee {

	private Integer employeeId;
	private String name;
	private String surname;
	private Integer balance;

	public Employee(Integer employeeId, String name, String surname, Integer balance) {
		this.employeeId = employeeId;
		this.name = name;
		this.surname = surname;
		this.balance = balance;
	}

	public Integer getEmployeeId() {
		return this.employeeId;
	}

	public String getName() {
		return this.name;
	}

	public String getSurname() {
		return this.surname;
	}

	public Integer getBalance() {
		return this.balance;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Employee) {
			Employee other = (Employee) obj;
			if(this == obj || (other.getBalance().equals(balance) &&
					other.getEmployeeId().equals(employeeId) &&
					other.getName().equals(name) && 
					other.getSurname().equals(surname)))
				return true;
		}
		return false;
	}
	
	

}
