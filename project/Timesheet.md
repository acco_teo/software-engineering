# Timesheet

Please use this table to provide the total working time per deliverable. This means that you have to sum the working hours of each component of the team.

| Deliverable  | Total working time in hours |
|:------------:|:---------------------------:|
| Requirements | 50 |
| Design       | 17 |
| Coding       | 42 |
| Testing      | 48 |
